#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir -p $DIR/Build/pkg_simplecaddy/packages
cd $DIR/pkg_simplecaddy/packages/

zip -rD $DIR/Build/pkg_simplecaddy/packages/com_simplecaddy.zip com_simplecaddy
zip -rD $DIR/Build/pkg_simplecaddy/packages/mod_simplecaddy.zip mod_simplecaddy
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_sccoupons.zip plg_sccoupons
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scpaypal.zip plg_scpaypal
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scshipping.zip plg_scshipping
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_sctax.zip plg_sctax
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_simplecaddy.zip plg_simplecaddy
zip -rD $DIR/Build/pkg_simplecaddy/packages/mod_altsbergcaddy.zip mod_altsbergcaddy
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scalbaniancountry.zip plg_scalbaniancountry
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scbulgariancountry.zip plg_scbulgariancountry
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scccnowcountry.zip plg_scccnowcountry
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scpaypalcountry.zip plg_scpaypalcountry
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scserbiancountry.zip plg_scserbiancountry
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scanalytics.zip plg_scanalytics
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scfbpixel.zip plg_scfbpixel
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scccnow.zip plg_scccnow
zip -rD $DIR/Build/pkg_simplecaddy/packages/plg_scexpresscheckout.zip plg_scexpresscheckout

cp $DIR/pkg_simplecaddy/pkg_simplecaddy.xml $DIR/Build/pkg_simplecaddy/

cd $DIR/Build/
zip -rD $DIR/pkg_simplecaddy.zip pkg_simplecaddy

rm -rf $DIR/Build