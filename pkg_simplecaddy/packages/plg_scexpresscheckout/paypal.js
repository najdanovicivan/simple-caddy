jQuery(document).ready(function($) {
    paypal.Button.render({

        env: ppenvironment, // Or 'sandbox'

        style: {
            layout: 'vertical',  // horizontal | vertical
            size:   'responsive',    // medium | large | responsive
            shape:  'rect',      // pill | rect
            color:  'gold'       // gold | blue | silver | black
        },

        client: {
            sandbox: ppsanboxid,
            production: ppproductionid
        },

        funding: {
            allowed: [ paypal.FUNDING.CARD ],
            disallowed: [ paypal.FUNDING.CREDIT ]
        },

        commit: true, // Show a 'Pay Now' button

        payment: function (data, actions) {
            return actions.payment.create({
                payer:{
                    payer_info: paypalpayer
                },
                payment: {
                    transactions: [
                        {
                            reference_id : paypalreferenceid,
                            amount: {total: paypaltotal, currency: paypalcurrency},
                            item_list: {
                                items: paypalitems,
                                shipping_address: paypalshipping
                            }
                        }
                    ],
                    redirect_urls: {
                        return_url: paypalsuccesurl,
                        cancel_url: paypalfailurl
                    }
                }

            });
        },

        onAuthorize: function (data, actions) {
            return actions.payment.execute().then(function (payment) {

                // The payment is complete!
                // You can now show a confirmation message to the customer
                return actions.redirect();
            });
        }

    }, '#paypal-button');
});