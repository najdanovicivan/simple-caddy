<?php
/**
 * @copyright Henk von Pickartz 2011-2014
 * SimpleCaddy Paypal processor
 * @version 2.05 for Joomla 3.x
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access allowed to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
    require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
    return;
}
/**
 * This plugin has three parts:
 * top part is getting the SimpleCaddy functions library
 * in the function onContentPrepare are the Joomla functions to manage the plugin in the content
 * the function showPayPalScreen displays and manages PayPal specifics
 */

class scexpresscheckoutconfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $ppenvironment;
	var $overrideaddress;
	var $reselleremail;
	var $paypalcurrency;
	var $returnsuccess;
	var $returnfail;
	var $dlpage;
	var $sandboxid;
	var $productionid;

	function __construct(){
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_scexpresscheckout', JPATH_ADMINISTRATOR);
		$db	= JFactory::getDBO();
		try {
            parent::__construct( '#__sc_expresschekout', 'id', $db );
		}
		catch (Exception $e) {
			$query= "CREATE TABLE `#__sc_expresschekout` (
			`id`  int NULL AUTO_INCREMENT ,
			`sandboxid` varchar(255),
			`productionid` varchar(255),
			`ppenvironment`  INT(11) NULL ,
			`overrideaddress`  INT(11) NULL ,
			`reselleremail`  varchar(255) NULL ,
			`paypalcurrency`  text NULL ,
			`returnsuccess`  text NULL ,
			`returnfail`  text NULL ,
			PRIMARY KEY (`id`)
			);";
			$this->_db->setQuery($query);
			$this->_db->execute();
			// now reconstruct, since it failed above...
            parent::__construct( '#__sc_expresschekout', 'id', $db );
		}
		// get the first and only useful record in the db here
		$query=$this->_db->getQuery(true);
		$query->select("id");
		$query->from("`$this->_tbl`");
		$this->_db->setQuery($query);
		$id=$this->_db->loadResult(); // this sets the first ID as the one to use.
		$this->load($id); // load the first id
	}

	function _showconfig() { // mandatory function name
		$lang = JFactory::getLanguage(); // joomla's backend does not load frontend languages by itself
		$extension = 'plg_content_scexpresscheckout'; // so we need to do this manually here
		$lang->load($extension);
		$ppcurrency=array();
		$ppcurrency["DISABLED"]="Disable PayPal For This Language";
		$ppcurrency["AUD"]="Australian Dollars (AUD)";
		$ppcurrency["BRL"]="Brazilian Real (BRL) (Domestic use only)";
		$ppcurrency["CAD"]="Canadian dollar (CAD)";
		$ppcurrency["CZK"]="Czech Koruna (CZK)";
		$ppcurrency["DKK"]="Danish Krone (DKK)";
		$ppcurrency["EUR"]="Euros (EUR)";
		$ppcurrency["HKD"]="Hong Kong Dollar (HKD)";
		$ppcurrency["HUF"]="Hungarian Forint (HUF)";
		$ppcurrency["ILS"]="Israeli New Sheqel (ILS)";
		$ppcurrency["JPY"]="Yen (YEN)";
		$ppcurrency["MYR"]="Malaysian Ringgit (MYR) (Domestic use only)";
		$ppcurrency["MXN"]="Mexican Pesos (MXN)";
		$ppcurrency["NOK"]="Norwegian Krone (NOK)";
		$ppcurrency["NZD"]="New Zealand Dollar (NZD)";
		$ppcurrency["PHP"]="Philippine Peso (PHP)";
		$ppcurrency["PLN"]="Polish Zloty (PLN)";
		$ppcurrency["GBP"]="Pound Sterling (GBP)";
		$ppcurrency["RUB"]="Russian Ruble (RUB)";
		$ppcurrency["SGD"]="Singapore Dollar (SGD)";
		$ppcurrency["SEK"]="Swedish Krona (SEK)";
		$ppcurrency["CHF"]="Swiss Franc (CHF)";
		$ppcurrency["TWD"]="Taiwan New Dollar (TWD)";
		$ppcurrency["THB"]="Thai Baht (THB)";
		$ppcurrency["USD"]="US Dollar (USD)";

		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");

		$this->load();

		$db = JFactory::getDbo();
		$query="SELECT sef, title_native, lang_code FROM #__languages ORDER BY sef ASC";
		$db->setQuery($query);
		$languages = $db->loadObjectList();

		$this->paypalcurrency = unserialize($this->paypalcurrency);
		$this->returnsuccess = unserialize($this->returnsuccess);
		$this->returnfail = unserialize($this->returnfail);

		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCPP_CONFIGURATION" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SCPP_SAVE"), false);
		JToolbarHelper::cancel();
		JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false,  false );
		?>
		<form name="adminForm" id="adminForm">
			<table class="adminform">
				<tr>
					<td style="width: 250px;">
						<label for="ppenvironment">
						<?php echo JText::_("SCPP_ENVIRONMENT");?>
						</label>
					</td>
					<td>
                        <div class='control-group'>
                            <div class="controls">
                                <fieldset class="radio btn-group">';
                                    <label for='0ppenvironment'><input type='radio' id='0ppenvironment' value='0' name='ppenvironment' <?= ($this->ppenvironment==0? 'checked="checked"':'') ?>><?= JText::_('SCPP_SANDBOX') ?></label>
                                    <label for='1ppenvironment'><input type='radio' id='1ppenvironment' value='1' name='ppenvironment' <?= ($this->ppenvironment==1? 'checked="checked"':'') ?>><?= JText::_('SCPP_LIVE') ?></label>
                                </fieldset>
                            </div>
                        </div>
					</td>
				</tr>
				<tr>
					<td style="width: 250px;">
						<label for="overrideaddress">
							<?php echo JText::_("Override Shipping Address");//Translate This?>
						</label>
					</td>
					<td>
                        <div class='control-group'>
                            <div class="controls">
                                <fieldset class="radio btn-group">';
                                    <label for='0overrideaddress'><input type='radio' id='0overrideaddress' value='0' name='overrideaddress' <?= ($this->overrideaddress==0? 'checked="checked"':'') ?>><?= JText::_('NO') ?></label>
                                    <label for='1overrideaddress'><input type='radio' id='1overrideaddress' value='1' name='overrideaddress' <?= ($this->overrideaddress==1? 'checked="checked"':'') ?>><?= JText::_('YES') ?></label>
                                </fieldset>
                            </div>
                        </div>
					</tr>
				<tr>
					<td style="width: 250px;">
						<label for="reselleremail">
						<?php echo JText::_("SCPP_RESELLER_EMAIL");?>
						</label>
					</td>
					<td>
						<input type="text" id="reselleremail" name="reselleremail" value="<?php echo $this->reselleremail; ?>" size="100" />
					</td>
				</tr>
                <tr>
                    <td style="width: 250px;">
                        <label for="sandboxid">
                            <?php echo JText::_("SCEC_SANDBOX_ID");?>
                        </label>
                    </td>
                    <td>
                        <input type="text" id="sandboxid" name="sandboxid" value="<?php echo $this->sandboxid; ?>" size="100" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 250px;">
                        <label for="productionid">
                            <?php echo JText::_("SCEC_PRODUCTION_ID");?>
                        </label>
                    </td>
                    <td>
                        <input type="text" id="productionid" name="productionid" value="<?php echo $this->productionid; ?>" size="100" />
                    </td>
                </tr>

				<?php
				foreach ($languages as $l){
					echo "<tr><td>$l->title_native ".JText::_("SCPP_CURRENCY")."</td><td>";
					echo "<select name=\"paypalcurrency[$l->lang_code]\">";
					foreach ($ppcurrency as $key=>$f) {
						echo "<option value='$key'".($key==$this->paypalcurrency["$l->lang_code"]?" selected":"").">$f</option>";
					}
					echo "</select></td></tr>" ;

					echo "<tr><td>";
					echo "$l->title_native ". JText::_("SCPP_RETURNSUCCESS");
					echo "</td><td>";
					echo "<input type=\"text\" name=\"returnsuccess[$l->lang_code]\" value=\"".$this->returnsuccess["$l->lang_code"]."\" size=\"100\" />";
					echo "</td></tr>";

					echo "<tr><td>";
					echo "$l->title_native ". JText::_("SCPP_RETURNFAIL");
					echo "</td><td>";
					echo "<input type=\"text\" name=\"returnfail[$l->lang_code]\" value=\"".$this->returnfail["$l->lang_code"]."\" size=\"100\" />";
					echo "</td></tr>";
				}

				?>



			</table>
			<input type="hidden" name="option" value="com_simplecaddy" />
			<input type="hidden" name="action" value="pluginconfig"/>
			<input type="hidden" name="pluginname" value="scexpresscheckout"/>
			<input type="hidden" name="task" />
			<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
		</form>
		<p style="color: red;font-weight: bold;">
			<?php
			echo JText::_("SCPP_CLOAK_WARNING");
			?>
		</p>
	<?php
	}

	function save($src="", $orderingFilter = '', $ignore = '') { // mandatory function to save any variables
		$this->bind($_REQUEST); // get all the fields from the admin form

		$this->paypalcurrency=serialize($this->paypalcurrency);
		$this->returnsuccess = serialize($this->returnsuccess);
		$this->returnfail = serialize($this->returnfail);


		$b=$this->store(); // store the relevant fields only
		if ($b) {
			$msg=JText::_("SCPP_CONFIG_SAVED");
		}
		else
		{
			$dbmsg=$this->_db->getErrorMsg();
			$msg=JText::_("SCPP_CONFIG_NOT_SAVED") . $dbmsg;
		}
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage($msg);
		$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=scexpresscheckout");
	}

    function cancel() {
        $mainframe=JFactory::getApplication();
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
    }
    function main() {
        $mainframe=JFactory::getApplication();
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
    }
}

// Import Joomla! Plugin library filec
jimport('joomla.plugin.plugin');
require_once (JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php'); // mandatory

//The Content plugin Loadmodule
class plgContentScexpresscheckout extends JPlugin {
	var $debugshow="hidden"; // just for testing purposes
	var $_plugin_number	= 0;

	function __construct( &$subject, $config ) {
        parent::__construct( $subject, $config );
        $this->loadLanguage(); // necessary or not, let's make sure we get the language file
	}

	public function _setPluginNumber() {
		$this->_plugin_number = (int)$this->_plugin_number + 1; // only the first occurrence of the plugin should load css
	}

	function onContentPrepare($context, &$article, &$params, $page = 0 ) {
		if (!JComponentHelper::isEnabled('com_simplecaddy', true)) { // check for the component install
			echo "<div style='color:red;'>The SimpleCaddy component is not installed or is not enabled</div>";
			return "";
		}

        $regex = '/{(scexpresscheckout)\s*(.*?)}/i'; // the plugin code to get from content

        $parms=array();
        $matches = array();
        preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
        foreach ($matches as $elm) {
 			$this->_setPluginNumber();
 			if ($this->_plugin_number==1) { // get the stylesheet only ONCE per page
				JHTML::stylesheet('components/com_simplecaddy/css/simplecaddy.css' );
				// on windows servers this may need to be changed to
				// JHTML::stylesheet('components\com_simplecaddy\css\simplecaddy.css' );
 			}
 			if ($this->_plugin_number>1 ) { // there should be only ONE payment plugin per page...
	            $article->text = preg_replace($regex, JText::_("SC_EXTRA_PLUGINS_REMOVED"), $article->text, 1);
		 		return true;
 			}
			$line=str_replace("&nbsp;", " ", $elm[2]);
            $line=str_replace(" ", "&", $line);
            $line=strtolower($line);
            parse_str( $line, $parms );

            if (!isset($parms['type'])) { // no type provided, or just forgot...
            	$parms["type"]="checkout";
            }
        	// get all different types of display here, define manually to avoid hacking of the code
        	switch (strtolower($parms['type']) ) {
        		case "ipn":
        		case "paysuccess":
        			$html=$this->showPayPalSuccess($parms);
        			break;
        		case "payfail":
        			$html=$this->showPayPalFail($parms);
        			break;
       			case "checkout": // the default if nothing has been provided
                	$html=$this->showExpressCheckout($parms);
                	break;
        		default: // anything else provides an error message
                	$html=JText::_("SC_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). "({$parms['type']})";
        	}
            $article->text = preg_replace($regex, $html, $article->text, 1);
        }
        return true;
	}

	function onAjaxCreatePayment(){
		//TODO: Server Side
    }

    function onAjaxExecutePayment(){
	    $lang = JFactory::getLanguage();
	    return "TODO// Execute Payment ".$lang->getTag();
    }

	function showExpressCheckout($parms){

		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;


		//Configurations
		$cfg=new scexpresscheckoutconfiguration();
		$lang = JFactory::getLanguage();
		$ucurrency = unserialize($cfg->paypalcurrency);
		$currency = $ucurrency[$lang->getTag()];
		if ($currency == "DISABLED") {
		    //TODO: JTEXT
			$html = "Paypal is disabled for this site language";
			return $html;
		}

		$environment = ($cfg->ppenvironment == 0 ? 'sandbox' : 'production');

        //Order Data

		$ordercode=$input->get("data");

		$orders=new orders();
		$orderid=$orders->getOrderIdFromCart($ordercode);
		$order=new order();
		$order->load($orderid);
		$order->status="Pending";
		$order->store();

		$gtotal = 0;
		$items = array();
		$odetails=new orderdetail();
		$lst=$odetails->getDetailsByOrderId($orderid);
		foreach ($lst as $product) {
			// create a post field and field value for PayPal
			if($product->total>0) {
			    $items[] = (object)[
			        'sku'  => $product->prodcode,
                    'name' => $product->shorttext,
                    'quantity' => $product->qty,
                    'price' => number_format($product->unitprice, 2,".", ""),
                    'currency' => $currency,
                ];
			}
			$gtotal += $product->total;
		}

		$jsonitems = json_encode($items);

		//Shipping Data
		$orderfields=unserialize($order->customfields);

		$state = '';
		if($orderfields["paypalcountry"]=="US")
			$state = $orderfields["paypalusstate"];
		else if($orderfields["paypalcountry"]=="CA")
			$state = $orderfields["paypalcastate"];

		$shipping_address="";

        $country = $orderfields["paypalcountry"];



			$shipping_address = (object)[
				'recipient_name' => $orderfields["first_name"]. " " . $orderfields["last_name"],
				'line1' => $orderfields["address1"],
				'line2' => $orderfields["address2"],
				'city' => $orderfields["city"],
				'country_code' => $orderfields["paypalcountry"],
				'postal_code' => $orderfields["codepostal"],
				'phone' => $orderfields["telephone"],
				'state' => $state
			];


		$payer = (object)[
			'email' => $orderfields["email"],
			'first_name' => $orderfields["first_name"],
			'last_name' => $orderfields["last_name"],
			'phone' => $orderfields["telephone"],
			'country_code' => $orderfields["paypalcountry"],
			'billing_address' => $shipping_address
        ];


		$jsonpayer = json_encode($payer);

		$jsonshipping = json_encode($shipping_address);

		//Redirects
		$ureturnsucces = unserialize($cfg->returnsuccess);
		$ufailurl = unserialize($cfg->returnfail);
		$successurl = $ureturnsucces[$lang->getTag()];
		$failurl = $ufailurl[$lang->getTag()];
		$successurl .="?custom=$order->id";
		$failurl .="?custom=$order->id";

		//Add JavaScript To Page;
		$document = JFactory::getDocument();

		$document->addScriptDeclaration("
		    var ppenvironment = '$environment';
		    var ppsanboxid = '$cfg->sandboxid';
		    var ppproductionid = '$cfg->productionid';
		    var paypalreferenceid = '$orderid';
		    var paypaltotal = '$gtotal';
		    var paypalcurrency = '$currency';
		    var paypalsuccesurl = '$successurl';
		    var paypalfailurl = '$failurl';
		    var paypalshipping = $jsonshipping;
		    var paypalitems = $jsonitems;
		    var paypalpayer = $jsonpayer;
		");



	    $document->addScript("https://www.paypalobjects.com/api/checkout.js");

		JHtml::_('jquery.framework');

		$document->addScript('/plugins/content/scexpresscheckout/paypal.js');

		$html = "<div id=\"paypal-button\"></div>";

		return $html;
    }

	function showPayPalSuccess($parms) {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;


        $orderid=$input->get("custom"); // orderid coming back from PP
		$txn_id=$input->get("txn_id"); // pp transaction ID
		
		
    	if (!is_null($orderid)){

        	$scorder=new order();
        	$scorder->load($orderid);
        	$scorder->status="Paid";
        	// obviously there are other changes you could make to an order upon successful payment...
        	$scorder->store();
		}
		
		$html = "";
		return $html;
	}

	function showPayPalFail() {


		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;

		//print_r($input);
		
		$orderid=$input->get("custom"); // orderid coming back from PP
		
		if (!is_null($orderid)){
	
			$scorder=new order();
			$scorder->load($orderid);
			$scorder->status="Payment Failed";
			// obviously there are other changes you could make to an order upon successful payment...
			$scorder->store();
		}
		
		$html = "";
		return $html;

	}
}