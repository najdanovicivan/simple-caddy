<?php
/**
* @package SimpleCaddy 3.0 for Joomla 3.6+
* @copyright Copyright (C) 2006-2017 Henk von Pickartz. All rights reserved.
* @license     GNU General Public License version 2 or later; see LICENSE.txt
* SimpleCaddy plugin
*/
// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die();
$debug=0;

jimport( 'joomla.plugin.plugin' );

if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
	require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
	return;
}

/**
 * configuration class
 */

class simplecaddyconfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $showproductcodetext;
	var $showproductcode;
	var $showtitle;
	var $showunitpricetext;
	var $showunitprice;
	var $showquantitytext;
	var $showquantity;
	var $showcartoptionstitle;
	var $showproductcodecart;
	var $showavailablequantity;
	var $showavailablequantitytext;

	function __construct() { // initialise class and check if data table exists. If not, create it
		// Load plugin language, since we use it in the backend, standard language loading is NOT performed
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_simplecaddy', JPATH_ADMINISTRATOR);
		$db	= JFactory::getDBO();
		try {
			parent::__construct( '#__sc_simplecaddy', 'id', $db );
	   	}
	   	catch (Exception $e) {
			$query= "CREATE TABLE `#__sc_simplecaddy` (
			`id`  int NULL AUTO_INCREMENT ,
			`showproductcodetext`  INT(11) NOT NULL ,
			`showproductcode`  INT(11) NOT NULL ,
			`showtitle`  INT(11) NOT NULL ,
			`showunitpricetext`  INT(11) NOT NULL ,
			`showunitprice`  INT(11) NOT NULL ,
			`showquantitytext`  INT(11) NOT NULL ,
			`showquantity`  INT(11) NOT NULL ,
			`showcartoptionstitle`  INT(11) NOT NULL ,
			`showproductcodecart`  INT(11) NOT NULL ,
			`showavailablequantity`  INT(11) NOT NULL ,
			`showavailablequantitytext`  INT(11) NOT NULL ,
			`skipstrings`  text NULL,
			PRIMARY KEY (`id`)
			);";
			$this->_db->setQuery($query);
			$this->_db->execute();
		    // now repeat the construct since it failed above...
		    parent::__construct( '#__sc_simplecaddy', 'id', $db );
	   	}
		$query=$db->getQuery(true);
		$query->select("`id`");
		$query->from("`$this->_tbl`");
//		$query="select `id` from `$this->_tbl` ";
		$this->_db->setQuery($query);
		$id=$this->_db->loadResult(); // this sets the first ID as the one to use.
		// get the first and only useful record in the db here
		$this->load($id); // load the first id
	}

	function _showconfig() { // mandatory function name
		$lang = JFactory::getLanguage(); // joomla's backend does not load frontend languages by itself
		$extension = 'plg_content_simplecaddy'; // so we need to do this manually here
		$lang->load($extension);

		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
		$fields=new fields();
		$fieldlist=$fields->getFieldNames(); // get all the fields, standard and custom
		$this->load();
		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SC_PLUGIN_CONFIG" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SC_SAVE"), false);
		JToolbarHelper::custom("apply", "apply", "", JText::_("SC_APPLY"), false);
		JToolbarHelper::cancel();
		?>
		<form name="adminForm" id="adminForm">
			<table class="table">
				<tr>
					<th style="width: 150px;">
						<?php echo JText::_("SC_OPTION");?></th><th><?php echo JText::_("SC_SETTING");?>
					</th>
				</tr>
				<tr>
					<td>
						<label for="showproductcode">
							<?php echo JText::_("SC_SHOW_PRODUCT_CODE");?>
						</label>
					</td>
					<td class="control-group">
							<div class="controls">
								<fieldset class="radio btn-group">
									<label for="0-showproductcode"><input type="radio" id="0-showproductcode" value="0" name="showproductcode" <?php echo ($this->showproductcode==0?' checked="checked"':'');?>><?php echo JText::_('SC_HIDE');?></label>
									<label for="1-showproductcode"><input type="radio" id="1-showproductcode" value="1" name="showproductcode" <?php echo ($this->showproductcode==1?' checked="checked"':'');?> ><?php echo JText::_('SC_SHOW');?></label>
								</fieldset>
							</div>
					</td>
				</tr>

				<tr>
					<td>
						<label for="showproductcodetext">
							<?php echo JText::_("SC_PRODUCT_CODE_TEXT");?>
						</label>
					</td>
					<td class="control-group">
						<div class="controls">
							<fieldset class="radio btn-group">
								<label for="0-showproductcodetext"><input type="radio" id="0-showproductcodetext" value="0" name="showproductcodetext" <?php echo ($this->showproductcodetext==0?' checked="checked"':'');?>><?php echo JText::_('SC_HIDE');?></label>
								<label for="1-showproductcodetext"><input type="radio" id="1-showproductcodetext" value="1" name="showproductcodetext" <?php echo ($this->showproductcodetext==1?' checked="checked"':'');?> ><?php echo JText::_('SC_SHOW');?></label>
							</fieldset>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<label for="showtitle">
							<?php echo JText::_("SC_PRODUCT_TITLE");?>
						</label>
					</td>
					<td class="control-group">
						<div class="controls">
							<fieldset class="radio btn-group">
								<label for="0-showtitle"><input type="radio" id="0-showtitle" value="0" name="showtitle" <?php echo ($this->showtitle==0?' checked="checked"':'');?>><?php echo JText::_('SC_HIDE');?></label>
								<label for="1-showtitle"><input type="radio" id="1-showtitle" value="1" name="showtitle" <?php echo ($this->showtitle==1?' checked="checked"':'');?> ><?php echo JText::_('SC_SHOW');?></label>
							</fieldset>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<label for="showquantity">
							<?php echo JText::_("SC_QUANTITY_FIELD");?>
						</label>
					</td>
					<td class="control-group">
						<div class="controls">
							<fieldset class="radio btn-group">
								<label for="0-showquantity"><input type="radio" id="0-showquantity" value="0" name="showquantity" <?php echo ($this->showquantity==0?' checked="checked"':'');?>><?php echo JText::_('SC_HIDE');?></label>
								<label for="1-showquantity"><input type="radio" id="1-showquantity" value="1" name="showquantity" <?php echo ($this->showquantity==1?' checked="checked"':'');?> ><?php echo JText::_('SC_SHOW');?></label>
							</fieldset>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<?php echo JText::_("SC_QUANTITY_FIELD_TEXT");?>
					</td>
					<td class="control-group">
						<div class="controls">
							<fieldset class="radio btn-group">
								<label for="0-showquantitytext"><input type="radio" id="0-showquantitytext" value="0" name="showquantitytext" <?php echo ($this->showquantitytext==0?' checked="checked"':'');?>><?php echo JText::_('SC_HIDE');?></label>
								<label for="1-showquantitytext"><input type="radio" id="1-showquantitytext" value="1" name="showquantitytext" <?php echo ($this->showquantitytext==1?' checked="checked"':'');?> ><?php echo JText::_('SC_SHOW');?></label>
							</fieldset>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<?php echo JText::_("SC_SHOW_OPTIONS_TITLE");?>
					</td>
					<td class="control-group">
						<div class="controls">
							<fieldset class="radio btn-group">
								<label for="0-showcartoptionstitle"><input type="radio" id="0-showcartoptionstitle" value="0" name="showcartoptionstitle" <?php echo ($this->showcartoptionstitle==0?' checked="checked"':'');?>><?php echo JText::_('SC_HIDE');?></label>
								<label for="1-showcartoptionstitle"><input type="radio" id="1-showcartoptionstitle" value="1" name="showcartoptionstitle" <?php echo ($this->showcartoptionstitle==1?' checked="checked"':'');?> ><?php echo JText::_('SC_SHOW');?></label>
							</fieldset>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<?php echo JText::_("SC_SHOW_UNITPRICE");?>
					</td>
					<td class="control-group">
						<div class="controls">
							<fieldset class="radio btn-group">
								<label for="0-showunitprice"><input type="radio" id="0-showunitprice" value="0" name="showunitprice" <?php echo ($this->showunitprice==0?' checked="checked"':'');?>><?php echo JText::_('SC_HIDE');?></label>
								<label for="1-showunitprice"><input type="radio" id="1-showunitprice" value="1" name="showunitprice" <?php echo ($this->showunitprice==1?' checked="checked"':'');?> ><?php echo JText::_('SC_SHOW');?></label>
							</fieldset>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<?php echo JText::_("SC_SHOW_UNITPRICE_TEXT");?>
					</td>
					<td class="control-group">
						<div class="controls">
							<fieldset class="radio btn-group">
								<label for="0-showunitpricetext"><input type="radio" id="0-showunitpricetext" value="0" name="showunitpricetext" <?php echo ($this->showunitpricetext==0?' checked="checked"':'');?>><?php echo JText::_('SC_HIDE');?></label>
								<label for="1-showunitpricetext"><input type="radio" id="1-showunitpricetext" value="1" name="showunitpricetext" <?php echo ($this->showunitpricetext==1?' checked="checked"':'');?> ><?php echo JText::_('SC_SHOW');?></label>
							</fieldset>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<?php echo JText::_("SC_SHOW_AVAILABLE_QUANTITY");?>
					</td>
					<td class="control-group">
						<div class="controls">
							<fieldset class="radio btn-group">
								<label for="0-showavailablequantity"><input type="radio" id="0-showavailablequantity" value="0" name="showavailablequantity" <?php echo ($this->showavailablequantity==0?' checked="checked"':'');?>><?php echo JText::_('SC_HIDE');?></label>
								<label for="1-showavailablequantity"><input type="radio" id="1-showavailablequantity" value="1" name="showavailablequantity" <?php echo ($this->showavailablequantity==1?' checked="checked"':'');?> ><?php echo JText::_('SC_SHOW');?></label>
							</fieldset>
						</div>
					</td>
				</tr>

				<tr>
					<td>
						<?php echo JText::_("SC_SHOW_AVAILABLE_QUANTITYTEXT");?>
					</td>
					<td class="control-group">
						<div class="controls">
							<fieldset class="radio btn-group">
								<label for="0-showavailablequantitytext"><input type="radio" id="0-showavailablequantitytext" value="0" name="showavailablequantitytext" <?php echo ($this->showavailablequantitytext==0?' checked="checked"':'');?>><?php echo JText::_('SC_HIDE');?></label>
								<label for="1-showavailablequantitytext"><input type="radio" id="1-showavailablequantitytext" value="1" name="showavailablequantitytext" <?php echo ($this->showavailablequantitytext==1?' checked="checked"':'');?> ><?php echo JText::_('SC_SHOW');?></label>
							</fieldset>
						</div>
					</td>
				</tr>
				<?php
				$astrings=explode("\r\n", $this->skipstrings);
				$i=0;
				$extra=false;
				foreach ($astrings as $aline) {
					@list($name, $label)=explode(":", $aline);
					if (trim($aline) == "") continue;
					echo "<tr>";
					echo "<td style=\"width: 250px;\">".JText::_("SC_LABEL_NAME")."&nbsp; $i <br /><input type='text' name='skipname[$i]' value='$name' /></td>";
					echo "<td>".JText::_("SC_LABEL_STRING")."<br /><input type='text' name='skiplabel[$i]' value='$label' /></td>";
					echo "</tr>";
					$i++;

				}
				// these are for the extra line
				echo "<tr>";
				echo "<td style=\"width: 250px;\">".JText::_("SC_LABEL_NAME")."<br /><input type='text' name='skipname[$i]' value='' /></td>";
				echo "<td>".JText::_("SC_LABEL_STRING")."<br /><input type='text' name='skiplabel[$i]' value='' /></td>";
				echo "</tr>";
				?>
				<input type="hidden" name="option" value="com_simplecaddy" />
				<input type="hidden" name="action" value="pluginconfig"/>
				<input type="hidden" name="pluginname" value="simplecaddy"/>
				<input type="hidden" name="task" />
				<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
			</table>
		</form>
		<?php
	}

	function apply() {
		$this->save(true);
	}


	function save($stay=false, $orderingFilter = '', $ignore = '') { // mandatory function to save any variables
		$mainframe=JFactory::getApplication();
		$this->bind($_REQUEST); // get all the fields from the admin form
		$input=$mainframe->input;
		$skipnames=$input->get("skipname",'', "ARRAY");
		$skipstrings=$input->get("skiplabel", '', "ARRAY");
		$skips="";
		foreach($skipnames as $key=>$value) {
			$label=$skipstrings[$key];
			if ( ($value!="") and ($label!="") ) $skips.= "$value:$label\r\n";
		}
		$skips=trim($skips);
		$this->skipstrings=$skips;
		$b=$this->store(); // store the relevant fields only
		$msg=JText::_("SC_CONFIG_SAVED");
		if (!$b) $msg .= ' ' . $this->_db->getErrorMsg();
		$mainframe->enqueueMessage($msg);
		if (!$stay) {
			$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
		}
		else
		{
			$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=simplecaddy");
		}
	}

	function get($property="", $default = null) { // retrieves config key from storage

	}

	function cancel() {
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
	}
}


class plgContentSimplecaddy extends JPlugin {
	var $tsep;
	var $dsep;
	var $decs;
	var $currency;
	var $curralign;
	var $separator;
	var $thiscid;
	var $itemid;
	var $_plugin_number	= 0;

    /**
     * Affects constructor behavior. If true, language files will be loaded automatically.
     *
     * @var    boolean
     * @since  1.0
     */
    protected $autoloadLanguage = true;


	public function _setPluginNumber() {
		$this->_plugin_number = (int)$this->_plugin_number + 1;
	}

	public function onContentPrepare($context, &$article, &$params, $page = 0) {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		// first check if the component is not missing or unavailable
		if (!JComponentHelper::isEnabled('com_simplecaddy')) {
			echo "<div style='color:red;'>The SimpleCaddy component is not installed or is not enabled</div>";
			return "";
		}

		JPluginHelper::importPlugin('content');
		$dispatcher = JEventDispatcher::getInstance();
		$dispatcher->trigger('onContentSimpleCaddyShipping', array($context, &$article, &$params, 0));
		$dispatcher->trigger('onContentSimpleCaddyTax', array($context, &$article, &$params, 0));

		$menu	= $mainframe->getMenu();
		$item	= $menu->getActive();
		$this->itemid=@$item->id;

		$this->thiscid=$article->id;

        $cfg=new sc_configuration(); // read from the component configurations
        $this->tsep=$cfg->get('thousand_sep');
        $this->dsep=$cfg->get('decimal_sep');
        $this->decs=$cfg->get('decimals');
        $this->currency=$cfg->get('currency');
        $this->curralign=$cfg->get('curralign');

        $regex = '/{(simplecaddy)\s*(.*?)}/i';
//        $plugin = JPluginHelper::getPlugin('content', 'simplecaddy');

        $parms=array();
        $matches = array();
        if (isset($article->text)) {
	        @preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
        }
        else
        {
	        @preg_match_all( $regex, $article->introtext, $matches, PREG_SET_ORDER );
        }

        foreach ($matches as $elm) {
 			$this->_setPluginNumber();
 			if ($this->_plugin_number==1) { // get the stylesheet only ONCE per page
			    JHtml::_('jquery.framework');
			    $document = JFactory::getDocument();
			    $document->addStyleSheet('components/com_simplecaddy/css/simplecaddy.css');
			//	JHTML::stylesheet('components/com_simplecaddy/css/simplecaddy.css' );
		    }
			$html ="";
            $line=strip_tags($elm[2]); // get rid of most of the embedded html tags, if any. Only tags directly after the { will not be cleaned as regex will not pick those up
			$line=str_replace("&nbsp;", " ", $line);
            $line=str_replace(" ", "&", $line);
            $line=strtolower($line);
            parse_str( $line, $parms );
            if (!isset($parms['type'])) { // no type provided, so either an old style plugin or just forgot...
            	$parms["type"]="buynow";
            }
	        if (!$this->thiscid) { // not found from article, get from commandline, if available
		        $this->thiscid=@$parms["thiscid"];
	        }
	        // get all different types of display here
        	switch (strtolower($parms['type']) ) {
        		case "details":
        			$html.=$this->details($parms);
        			break;
        		case "showorderheader":
        			$html.=$this->showorderheader($parms);
        			break;
        		case "showcart":
        			$html.=$this->showcart($parms);
        			break;
        		case "showorder":
        			$html.=$this->showOrder($parms);
        			break;
        		case "editcart":
        			$html.=$this->editcart($parms);
        			break;
        		case "skip":
        			$html.=$this->skip($parms);
        			break;
       			case "category":
                    $html.=$this->getSCbyCategory($parms);
           			break;
       			case "emailorder":
                    $html.=$this->emailorder($parms);
           			break;
       			case "dllist":
                    $html.=$this->dllist($parms);
           			break;
		        case "emptycart":
			        $html.=$this->emptycart($parms);
			        break;
       			case "buynow": // the default if nothing has been provided
                	$html.=$this->getSCSingle($parms);
                	break;
        		default: // anything else provides an error message
                	$html.=JText::_("SC_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). "({$parms['type']})";
        	}

			if (isset($article->text)) {
	            $article->text = preg_replace($regex, $html, $article->text, 1);
			}
			else
			{
	            $article->introtext = preg_replace($regex, $html, $article->introtext, 1);
			}
        }
        return false;
	}

    function optionshow1($lstoptions, $product, $groupid, $classsuffix, $picname="") {// horizontal radio buttons
        $html="";
        foreach ($lstoptions as $po) {
            $optid=$po->id;
            $shorttext=$po->description;
            $formula=$po->formula;
            $caption=$po->caption;
            $defselect=$po->defselect;
            $id=md5($product->prodcode.$shorttext.$picname);
            $checked="";
            if (trim($defselect) == "1") $checked=" checked='checked' ";
            $html .= "<input class='scradio$classsuffix' type='radio' name='edtoption[$groupid]' value='$id:$optid' id='$id' $checked /><label style='display: inline;' for='$id'>".stripslashes($shorttext)."</label> $caption\n";
        }
        return $html;
    }

    function optionshow2($lstoptions, $product, $groupid, $classsuffix, $picname="") {// dropdown list
        $html="";
        $html.="<div class='scoptionselect$classsuffix'><select name='edtoption[$groupid]'>\n";
        foreach ($lstoptions as $po) {
            $optid=$po->id;
            $shorttext=$po->description;
            $formula=$po->formula;
            $caption=$po->caption;
            $defselect=$po->defselect;
            $id=md5($product->prodcode.$shorttext.$picname);
            $checked="";
            if (trim($defselect) == "1") $checked=" selected='selected' ";
            $html .= "<option value='$id:$optid' $checked>".stripslashes($shorttext)." $caption</option>\n";
        }
        $html .= "</select></div>\n";
        return $html;
    }

    function optionshow3($lstoptions, $product, $groupid, $classsuffix, $picname="") {// standard list
        $html="";
		$html.="<div class='scoptionselect$classsuffix'><select name='edtoption[$groupid]' size='10'>\n";
		foreach ($lstoptions as $po) {
            $optid=$po->id;
            $shorttext=$po->description;
            $formula=$po->formula;
            $caption=$po->caption;
            $defselect=$po->defselect;
            $id=md5($product->prodcode.$shorttext.$picname);
            $checked="";
            if (trim($defselect) == "1") $checked=" selected='selected' ";
            $html .= "<option value='$id:$optid' $checked>".stripslashes($shorttext)." $caption</option>\n";
		}
		$html .= "</select></div>\n";
        return $html;
    }

    function optionshow4($lstoptions, $product, $groupid, $classsuffix, $picname="") {// vertical radio buttons
        $html="";
		foreach ($lstoptions as $po) {
            $optid=$po->id;
            $shorttext=$po->description;
            $formula=$po->formula;
            $caption=$po->caption;
            $defselect=$po->defselect;
            $id=md5($product->prodcode.$shorttext.$picname);
            $checked="";
            if (trim($defselect) == "1") $checked=" checked='checked' ";
            $html .= "\n<input class='scradio$classsuffix' type='radio' name='edtoption[$groupid]' value='$id:$optid' id='$id' $checked /><label style='display: inline;' for='$id'>".stripslashes($shorttext)."</label>";
            $html.=" $caption<br />";
		}
        return $html;
    }

    function optionshow5($lstoptions, $product, $groupid, $classsuffix, $picname="") {// free text
        $html="";
		$html.="<input type='text' name='edtoption[$groupid]'>\n";
		$html.="<input type='hidden' name='edtoption2[$groupid]' value='$groupid'>\n";
        return $html;
    }

    function optionshow6($lstoptions, $product, $groupid, $picname="") {// calendar control
        $html="";
		$html.="<input type='hidden' name='edtoption2[$groupid]' value='$groupid'>\n";
	    $html .=  JHTML::calendar("","edtoption[$groupid]","edtoption[$groupid]",'%d-%m-%Y');
	    return $html;
    }

	function optionshow7($lstoptions, $product, $groupid, $classsuffix, $picname="") {// radio buttons with images
		$html="";
		foreach ($lstoptions as $po) {
			$optid=$po->id;
			$shorttext=$po->description;
			$formula=$po->formula;
			$caption=$po->caption;
			$defselect=$po->defselect;
			$optionimg=$po->image;
			$optionclass=$po->classname;
			$id=md5($product->prodcode.$shorttext.$picname);
			$checked="";
			if (trim($defselect) == "1") $checked=" checked='checked' ";

			$html .= "<div class='scradio$classsuffix'><label for='$id'><input class='scradio$classsuffix' type='radio' id='$id' name='edtoption[$groupid]' value='$id:$optid' $checked /><img src='$optionimg' class='$optionclass' title='$shorttext'/></label></div>\n";

		}
		return $html;
	}



	function getSCSingle($params) {
		global $Itemid;
		$classsuffix=isset($params['classsfx']) ? $params['classsfx'] : "";
		$prodcode=@$params['code'];
		$defaultqty=isset($params['defqty']) ? $params['defqty'] : 1; // default qty set in qty edit box
		$minqty=isset($params['minqty']) ? $params['minqty'] : 0;
		$strqties=isset($params['qties']) ? $params['qties'] : null;
		$checkoos=isset($params['checkoos']) ? $params['checkoos'] : 0;
		$picname=isset($params['picname']) ? $params['picname'] : "";
		$nextcid=isset($params['nextcid']) ? $params['nextcid'] : null; //$this->thiscid;
		$aqties=explode(",", $strqties);

		$pcfg=new simplecaddyconfiguration();

		$db	= JFactory::getDBO();
		$query=$db->getQuery(true);
		$query->select("*");
		$query->from("#__sc_products");
		$query->where("prodcode='$prodcode'");
		$db->setQuery( $query );
		$product = $db->loadObject();

		if (@!$product->id) {
			$html  ="<div class='sccart$classsuffix'>";
            $str= JText::sprintf("SC_PRODUCT_NOT_FOUND", $prodcode);
			$html .= $str;
			$html .="</div>";
			return $html;
		}

		if ($product->published=='0') {
			$html  ="<div class='sccart$classsuffix'>";
            $str= JText::sprintf("SC_PRODUCT_NOT_PUBLISHED", $prodcode);
			$html .= $str;
			$html .="</div>";
			return $html;
		}

		if (!$db->execute()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}

        //Formatters
        $amount = number_format($product->unitprice, $this->decs, $this->dsep, $this->tsep);
        $product->shorttext = stripslashes( $product->shorttext );

        //Get Options
        $optgroups=new optiongroups();
        $optiongroups=$optgroups->getgroups($product->id);

        /*Open Graph
		$doc = JFactory::getDocument();

		$uri = & JFactory::getURI();
		$absolute_url = $uri->toString();

		$doc->setMetaData('og:title', $product->shorttext, 'property');
		$doc->setMetaData('og:type', 'product', 'property');
		$doc->setMetaData('og:url', $absolute_url, 'property');
		$doc->setMetaData('og:image', rtrim(JURI::base(), '/').$product->filename, 'property');
        */


        $html ='';
        // Get the path for the layout file
        $path = JPluginHelper::getLayoutPath('content', 'simplecaddy', 'product');
        ob_start();
        include $path;
        $html .= ob_get_clean();


        return $html;
    }

	function getSCbyCategory($params) {
		global $Itemid, $mainframe;
		$category=$params['category'];
		$defaultqty=isset($params['defqty']) ? $params['defqty'] : 1;
		$classsuffix=isset($params['classsfx']) ? $params['classsfx'] : "";
		$nextcid=isset($params['nextcid']) ? $params['nextcid'] : $this->thiscid;

		$pcfg=new simplecaddyconfiguration();

		$db	= JFactory::getDBO();
		$query=$db->getQuery(true);
		$query->select("*");
		$query->from("#__sc_products");
		$query->where("LCASE(`category`)='$category'");
		$query->where("`published`=1");
		$db->setQuery( $query );
		$lstproduct = $db->loadObjectList();

		if (!$lstproduct) {
			$html  ="<div class='sccart$classsuffix'>";
			$html .="<h3>The category ($category) is not found.</h3>";
			$html .="</div>";
			return $html;
		}

		if (!$db->execute()) {
			echo "<script> alert('".$db->getErrorMsg()."'); window.history.go(-1); </script>\n";
			exit();
		}
		$html  ="";

        foreach ($lstproduct as $product) {
            //Formatters
            $amount = number_format($product->unitprice, $this->decs, $this->dsep, $this->tsep);
            $product->shorttext = stripslashes( $product->shorttext );

            //Get Options
            $optgroups=new optiongroups();
            $optiongroups=$optgroups->getgroups($product->id);

            // Get the path for the layout file
            $path = JPluginHelper::getLayoutPath('content', 'simplecaddy', 'product');
            ob_start();
            include $path;
            $html .= ob_get_clean();
        }
        return $html;
    }

	function editCart($params) {
	    $mainframe=JFactory::getApplication();
	    $thiscid=$this->thiscid; // article ID, used to stay on page
		$nextcid=isset($params['nextcid']) ? $params['nextcid'] : $thiscid; // next page to go to, defaults to stay on this page

		$pcfg=new simplecaddyconfiguration();

		$cart2=new cart2();
		$cart=$cart2->readCart();

		$showhidden="hidden";
			$cfg=new sc_configuration();
			$tsep=$cfg->get('thousand_sep');
			$dsep=$cfg->get('decimal_sep');
			$decs=$cfg->get('decimals');
			$currency=$cfg->get('currency');
			$curralign=$cfg->get('curralign');
			$showremove=$cfg->get('remove_button');
			$show_emptycart=$cfg->get('show_emptycart');
			$currency=$cfg->get('currency');
			$currleftalign=$cfg->get('curralign');
			$stdprodcode=$cfg->get("cart_fee_product");

        $gtotal=0;
        $html ="";

        $emptycart=true;

        if (!is_array($cart))
            $cart=array();

        foreach ($cart as $key=>$cartproduct){
            $total=$cartproduct->quantity*$cartproduct->finalprice;
            $gtotal= $gtotal + $total;
            if ($cartproduct->quantity)
                $emptycart=false;
        }

        if ($emptycart) {
            $html = "<div>".JText::_('SC_CART_EMPTY')."</div>";
        }else {
            $cartformstart = "<form name='checkout{$this->_plugin_number}' method='post'>";
            $cartformend  = "<input type='hidden' name='option' value='com_simplecaddy'>";
            $cartformend .= "<input type='hidden' name='action' value='cart'>";
            $cartformend .= "<input type='hidden' name='task' value='checkout'>";
            $cartformend .= "<input type='hidden' name='nextcid' value='$nextcid'>";
            $cartformend .= "<input type='hidden' name='thiscid' value='$thiscid'>";
            $cartformend .= "</form>";

            // Get the path for the layout file
            $path = JPluginHelper::getLayoutPath('content', 'simplecaddy', 'editcart');
            ob_start();
            include $path;
            $html .= ob_get_clean();
        }
        return $html;
    }

	function showCart($params) {
		$thiscid=$this->thiscid; // article ID, used to stay on page
		$nextcid=isset($params['nextcid']) ? $params['nextcid'] : $thiscid; // next page to go to, defaults to stay on this page
		$showbuttons=isset($params['showbuttons']) ? $params['showbuttons'] : 1; // show the buttons in the bottom of the read only cart
		$cart2=new cart2();
		$cart=$cart2->readCart();
		if (!is_array($cart)) $cart=array();

		$cfg=new sc_configuration();
		$tsep=$cfg->get('thousand_sep');
		$dsep=$cfg->get('decimal_sep');
		$decs=$cfg->get('decimals');
		$currency=$cfg->get('currency');
		$curralign=$cfg->get('curralign');
		$show_emptycart=$cfg->get('show_emptycart');
		$currency=$cfg->get('currency');
		$currleftalign=$cfg->get('curralign');

        $gtotal=0;
        $html ="";

        $emptycart=true;

        foreach ($cart as $key=>$cartproduct){
            $total=$cartproduct->quantity*$cartproduct->finalprice;
            $gtotal= $gtotal + $total;
            if ($cartproduct->quantity)
                $emptycart=false;
        }


        if ($emptycart) {
            $html ="<div>";
            $html .=JText::_('SC_CART_EMPTY');
            $html .= "</div>";
        }else{
            $cartformstart = "<form name='checkout{$this->_plugin_number}' method='post'>";
            $cartformend  = "<input type='hidden' name='option' value='com_simplecaddy'>";
            $cartformend .= "<input type='hidden' name='action' value='cart'>";
            $cartformend .= "<input type='hidden' name='task' value='gotopayment'>";
            $cartformend .= "<input type='hidden' name='nextcid' value='$nextcid'>";
            $cartformend .= "<input type='hidden' name='thiscid' value='$thiscid'>";
            $cartformend .= "</form>";

            // Get the path for the layout file
            $path = JPluginHelper::getLayoutPath('content', 'simplecaddy', 'showcart');
            ob_start();
            include $path;
            $html .= ob_get_clean();
        }
        return $html;
    }

	function showOrder($params) {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$thiscid=$this->thiscid; // article ID, used to stay on page
		$nextcid=isset($params['nextcid']) ? $params['nextcid'] : $thiscid; // next page to go to, defaults to stay on this page
		$showbuttons=isset($params['showbuttons']) ? $params['showbuttons'] : 1; // show the buttons in the bottom of the read only cart

		$ordercode=$input->get("data"); // the data contains the ordercode when you finish the details page

		if (!$ordercode) {
			return "";
		}


		$orders=new orders();
		$orderid=$orders->getOrderIdFromCart($ordercode); // now we have an order ID
		$order= new order();
		$order->load($orderid);

		$details=new orderdetail();
		$lst=$details->getDetailsByOrderId($orderid);

		$cfg=new sc_configuration();
		$tsep=$cfg->get('thousand_sep');
		$dsep=$cfg->get('decimal_sep');
		$decs=$cfg->get('decimals');
		$currency=$cfg->get('currency');
		$curralign=$cfg->get('curralign');
		$show_emptycart=$cfg->get('show_emptycart');
		$currency=$cfg->get('currency');
		$currleftalign=$cfg->get('curralign');
		$itemseparator = $cfg->get('itemseparator');

        $gtotal=0;

        foreach ($lst as $key=>$cartproduct){
            $total=$cartproduct->quantity*$cartproduct->finalprice;
            $gtotal= $gtotal + $total;
            if ($cartproduct->quantity)
                $emptycart=false;
        }
        if($order->shipCost>0)
            $gtotal=$gtotal+$order->shipCost;

        if(@$order->discount<0)
            $gtotal=$gtotal+$order->discount;
        if($order->tax>0)
            $gtotal=$gtotal+$order->tax;


        $html ="";


        $cartformstart = "<form name='checkout{$this->_plugin_number}' method='post'>";
        $cartformend  = "<input type='hidden' name='option' value='com_simplecaddy'>";
        $cartformend .= "<input type='hidden' name='action' value='cart'>";
        $cartformend .= "<input type='hidden' name='task' value='gotopayment'>";
        $cartformend .= "<input type='hidden' name='nextcid' value='$nextcid'>";
        $cartformend .= "<input type='hidden' name='thiscid' value='$thiscid'>";
        $cartformend .= "</form>";



        // Get the path for the layout file
        $path = JPluginHelper::getLayoutPath('content', 'simplecaddy', 'order');
        ob_start();
        include $path;
        $html .= ob_get_clean();

        return $html;
    }

	function emailorder() {
		$ordercode=JRequest::getVar("data"); // the data contains the ordercode when you finish the details page
        if(!isset($ordercode))
            return "";
		$orders=new orders();
		$orderid=$orders->getOrderIdFromCart($ordercode);
		$mail=new email();
		$result=$mail->mailorder($orderid); // should be 1 for successful email
		return "";
	}

	function skip($params) {
	    $thiscid=$this->thiscid; // article ID, used to stay on page
		$nextcid=isset($params['nextcid']) ? $params['nextcid'] : $thiscid; // next page to go to, defaults to stay on this page
		$html  = "<form name='frmSkip' method='post' action=".JRoute::_('index.php').">";
		$html .= "\n<input type='hidden' name='option' value='com_simplecaddy'>";
		$html .= "\n<input type='hidden' name='action' value='cart'>";
		$html .= "\n<input type='hidden' name='task' value='skip'>";
		$html .= "\n<input type='hidden' name='nextcid' value='$nextcid'>";
		$html .= "\n<input type='button' class='btnorder' value='".JText::_('SC_SKIP')."' onclick='javascript:document.frmSkip.submit()'>";
		$html .= "</form>";
		return $html;
	}

	function details($params) { //$formfields, $errormessage=null, $fielddata=array()
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$thiscid=$this->thiscid; // article ID, used to stay on page
		$nextcid=isset($params['nextcid']) ? $params['nextcid'] : $thiscid; // next page to go to, defaults to stay on this page
		$encdatafields=$input->get("data", "", "RAW");
		$fielddata=array();
		if (@$encdatafields!="") { // do a few checks for validity against hacking...
			$b64data=urldecode($encdatafields);
			$res=unserialize(base64_decode($b64data));
			if (is_array($res) ) { //  this must be an array!
				if (@$res["herkomst"]!="simplecaddy") { // valid array, posted from somewhere else
					$fielddata=array(); // wipe it out
				}
				else
				{
					$fielddata=$res; // all ok, transfer the data to the fields, so we don't have to fill in everything again
				}
			}
			else { // data is not an array, that is wrong so force it
				$fielddata=array();
			}
		}


        $fields=new fields();
        $formfields=$fields->getPublishedFields();
        $user=JFactory::getUser(); // get the Joomla logged in user
        $userId=$user->id;
        // fielddata is an array containing field names as key and values.
        // fieldnames can be custom field names
        // here is also the moment to get info from Community Builder

        if ($user->id) {
            $db=JFactory::getDbo();
            $query=$db->getQuery(true);
            $query->select("*");
            $query->from("#__user_profiles");
            $query->where("user_id = ".(int) $userId);
            $query->where("profile_key LIKE 'profile.%'");
            $query->order("ordering");
            $db->setQuery($query);
            $results = $db->loadObjectList();

            $fielddata['username']=$user->username;
            $fielddata['name']=$user->name;
            $fielddata['email']=$user->email;
            foreach ($results as $profile) {
                $key=str_replace("profile.", "", $profile->profile_key); // remove the profile. part
                $fielddata[$key]=str_replace('"', "", $profile->profile_value); // remove any quotes
            }
        }


        JHTML::_('behavior.formvalidation');
        JHTML::_('behavior.formvalidator');

        $startform = "<form name='frmdetails' method='post' class='form-validate'>";

        $endform = "<input type='hidden' name='ipaddress' value='". $_SERVER['REMOTE_ADDR'] ."'/>";
        $endform .= "<input type='hidden' name='option' value='com_simplecaddy' />";
        $endform .= "<input type='hidden' name='action' value='cart' />";
        $endform .= "<input type='hidden' name='task' value='allconfirm' />";
        $endform .= "<input type='hidden' name='thiscid' value='$thiscid' />";
        $endform .= "<input type='hidden' name='nextcid' value='$nextcid' />";
        $endform .= "<input type='hidden' name='herkomst' value='simplecaddy' />";
        $endform .= "</form>";

        $html = "";

        // Get the path for the layout file
        $path = JPluginHelper::getLayoutPath('content', 'simplecaddy', 'details');
        ob_start();
        include $path;
        $html .= ob_get_clean();

        return $html;
    }



	function details0($params) { //$formfields, $errormessage=null, $fielddata=array()
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
	    $thiscid=$this->thiscid; // article ID, used to stay on page
		$nextcid=isset($params['nextcid']) ? $params['nextcid'] : $thiscid; // next page to go to, defaults to stay on this page
		$encdatafields=$input->get("data", "", "RAW");
		$fielddata=array();
		if (@$encdatafields!="") { // do a few checks for validity against hacking...
			$b64data=urldecode($encdatafields);
			$res=unserialize(base64_decode($b64data));
			if (is_array($res) ) { //  this must be an array!
				if (@$res["herkomst"]!="simplecaddy") { // valid array, posted from somewhere else
					$fielddata=array(); // wipe it out
				}
				else
				{
					$fielddata=$res; // all ok, transfer the data to the fields, so we don't have to fill in everything again
				}
			}
			else { // data is not an array, that is wrong so force it
				$fielddata=array();
			}
		}

		$fields=new fields();
		$formfields=$fields->getPublishedFields();
		$user=JFactory::getUser(); // get the Joomla logged in user
		$userId=$user->id;
		// fielddata is an array containing field names as key and values.
		// fieldnames can be custom field names
		// here is also the moment to get info from Community Builder

		if ($user->id) {
			$db=JFactory::getDbo();
			$query=$db->getQuery(true);
			$query->select("*");
			$query->from("#__user_profiles");
			$query->where("user_id = ".(int) $userId);
			$query->where("profile_key LIKE 'profile.%'");
			$query->order("ordering");
			$db->setQuery($query);
			$results = $db->loadObjectList();

			$fielddata['username']=$user->username;
			$fielddata['name']=$user->name;
			$fielddata['email']=$user->email;
			foreach ($results as $profile) {
				$key=str_replace("profile.", "", $profile->profile_key); // remove the profile. part
				$fielddata[$key]=str_replace('"', "", $profile->profile_value); // remove any quotes
			}
		}

        JHTML::_('behavior.formvalidation');
        JHTML::_('behavior.formvalidator');

        $startform = "<form name='frmdetails' method='post' class='form-validate'>";

        $endform .= "<input type='hidden' name='ipaddress' value='". $_SERVER['REMOTE_ADDR'] ."'/>";
        $endform .= "<input type='hidden' name='option' value='com_simplecaddy' />";
        $endform .= "<input type='hidden' name='action' value='cart' />";
        $endform .= "<input type='hidden' name='task' value='allconfirm' />";
        $endform .= "<input type='hidden' name='thiscid' value='$thiscid' />";
        $endform .= "<input type='hidden' name='nextcid' value='$nextcid' />";
        $endform .= "<input type='hidden' name='herkomst' value='simplecaddy' />";
        $endform .= "</form>";

        $html = "";

        // Get the path for the layout file
        $path = JPluginHelper::getLayoutPath('content', 'simplecaddy', 'details');
        ob_start();
        include $path;
        $html .= ob_get_clean();

        return $html;
    }

	function _keyform() {
		$html  = JText::_('SC_ENTER_DL_KEY');;
		$html .= '<div>';
		$html .= '<form name="keyform" method="post" action="index.php">';
		$html .= '<input type="text" name="dlkey" size="50" />';
		$html .= '<input type="submit" name="submit" value="Go" />';
		$html .= '<input type="hidden" name="option" value="com_simplecaddy" />';
		$html .= '<input type="hidden" name="action" value="scdl" />';
		$html .= '<input type="hidden" name="task" value="view" />';
		$html .= '</form>';
		$html .= '</div>';
		return $html;
	}

	function dllist() {
		$input=JFactory::getApplication()->input;

		$key=$input->get("dlkey");
		if (empty($key)) {
			return $this->_keyform();
		}
		$dli=new scdl_items();
		$lst=$dli->getlist($key);
		$this->data->dlitems=$lst;
		$target="";
		$stype="";
		$html ="";
		$lst=$this->data->dlitems;

        $cfg=new sc_configuration();
        $dlpath=$cfg->get("downloadpath");

        $html = "";
        // Get the path for the layout file
        $path = JPluginHelper::getLayoutPath('content', 'simplecaddy', 'dlist');
        ob_start();
        include $path;
        $html .= ob_get_clean();

        return $html;
    }

	function showOrderHeader() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$ordercode=$input->get("data"); // the data contains the ordercode when you finish the details page
		$orders=new orders();
		$orderid=$orders->getOrderIdFromCart($ordercode);

		$cfg=new sc_configuration();
		$dateformat=$cfg->get('dateformat');
		$timeformat=$cfg->get('timeformat');

		$order=new order();
		$order->load($orderid);
		$header=file_get_contents(dirname(__FILE__)."/views/orderheader.html");
		$header=str_replace("#orderdatetime#", date("$dateformat $timeformat", $order->orderdt), $header);
		$header=str_replace("#orderid#", $order->id, $header);
		$cfields="";
		$fields=new fields();
		$fieldslist=$fields->getPublishedFields() ;// the custom fields defined for this system
		$thefields=unserialize($order->customfields); // the fields filled by customers also contain standard fields
		foreach ($fieldslist as $key=>$customfield) {
			$cfields.="<tr><td>$customfield->name</td><td>".$thefields[$customfield->name]."</td></tr>";
			// the following will replace all custom fields that are in the file after developer modification
			$header=str_replace("#$customfield->name#", $thefields[$customfield->name], $header);
		}

		$header=str_replace("#clientinfo#", $cfields, $header);
		$html=$header;
		return $html;
	}

	function emptycart($params) {
		$cart2=new cart2();
		$cart2->destroyCart();
		return "";
	}


}

?>