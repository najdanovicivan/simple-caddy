<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.altsbergcaddy
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>
<div class='sccart<?= $classsuffix?>'>
    <form name='addtocart<?= $product->id ?>' action='index.php' method='post'>
    <?php if ($pcfg->showproductcodetext)
        echo "<div class='scproduct$classsuffix'>".JText::_('SC_PRODUCT')." $this->separator</div>";
    if ($pcfg->showproductcode)
        echo "<div class='scprodcode$classsuffix'>$product->prodcode $this->separator</div>";
    if ($pcfg->showtitle)
        echo "<div class='scshorttext$classsuffix'>$product->shorttext $this->separator</div>";
    if ($pcfg->showunitpricetext)
        echo "<div class='scunitpricetext$classsuffix'>".JText::_('SC_PRICE_PER_UNIT')." $this->separator</div>";

    if ($pcfg->showunitprice) {
        if ($this->curralign == 1)
            $amount = $this->currency . "&nbsp;" . $amount;
        else
            $amount = $amount . "&nbsp;" . $this->currency;

        echo "<div class='scunitprice$classsuffix'>$amount $this->separator</div>";
    }
    if ($pcfg->showquantitytext)
        echo "<div class='scqtytext$classsuffix'>".JText::_('SC_QUANTITY')." $this->separator</div>";


    if ($strqties):?>
        <select name='edtqty' class='scp_selectqty<?= $classsuffix ?>'>
            <? foreach ($aqties as $key=>$value) {
                echo "<option value='$value' ".($value==$defaultqty?" selected":"").">$value</option>";
            }?>
        </select>
    <?php else: ?>
        <div class='scqty<?= $classsuffix ?>'>
            <input type='<?= ($pcfg->showquantity?'text':'hidden') ?>' name='edtqty' value='<?= $defaultqty ?>' class='scp_qty<?= $classsuffix ?>' />
            <?= $this->separator ?>
        </div>
    <?php endif;

     if ($pcfg->showavailablequantitytext)
        echo "<div class='scavqtytext$classsuffix'>".JText::_("SC_AVAILABLE_QTY")." $this->separator</div>";
     if ($pcfg->showavailablequantity)
        echo "<div class='scavqty$classsuffix'>$product->av_qty $this->separator</div>";


     //Product Options part


    echo "<div class='cartoptions$classsuffix'>";
    if ($pcfg->showcartoptionstitle)
        echo "<div class='cartoptionstitle$classsuffix'>".JText::_('SC_OPTIONS')."</div>";
    foreach ($optiongroups as $optiongroup) {
        $groupid=$optiongroup->id;
        $options=new productoption();
        $lstoptions=$options->getbygroup($groupid);
        $show="optionshow".$optiongroup->showas;

        if ($pcfg->showcartoptionstitle)
            echo "<div class='cartoptionstitle$classsuffix'>{$optiongroup->title}</div>";

        echo $this->$show($lstoptions, $product, $groupid, $classsuffix, $picname);
    }
    echo "</div>";



    ?>

        <div class='atczone<?= $classsuffix ?>'>
            <input type='submit' name='submit' class='scp_atc<?= $classsuffix ?>' value='<?= ($checkoos==1 && $product->av_qty<1) ? JText::_('SC_OUT_OF_STOCK')."' disabled='disabled'" : JText::_('SC_ADD_TO_CART') ?>' ./>
        </div>

        <input type='hidden' name='edtprodcode' value='<?= $product->prodcode ?>' />
        <input type='hidden' name='edtshorttext' value='<?= $product->shorttext ?>' />
        <input type='hidden' name='edtunitprice' value='<?= $product->unitprice ?>' />
        <input type='hidden' name='option' value='com_simplecaddy' />
        <input type='hidden' name='action' value='cart' />
        <input type='hidden' name='task' value='add' />
        <input type='hidden' name='picname' value='<?= $picname ?>' />
        <input type='hidden' name='nextcid' value='<?= $nextcid ?>' />
        <input type='hidden' name='Itemid' value='<?= $Itemid ?>' />
        <input type='hidden' name='thiscid' value='<?= $this->thiscid ?>' />
        <?php if ($minqty>0) : // check for minimum quantity in the component ?>
           <input type='hidÍden' name='minqty' value='<?= $minqty ?>' />
        <?php endif; ?>
    </form>
</div>