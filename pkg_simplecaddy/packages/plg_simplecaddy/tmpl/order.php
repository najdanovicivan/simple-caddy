<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.altsbergcaddy
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<div class='sc_cart'>
    <div class='cartheading'>
        <div class='code_col'><?= JText::_('SC_CODE') ?></div>
        <div class='desc_col'><?= JText::_('SC_DESCRIPTION') ?></div>
        <div class='price_col'><?= JText::_('SC_PRICE_PER_UNIT') ?></div>
        <div class='qty_col'><?= JText::_('SC_QUANTITY') ?></div>
        <div class='total_col'><?= JText::_('SC_TOTAL') ?></div>
        <div class='actions_col'>&nbsp;</div>
    </div>

    <?php foreach ($lst as $key=>$cartproduct) :

        $pu = number_format($cartproduct->finalprice, $decs, $dsep, $tsep);
        $total=$cartproduct->quantity*$cartproduct->finalprice;
        $nombre_format = number_format($total, $decs, $dsep, $tsep);

        if (!$cartproduct->quantity) continue; ?>

        <div class='code_col'><?= $cartproduct->prodcode ?></div>
        <?= "<div class='desc_col'>".urldecode($cartproduct->prodname) . urldecode($cartproduct->option)."</div>" ?>
        <div class='price_col'><?= ($currleftalign==1) ? $currency."&nbsp;".$pu : $pu."&nbsp;".$currency ?></div>
        <div class='qty_col'><?= $cartproduct->quantity ?></div>
        <div class='total_col'><?= ($currleftalign==1) ? $currency."&nbsp;".$nombre_format : $nombre_format."&nbsp;".$currency ?></div>

    <?php endforeach; ?>


    <?php if($order->shipCost>0 or $order->tax>0 or @$order->discount<0) : ?>"
        <div class='fill_col'>
            <div class='text_left'><?= JText::_('SC_SUBTOTAL') ?></div>
            <div class='text_right'>
                <?= ($currleftalign == 1) ? $currency."&nbsp;".number_format($gtotal, $decs, $dsep, $tsep) : number_format($gtotal, $decs, $dsep, $tsep)."&nbsp;".$currency ?>
            </div>
        </div>
    <?php endif; ?>


    <?php if($order->shipCost>0) : ?>
        <div class='fill_col'>
            <div class='text_left'><?= JText::_('SC_SHIPPING')." ".$order->shipper ?></div>
            <div class='text_right'>
                <?= ($currleftalign==1) ? $currency."&nbsp;".number_format($order->shipCost, $decs, $dsep, $tsep) : number_format($order->shipCost, $decs, $dsep, $tsep)."&nbsp;".$currency ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if(@$order->discount<0) : ?>
        <div class='fill_col'>
            <div class='text_left'><?= JText::_('SC_DISCOUNT')." ".$order->shipper ?></div>
            <div class='text_right'>
                <?= ($currleftalign==1) ? $currency."&nbsp;".number_format($order->discount, $decs, $dsep, $tsep) : number_format($order->discount, $decs, $dsep, $tsep)."&nbsp;".$currency ?>
            </div>
        </div>
    <?php endif; ?>


    <?php if($order->tax>0) : ?>
        <div class='fill_col'>
            <div class='text_left'><?= JText::_('SC_DISCOUNT')." ".$order->shipper ?></div>
            <div class='text_right'>
                <?= ($currleftalign==1) ? $currency."&nbsp;".number_format($order->tax, $decs, $dsep, $tsep) : number_format($order->tax, $decs, $dsep, $tsep)."&nbsp;".$currency ?>
            </div>
        </div>
    <?php endif; ?>



    <div class='fill_col'>
        <div class='text_left'><? JText::_('SC_TOTAL') ?></div>
        <div class='text_right'>
            <?= ($currleftalign==1) ? "$currency&nbsp;".number_format($gtotal, $decs, $dsep, $tsep) : number_format($gtotal, $decs, $dsep, $tsep)."&nbsp;$currency" ?>
        </div>
    </div>

    <?php if ($showbuttons==1) : ?>
        <?= $cartformstart ?>
        <div class='cartactions'>
            <input class='btnorder' type='button' value='<?= JText::_('SC_PROCEED_ORDER') ?>' onclick='javascript:document.checkout<?= $this->_plugin_number ?>.submit()'>
        </div>
        <?= $cartformend ?>
    <?php endif; ?>
</div>