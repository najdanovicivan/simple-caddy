<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.altsbergcaddy
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<div class='sc_cart'>
    <div class='cartheading'>
        <div class='code_col'><?= JText::_('SC_CODE') ?></div>
        <div class='desc_col'><?= JText::_('SC_DESCRIPTION') ?></div>
        <div class='price_col'><?= JText::_('SC_PRICE_PER_UNIT') ?></div>
        <div class='qty_col'><?= JText::_('SC_QUANTITY') ?></div>
        <div class='total_col'><?= JText::_('SC_TOTAL') ?></div>
        <div class='actions_col'>&nbsp;</div>
    </div>


<?php foreach ($cart as $key=>$cartproduct) :

    $pu = number_format($cartproduct->finalprice, $decs, $dsep, $tsep);
    $formname=uniqid("Z");
    $total=$cartproduct->quantity*$cartproduct->finalprice;
    $nombre_format = number_format($total, $decs, $dsep, $tsep);

    if (!$cartproduct->quantity) continue; ?>

    <form name='<?= $formname ?>' method='post'>
        <div class='code_col'><?= $cartproduct->prodcode ?></div>
        <?= "<div class='desc_col'>".urldecode($cartproduct->prodname) . urldecode($cartproduct->option)."</div>" ?>

        <div class='price_col'>
            <?= ($currleftalign==1) ? $currency."&nbsp;".$pu : $pu."&nbsp;".$currency ?>
        </div>

        <div class='qty_col'>
            <input type='text' name='edtqty' value='<?= $cartproduct->quantity ?>' class='sc_edtqty'>
        </div>
        <div class='total_col'>
            <?= ($currleftalign==1) ? $currency."&nbsp;".$nombre_format : $nombre_format."&nbsp;".$currency ?>
            <input type='hidden' name='productid' value='<?= $cartproduct->id ?>'>
        </div>
        <div class='actions_col'>
        <?php if ($cartproduct->prodcode!=$stdprodcode and ($cartproduct->prodcode!="voucher")) : ?>
            <input type='button' name='btnsubmit' value='<?= JText::_('SC_CHANGE') ?>' class='btnchange' onclick='javascript:document.<?= $formname ?>.nextcid.value="<?= $thiscid ?>";document.<?= $formname ?>.submit()' />
        <?php endif ;?>

        <?php if ($showremove==1 and ($cartproduct->prodcode!=$stdprodcode) and ($cartproduct->prodcode!="voucher")) : ?>
            <input type='button' name='btnremove' value='<?= JText::_('SC_REMOVE') ?>' class='btnremove' onclick='javascript:document.<?= $formname ?>.edtqty.value=0;javascript:document.<?= $formname ?>.nextcid.value="<?= $thiscid ?>";javascript:document.<?= $formname ?>.submit()' />
        <?php endif ;?>
        </div>

        <input type='hidden' name='option' value='com_simplecaddy' />
        <input type='hidden' name='action' value='cart' />
        <input type='hidden' name='task' value='changeqty' />
        <input type='hidden' name='nextcid' value='<?= $nextcid ?>' />
        <input type='hidden' name='thiscid' value='<?= $thiscid ?>' />
        <input type='hidden' name='edtid' value='<?= $cartproduct->md5id ?>' />
        <input type='hidden' name='edtprodcode' value='<?= $cartproduct->prodcode ?>' />
        <input type='hidden' name='edtunitprice' value='<?= $cartproduct->unitprice ?>' />
        <input type='hidden' name='edtshorttext' value='<?= $cartproduct->prodname ?>' />
        <input type='hidden' name='edtoption' value='<?= $cartproduct->option ?>' />

    </form>
<?php endforeach; ?>


    <div class='fill_col'>
        <div class='text_left'><? JText::_('SC_TOTAL') ?></div>
        <div class='text_right'>
            <?= ($currleftalign==1) ? "$currency&nbsp;".number_format($gtotal, $decs, $dsep, $tsep) : number_format($gtotal, $decs, $dsep, $tsep)."&nbsp;$currency" ?>
        </div>
    </div>

    <?= $cartformstart ?>
        <div class='cartactions'>
        <?php if ($show_emptycart==1) : ?>
            <input type='button' name='btnemptycart' value='<?= JText::_('SC_EMPTY_CART') ?>' class='btnemptycart' onclick='javascript:document.checkout<?= $this->_plugin_number ?>.action.value="cart";javascript:document.checkout<?= $this->_plugin_number ?>.task.value="scempty";javascript:document.checkout<?= $this->_plugin_number ?>.nextcid.value="<?= $this->thiscid ?>";javascript:document.checkout<?= $this->_plugin_number ?>.submit()'>
        <?php endif ;?>
            <input class='btnorder' name='btnorder' type='button' value='<?= JText::_('SC_ORDER') ?>' onclick='javascript:document.checkout<?= $this->_plugin_number ?>.submit()'>
        </div>
    <?= $cartformend ?>
</div>