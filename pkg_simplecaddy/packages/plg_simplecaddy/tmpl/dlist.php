<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.altsbergcaddy
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<?= (count($lst)>1) ? JText::_('SC_REQUESTED_FILES') : JText::_('SC_REQUESTED_FILE') ?>
<table class="downloadtable">';
    <?php foreach ($lst as $dlobj) : ?>
    <tr>
        <td>
            <a href='<? $dlpath ."/". $dlobj->filename ?>'><?= $dlobj->filename ?></a>";
        </td>
    </tr>
    <?php endforeach; ?>
</table>
