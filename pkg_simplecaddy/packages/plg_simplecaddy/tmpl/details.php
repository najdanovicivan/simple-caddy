

<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.altsbergcaddy
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die  ?>

<?= $startform ?>
    <div class='sc_details'>
<?php foreach ($formfields as $field) {
    switch($field->type) {
        case "divider": // simple line with text, no fields
            $html .= "<div class='$field->classname'><div>". JText::_($field->caption);
            break;
        case "text": // textbox field, single line
            $html .= "<div><div>". JText::_($field->caption).($field->required ? "<span class='reqfield'>".JText::_('SC_REQUIRED')."</span>" : "")."</div><div>";
            $html .= "<input type='text' name='$field->name' size='$field->length' class='$field->classname ".($field->required?"required":"")."' value='". @$fielddata["$field->name"]."'>";
            break;
        case "textarea": // multiline textbox/textarea, no wysiwyg editor
            $html .= "<div><div>".JText::_($field->caption).($field->required ? "<span class='reqfield'>".JText::_('SC_REQUIRED')."</span>" : "")."</div><div>";
            @list($cols, $rows)=explode(",", $field->length);
            $html .= "<textarea name='$field->name' class='$field->classname ".($field->required?"required":"")."' cols='$cols' rows='$rows'>". @$fielddata["$field->name"]."</textarea>";
            break;
        case "radio": // yes/no radio buttons
            $html .= "<div><div>".JText::_($field->caption).($field->required ? "<span class='reqfield'>".JText::_('SC_REQUIRED')."</span>" : "")."</div><div>";
            $html .= "<input type='radio' name='$field->name' class='$field->classname ".($field->required?"required":"")."' value='yes' ". (@$fielddata["$field->name"]=="yes"?"checked":"").">". JText::_('JYES');
            $html .= "<input type='radio' name='$field->name' class='$field->classname ".($field->required?"required":"")."' value='no' ". (@$fielddata["$field->name"]=="no"?"checked":"").">". JText::_('JNO');
            break;
        case "checkbox": // single checkbox
            $html .= "<div><div>".JText::_($field->caption).($field->required ? "<span class='reqfield'>".JText::_('SC_REQUIRED')."</span>" : "")."</div><div>";
            $html .= "<input type='checkbox' name='$field->name' class='$field->classname ".($field->required?"required":"")."' value='yes' ". (@$fielddata["$field->name"]=="yes"?"checked":"").">". JText::_('JYES');
            break;
        case "date": // textfield with calendar javascript
            $html .= "<div><div>$field->caption".($field->required ? "<span class='reqfield'>".JText::_('SC_REQUIRED')."</span>" : "")."</div><div>";
            $html .=  JHTML::calendar("","$field->name","$field->name",'%d-%m-%Y');

            break;
        case "dropdown": // dropdown list, single selection
            $html .= "<div><div>".JText::_($field->caption).($field->required ? "<span class='reqfield'>".JText::_('SC_REQUIRED')."</span>" : "")."</div><div>";
            $html .= "<select name='$field->name' id='$field->name' class='$field->classname ".($field->required?"required":"")."'>";
            $aoptions=explode(";", $field->fieldcontents);
            foreach ($aoptions as $key=>$value) {
                $html .= "<option value='$value'".(@$fielddata["$field->name"]=="$value"?" selected":"").">$value</option>";
            }
            $html .= "</select>";
            break;
    }

    //	$html .= $field->required ? "<span class='reqfield'>".JText::_('SC_REQUIRED')."</span>" : "";


    $html .= "";
    $html .= "</div>";
    $html .= "</div>";
} ?>
        <div>
            <div>&nbsp;</div>
            <div>
                <input class='sc_detailsbutton validate' type='submit' name='submit' value='<?= JText::_('SC_CONFIRM') ?>'/>
            </div>
        </div>
    </div>
<?= $endform ?>
