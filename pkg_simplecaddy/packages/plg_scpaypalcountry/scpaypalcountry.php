<?php
/**
 * @copyright Ivan Najdanović 2017
 * SimpleCaddy PayPal Country Selector
 * @version 2.0.4 for Joomla 2.5
 */
  
// No direct access allowed to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
    require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
    return;
}

/**
 * This plugin has three parts:
 * top part is getting the SimpleCaddy functions library
 * in the function onContentPrepare are the Joomla functions to manage the plugin in the content
 * the function showpaypalScreen displays and manages PayPal Country Selector specifics
 * 
 */

class scpaypalcountryconfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $countrylabel;
	var $countryclass;
	var $countrylength;
	var $usstatelabel;
	var $usstateclass;
	var $usstatelength;
	var $castatelabel;
	var $castateclass;
	var $castatelength;
	var $ostatelabel;
	var $ostateclass;
	var $ostatelength;

	function __construct(){
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_scpaypalcountry', JPATH_ADMINISTRATOR);
		$db	= JFactory::getDBO();
        try {
            parent::__construct( '#__sc_paypalcountry', 'id', $db );
		}
		catch (Exception $e) {
			$query= "CREATE TABLE `#__sc_paypalcountry` (
			`id`  int NULL AUTO_INCREMENT ,
			`countrylabel`  varchar(255) NULL ,
			`countryclass`  varchar(255) NULL ,
			`countrylength`  int NULL ,
			`usstatelabel`  varchar(255) NULL ,
			`usstateclass`  varchar(255) NULL ,
			`usstatelength`  int NULL ,
			`castatelabel`  varchar(255) NULL ,
			`castateclass`  varchar(255) NULL ,
			`castatelength`  int NULL ,
			`ostatelabel`  varchar(255) NULL ,
			`ostateclass`  varchar(255) NULL ,
			`ostatelength`  int NULL ,
			PRIMARY KEY (`id`)
			);";
			$this->_db->setQuery($query);
			$this->_db->execute();
			// now reconstruct, since it failed above...
            parent::__construct( '#__sc_paypalcountry', 'id', $db );
		};
        // get the first and only useful record in the db here
        $query=$this->_db->getQuery(true);
        $query->select("id");
        $query->from("`$this->_tbl`");
        $this->_db->setQuery($query);
        $id=$this->_db->loadResult(); // this sets the first ID as the one to use.
        $this->load($id); // load the first id
	}

	function _showconfig() { // mandatory function name
		$lang = JFactory::getLanguage(); // joomla's backend does not load frontend languages by itself
		$extension = 'plg_content_scpaypalcountry'; // so we need to do this manually here
		$lang->load($extension);
				
		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
		$this->load();
		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCPAYPALCOUNTRY_CONFIGURATION" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SCPAYPALCOUNTRY_SAVE"), false); 
		JToolbarHelper::cancel();
        JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false,  false );
		?>
		<form name="adminForm" id="adminForm">
			<table class="adminform">
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_COUNTRYLABEL");?></td>
				<td><input type="text" name="countrylabel" value="<?php echo $this->countrylabel; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_COUNTRYCLASS");?></td>
				<td><input type="text" name="countryclass" value="<?php echo $this->countryclass; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_COUNTRYLENGTH");?></td>
				<td><input type="text" name="countrylength" value="<?php echo $this->countrylength; ?>" size="100" /></td></tr>
				
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_USSTATELABEL");?></td>
				<td><input type="text" name="usstatelabel" value="<?php echo $this->usstatelabel; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_USSTATECLASS");?></td>
				<td><input type="text" name="usstateclass" value="<?php echo $this->usstateclass; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_USSTATELENGTH");?></td>
				<td><input type="text" name="usstatelength" value="<?php echo $this->usstatelength; ?>" size="100" /></td></tr>
				
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_CASTATELABEL");?></td>
				<td><input type="text" name="castatelabel" value="<?php echo $this->castatelabel; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_CASTATECLASS");?></td>
				<td><input type="text" name="castateclass" value="<?php echo $this->castateclass; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_CASTATELENGTH");?></td>
				<td><input type="text" name="castatelength" value="<?php echo $this->castatelength; ?>" size="100" /></td></tr>
				
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_OSTATELABEL");?></td>
				<td><input type="text" name="ostatelabel" value="<?php echo $this->ostatelabel; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_OSTATECLASS");?></td>
				<td><input type="text" name="ostateclass" value="<?php echo $this->ostateclass; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCPAYPALCOUNTRY_OSTATELENGTH");?></td>
				<td><input type="text" name="ostatelength" value="<?php echo $this->ostatelength; ?>" size="100" /></td></tr>
				
				<input type="hidden" name="option" value="com_simplecaddy" />
				<input type="hidden" name="action" value="pluginconfig"/>
				<input type="hidden" name="pluginname" value="scpaypalcountry"/>
				<input type="hidden" name="task" />
				<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
			</table>
		</form>
		<?php
	}

    function save($src="", $orderingFilter = '', $ignore = '') { // mandatory function to save any variables
		$this->bind($_REQUEST); // get all the fields from the admin form
		$b=$this->store(); // store the relevant fields only
		if ($b) {
			$msg=JText::_("SCPAYPALCOUNTRY_CONFIG_SAVED");
		}
		else
		{
			$dbmsg=$this->_db->getErrorMsg();
			$msg=JText::_("SCPAYPALCOUNTRY_CONFIG_NOT_SAVED") . $dbmsg;
		}
		$mainframe=JFactory::getApplication();
        $mainframe->enqueueMessage($msg);
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=scpaypalcountry");
	}
	
	function cancel() {
		$mainframe=JFactory::getApplication();
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
	}
    function main() {
        $mainframe=JFactory::getApplication();
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
    }
}

//The Content plugin Loadmodule
class plgContentscpaypalcountry extends JPlugin {
	var $debugshow="hidden"; // just for testing purposes
	var $_plugin_number	= 0;

    function __construct( &$subject, $config ) {
        parent::__construct( $subject, $config );
        $this->loadLanguage(); // necessary or not, let's make sure we get the language file
    }

    public function _setPluginNumber() {
        $this->_plugin_number = (int)$this->_plugin_number + 1; // only the first occurrence of the plugin should load css
    }

    function onContentPrepare($context, &$article, &$params, $page = 0 ) {
        if (!JComponentHelper::isEnabled('com_simplecaddy', true)) { // check for the component install
            echo "<div style='color:red;'>The SimpleCaddy component is not installed or is not enabled</div>";
            return "";
        }

        $regex = '/{(sccountrypaypal)\s*(.*?)}/i'; // the plugin code to get from content

        $parms=array();
        $matches = array();
        preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
        foreach ($matches as $elm) {
 			$this->_setPluginNumber();
 			if ($this->_plugin_number==1) { // get the stylesheet only ONCE per page
				JHTML::stylesheet('components/com_simplecaddy/css/simplecaddy.css' );
				// on windows servers this may need to be changed to 
				// JHTML::stylesheet('components\com_simplecaddy\css\simplecaddy.css' );
 			}
 			if ($this->_plugin_number>1 ) { // there should be only ONE payment plugin per page...
	            $article->text = preg_replace($regex, JText::_("SC_EXTRA_PLUGINS_REMOVED"), $article->text, 1);
		 		return true;		
 			}
			$line=str_replace("&nbsp;", " ", $elm[2]);
            $line=str_replace(" ", "&", $line);
            $line=strtolower($line);
            parse_str( $line, $parms );

            if (!isset($parms['type'])) { // no type provided, or just forgot...
            	$parms["type"]="details";
            }
        	// get all different types of display here, define manually to avoid hacking of the code
        	switch (strtolower($parms['type']) ) {
        		
        		case "details": // the default if nothing has been provided
                	$html=$this->showpaypalscreen($parms);
                	break;
        		default: // anything else provides an error message
                	$html=JText::_("SC_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). "({$parms['type']})";
        	}
            $article->text = preg_replace($regex, $html, $article->text, 1);
        }
        return true;
	}
	
	function showpaypalScreen($parms) { // display of any PayPal specific stuff goes here
		$cfg=new scpaypalcountryconfiguration();
		
		$html ="<script type='text/javascript'>
		function showhidepaypal(what) {
    	if(what == 'US') {
         	showpaypal('paypalusstate');
			hidepaypal('paypalcastate');
			hidepaypal('paypalostate');
		} 
        else if (what == 'CA') {
            showpaypal('paypalcastate');
            hidepaypal('paypalusstate');
            hidepaypal('paypalostate');
		}
        else{
            showpaypal('paypalostate');
            hidepaypal('paypalusstate');
            hidepaypal('paypalcastate');
		}  
		}
		function hidepaypal(obj) {
			obj1 = document.getElementById(obj);
			obj1.style.display = 'none';
		}
		function showpaypal(obj) {
    	    obj1 = document.getElementById(obj);
			obj1.style.display = '';
		}


		</script>";


		$html .= "<div id='paypalcountry' class='form-group required'>";
		$html .= "<label for='country' class='col-sm-3 col-md-3 col-lg-3 control-label'>".JText::_($cfg->countrylabel)."</label>";
		$html .= "<div class='col-sm-9 col-md-9 col-lg-9'>";
		$html .="
		<select name='paypalcountry'  class='form-control' onchange='showhidepaypal(this.value)'>
		<option value='AL'>Albania</option>
		<option value='DZ'>Algeria</option>
		<option value='AD'>Andorra</option>
		<option value='AO'>Angola</option>
		<option value='AI'>Anguilla</option>
		<option value='AG'>Antigua and Barbuda</option>
		<option value='AR'>Argentina</option>
		<option value='AM'>Armenia</option>
		<option value='AW'>Aruba</option>
		<option value='AU'>Australia</option>
		<option value='AT'>Austria</option>
		<option value='AZ'>Azerbaijan</option>
		<option value='BS'>Bahamas</option>
		<option value='BH'>Bahrain</option>
		<option value='BB'>Barbados</option>
		<option value='BY'>Belarus</option>
		<option value='BE'>Belgium</option>
		<option value='BZ'>Belize</option>
		<option value='BJ'>Benin</option>
		<option value='BM'>Bermuda</option>
		<option value='BT'>Bhutan</option>
		<option value='BO'>Bolivia</option>
		<option value='BA'>Bosnia and Herzegovina</option>
		<option value='BW'>Botswana</option>
		<option value='BR'>Brazil</option>
		<option value='VG'>British Vigin Islands</option>
		<option value='BN'>Brunei</option>
		<option value='BG'>Bulgaria</option>
		<option value='BF'>Burkina Faso</option>
		<option value='BI'>Burundi</option>
		<option value='KH'>Cambodia</option>
		<option value='CM'>Cameroon</option>
		<option value='CA'>Canada</option>
		<option value='CV'>Cape Verde</option>
		<option value='KY'>Cayman Island</option>
		<option value='TD'>Chad</option>
		<option value='CL'>Chile</option>
		<option value='C2'>China</option>
		<option value='CO'>Columbia</option>
		<option value='KM'>Comoros</option>
		<option value='CG'>Congo - Brazzaville</option>
		<option value='CD'>Congo - Kinshasa</option>
		<option value='CK'>Cook Islands</option>
		<option value='CR'>Costa Rica</option>
		<option value='CI'>Cote D'Ivoire (Ivory Coast)</option>
		<option value='HR'>Croatia (Hrvatska)</option>
		<option value='CY'>Cyprus</option>
		<option value='CZ'>Czech Republic</option>
		<option value='DK'>Denmark</option>
		<option value='DJ'>Djibouti</option>
		<option value='DM'>Dominica</option>
		<option value='DO'>Dominican Republic</option>
		<option value='EC'>Ecuador</option>
		<option value='EG'>Egypt</option>
		<option value='SV'>El Salvador</option>
		<option value='ER'>Eritrea</option>
		<option value='EE'>Estonia</option>
		<option value='ET'>Ethiopia</option>
		<option value='FK'>Falkland Islands (Malvinas)</option>
		<option value='FO'>Faroe Islands</option>
		<option value='FJ'>Fiji</option>
		<option value='FI'>Finland</option>
		<option value='FR'>France</option>
		<option value='GF'>French Guiana</option>
		<option value='PF'>French Polynesia</option>
		<option value='GA'>Gabon</option>
		<option value='GM'>Gambia</option>
		<option value='GE'>Georgia</option>
		<option value='DE'>Germany</option>
		<option value='GI'>Gibraltar</option>
		<option value='GR'>Greece</option>
		<option value='GL'>Greenland</option>
		<option value='GD'>Grenada</option>
		<option value='GP'>Guadeloupe</option>
		<option value='GT'>Guatemala</option>
		<option value='GN'>Guinea</option>
		<option value='GW'>Guinea-Bissau</option>
		<option value='GY'>Guyana</option>
		<option value='HN'>Honduras</option>
		<option value='HK'>Hong Kong</option>
		<option value='HU'>Hungary</option>
		<option value='IS'>Iceland</option>
		<option value='IN'>India</option>
		<option value='ID'>Indonesia</option>
		<option value='IE'>Ireland</option>
		<option value='IL'>Israel</option>
		<option value='IT'>Italy</option>
		<option value='JM'>Jamaica</option>
		<option value='JP'>Japan</option>
		<option value='JO'>Jordan</option>
		<option value='KZ'>Kazakhstan</option>
		<option value='KE'>Kenya</option>
		<option value='KI'>Kiribati</option>
		<option value='KW'>Kuwait</option>
		<option value='KG'>Kyrgyzstan</option>
		<option value='LA'>Laos</option>
		<option value='LV'>Latvia</option
		<option value='LS'>Lesotho</option>
		<option value='LI'>Liechtenstein</option>
		<option value='LT'>Lithuania</option>
		<option value='LU'>Luxembourg</option>
		<option value='MK'>Macedonia</option>
		<option value='MG'>Madagascar</option>
		<option value='MW'>Malawi</option>
		<option value='MY'>Malaysia</option>
		<option value='MV'>Maldives</option>
		<option value='ML'>Mali</option>
		<option value='MT'>Malta</option>
		<option value='MH'>Marshall Islands</option>
		<option value='MQ'>Martinique</option>
		<option value='MR'>Mauritania</option>
		<option value='MU'>Mauritius</option>
		<option value='YT'>Mayotte</option>
		<option value='MX'>Mexico</option>
		<option value='FM'>Micronesia</option>
		<option value='MD'>Moldova</option>
		<option value='MC'>Monaco</option>
		<option value='MN'>Mongolia</option>
		<option value='ME'>Montenegro</option>
		<option value='MS'>Montserrat</option>
		<option value='MA'>Morocco</option>
		<option value='MZ'>Mozambique</option>
		<option value='NA'>Namibia</option>
		<option value='NR'>Nauru</option>
		<option value='NP'>Nepal</option>
		<option value='NL'>Netherlands</option>
		<option value='NC'>New Caledonia</option>
		<option value='NZ'>New Zealand</option>
		<option value='NI'>Nicaragua</option>
		<option value='NE'>Niger</option>
		<option value='NG'>Nigeria</option>
		<option value='NU'>Niue</option>
		<option value='NF'>Norfolk Island</option>
		<option value='NO'>Norway</option>
		<option value='OM'>Oman</option>
		<option value='PW'>Palau</option>
		<option value='PA'>Panama</option>
		<option value='PG'>Papua New Guinea</option>
		<option value='PY'>Paraguay</option>
		<option value='PE'>Peru</option>
		<option value='PH'>Philippines</option>
		<option value='PN'>Pitcairn Islands</option>
		<option value='PL'>Poland</option>
		<option value='PT'>Portugal</option>
		<option value='QA'>Qatar</option>
		<option value='RE'>Reunion</option>
		<option value='RO'>Romania</option>
		<option value='RU'>Russia</option>
		<option value='RW'>Rwanda</option>
		<option value='WS'>Samoa</option>
		<option value='SM'>San Marino</option>
		<option value='ST'>Sao Tome and Principle</option>
		<option value='SA'>Saudi Arabia</option>
		<option value='SN'>Senegal</option>
		<option value='RS'>Serbia</option>
		<option value='SC'>Seychelles</option>
		<option value='SL'>Sierra Leone</option>
		<option value='SG'>Singapore</option>
		<option value='SK'>Slovakia</option>
		<option value='SI'>Slovenia</option>
		<option value='SB'>Solomon Islands</option>
		<option value='SO'>Somalia</option>
		<option value='ZA'>South Africa</option>
		<option value='KR'>South Korea</option>
		<option value='ES'>Spain</option>
		<option value='LK'>Sri Lanka</option>
		<option value='SH'>St Helena</option>
		<option value='KN'>St Helena and Nevis</option>
		<option value='LC'>St Lucia</option>
		<option value='PM'>St Pierre and Miquelon</option>
		<option value='VC'>St Vincent and Grenadines</option>
		<option value='SR'>Suriname</option>
		<option value='SJ'>Svalbard And Jan Mayen Islands</option>
		<option value='SZ'>Swaziland</option>
		<option value='SE'>Sweden</option>
		<option value='CH'>Switzerland</option>
		<option value='TW'>Taiwan</option>
		<option value='TJ'>Tajikistan</option>
		<option value='TZ'>Tanzania</option>
		<option value='TH'>Thailand</option>
		<option value='TG'>Togo</option>
		<option value='TO'>Tonga</option>
		<option value='TT'>Trinidad and Tobago</option>
		<option value='TN'>Tunisia</option>
		<option value='TM'>Turkmenistan</option>
		<option value='TC'>Turks and Caicos Islands</option>
		<option value='TV'>Tuvalu</option>
		<option value='UG'>Uganda</option>
		<option value='UA'>Ukraine</option>
		<option value='AE'>United Arab Emirates</option>
		<option value='GB'>United Kingdom</option>
		<option value='US'>United States</option>
		<option value='UY'>Uruguay</option>
		<option value='VU'>Vanuatu</option>
		<option value='VA'>Vatican City</option>
		<option value='VE'>Venezuela</option>
		<option value='VN'>Vietnam</option>
		<option value='WF'>Wallis And Futuna Islands</option>
		<option value='YE'>Yemen</option>
		<option value='ZM'>Zambia</option>
		<option value='ZW'>Zimbabwe</option>
		</select>";
		$html .= "</div>";
		$html .= "</div>";


		$html .= "<div id='paypalusstate' class='form-group required' style='display:none;'>";
		$html .= "<label for='country' class='col-sm-3 col-md-3 col-lg-3 control-label'>".JText::_($cfg->usstatelabel)."</label>";
		$html .= "<div class='col-sm-9 col-md-9 col-lg-9'>";

		$html .="
		<select name='paypalusstate' class='form-control'>
		<option value='AL'>Alabama</option>
		<option value='AK'>Alaska</option>
		<option value='AZ'>Arizona</option>
		<option value='AR'>Arkansas</option>
		<option value='CA'>California</option>
		<option value='CO'>Colorado</option>
		<option value='CT'>Connecticut</option>
		<option value='DE'>Delaware</option>
		<option value='DC'>District of Columbia</option>
		<option value='FL'>Florida</option>
		<option value='GA'>Georgia</option>
		<option value='HI'>Hawaii</option>
		<option value='ID'>Idaho</option>
		<option value='IL'>Illinois</option>
		<option value='IN'>Indiana</option>
		<option value='IA'>Iowa</option>
		<option value='KS'>Kansas</option>
		<option value='KY'>Kentucky</option>
		<option value='LA'>Louisiana</option>
		<option value='ME'>Maine</option>
		<option value='MD'>Maryland</option>
		<option value='MA'>Massachusetts</option>
		<option value='MI'>Michigan</option>
		<option value='MN'>Minnesota</option>
		<option value='MS'>Mississippi</option>
		<option value='MO'>Missouri</option>
		<option value='MT'>Montana</option>
		<option value='NE'>Nebraska</option>
		<option value='NV'>Nevada</option>
		<option value='NH'>New Hampshire</option>
		<option value='NJ'>New Jersey</option>
		<option value='NM'>New Mexico</option>
		<option value='NY'>New York</option>
		<option value='NC'>North Carolina</option>
		<option value='ND'>North Dakota</option>
		<option value='OH'>Ohio</option>
		<option value='OK'>Oklahoma</option>
		<option value='OR'>Oregon</option>
		<option value='PA'>Pennsylvania</option>
		<option value='RI'>Rhode Island</option>
		<option value='SC'>South Carolina</option>
		<option value='SD'>South Dakota</option>
		<option value='TN'>Tennessee</option>
		<option value='TX'>Texas</option>
		<option value='UT'>Utah</option>
		<option value='VT'>Vermont</option>
		<option value='VA'>Virginia</option>
		<option value='WA'>Washington</option>
		<option value='WV'>Vest Virginia</option>
		<option value='WI'>Wisconsin</option>
		<option value='WY'>Wyoming</option>
		<option value='AA'>Armed Forces Americas</option>
		<option value='AE'>Armed Forces Europe</option>
		<option value='AP'>Armed Forces Pacific</option>
		<option value='AS'>American Samoa</option>
		<option value='GU'>Guam</option>
		<option value='MH'>Marshall Islands</option>
		<option value='MP'>Northern Mariana Islands</option>
		<option value='PW'>Palau</option>
		<option value='VI'>Virgin Islands</option>
		</select>";
		$html .= "</div>";
		$html .= "</div>";

		$html .= "<div id='paypalcastate' class='form-group required'  style='display:none;'>";
		$html .= "<label for='country' class='col-sm-3 col-md-3 col-lg-3 control-label'>".JText::_($cfg->castatelabel)."</label>";
		$html .= "<div class='col-sm-9 col-md-9 col-lg-9'>";

		$html .="
		<select name='paypalcastate' class='form-control'>
		<option value='AB'>Alberta</option>
		<option value='BC'>Brithis Columbia</option>
		<option value='MB'>Manitoba</option>
		<option value='NB'>New Brunswick</option>
		<option value='NL'>Newfoundland and Labrador</option>
		<option value='NT'>Northwest Territories</option>
		<option value='NS'>Nova Scotia</option>
		<option value='NU'>Nunavut</option>
		<option value='ON'>Ontario</option>
		<option value='PE'>Prince Edward Island</option>
		<option value='QC'>Quebec</option>
		<option value='SK'>Saskatchewan</option>
		<option value='YT'>Yukon</option>
		</select>";
		$html .= "</div>";
		$html .= "</div>";

		$html .= "<div id='paypalostate' class='form-group'>";
		$html .= "<label for='country' class='col-sm-3 col-md-3 col-lg-3 control-label'>".JText::_($cfg->ostatelabel)."</label>";
		$html .= "<div class='col-sm-9 col-md-9 col-lg-9'>";

		$html .="
		
		<input type='text' name='paypalostate' class='form-control'>";

		$html .= "</div>";
		$html .= "</div>";



		return $html;
	}
	
}