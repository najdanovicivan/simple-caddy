<?php
/**
 * @copyright Ivan Najdanović 2013
 * SimpleCaddy CCNow processor 
 * @version 2.0.4 for Joomla 2.5
 */
  
// No direct access allowed to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
    require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
    return;
}

/**
 * This plugin has three parts:
 * top part is getting the SimpleCaddy functions library
 * in the function onContentPrepare are the Joomla functions to manage the plugin in the content
 * the function showccnowScreen displays and manages CCNow specifics
 * 
 */

class scccnowconfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $ccnowenvironment;
	var $ccnowapikey;
	var $ccnowreturnapikey;
	var $ccnowclientid;
	var $ccnowcurrency;
	var $returnurl; 
	
	function __construct(){
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_scccnow', JPATH_ADMINISTRATOR);
		$db	= JFactory::getDBO();
		try {
            parent::__construct( '#__sc_ccnow', 'id', $db );
        }catch (Exception $e) {
            $query= "CREATE TABLE `#__sc_ccnow` (
			`id`  int NULL AUTO_INCREMENT ,
			`ccnowenvironment`  INT(11) NULL ,
			`ccnowclientid`  varchar(255) NULL ,
			`ccnowapikey`  varchar(255) NULL ,
			`ccnowreturnapikey`  varchar(255) NULL ,
			`ccnowcurrency`  text NULL ,
			`returnurl`  text NULL ,
			PRIMARY KEY (`id`)
			);";
			$this->_db->setQuery($query);
            $this->_db->execute();
            // now reconstruct, since it failed above...
            parent::__construct( '#__sc_ccnow', 'id', $db );
		};
        // get the first and only useful record in the db here
        $query=$this->_db->getQuery(true);
        $query->select("id");
        $query->from("`$this->_tbl`");
        $this->_db->setQuery($query);
        $id=$this->_db->loadResult(); // this sets the first ID as the one to use.
        $this->load($id); // load the first id
	}

	function _showconfig() { // mandatory function name
		$lang = JFactory::getLanguage(); // joomla's backend does not load frontend languages by itself
		$extension = 'plg_content_scccnow'; // so we need to do this manually here
		$lang->load($extension);
		$ccnowcurrency=array();
		$ccnowcurrency["DISABLED"]="Disable CCNow For This Language";	
		$ccnowcurrency["AUD"]="Australian Dollars (AUD)";			
		$ccnowcurrency["GBP"]="Pound Sterling (GBP)";			
		$ccnowcurrency["CAD"]="Canadian dollar (CAD)";				
		$ccnowcurrency["CHF"]="Swiss Franc (CHF)";				
		$ccnowcurrency["DKK"]="Danish Krone (DKK)";				
		$ccnowcurrency["EUR"]="Euros (EUR)";
		$ccnowcurrency["HKD"]="Hong Kong Dollar (HKD)";
		$ccnowcurrency["NOK"]="Norwegian krone (NOK)";
		$ccnowcurrency["JPY"]="Yen (YEN)";	
		$ccnowcurrency["NOK"]="Norwegian Krone (NOK)";				
		$ccnowcurrency["NZD"]="New Zealand Dollar (NZD)";				
		$ccnowcurrency["SEK"]="Swedish Krona (SEK)";				
		$ccnowcurrency["USD"]="US Dollar (USD)";				
		$ccnowcurrency["ZAR"]="South African Rand (ZAR)";
		
		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
		$this->load();

		$db = JFactory::getDbo();
		$query="SELECT sef, title_native, lang_code FROM #__languages ORDER BY sef ASC";
		$db->setQuery($query);
		$languages = $db->loadObjectList();

		$this->ccnowcurrency = unserialize($this->ccnowcurrency);
		$this->returnurl = unserialize($this->returnurl);

		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCCCNOW_CONFIGURATION" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SCCCNOW_SAVE"), false);
		JToolbarHelper::cancel();
		JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false,  false );
		?>
		<form name="adminForm" id="adminForm">
			<table class="adminform">
				<tr><td style="width: 450px;"><?php echo JText::_("SCCCNOW_ENVIRONMENT");?></td>
				<td>
                    <div class='control-group'>
                        <div class="controls">
                            <fieldset class="radio btn-group">';
                                <label for='0ccnowenvironment'><input type='radio' id='0ccnowenvironment' value='0' name='ccnowenvironment' <?= ($this->ccnowenvironment==0? 'checked="checked"':'') ?>><?= JText::_('SCCCNOW_SANDBOX') ?></label>
                                <label for='1ccnowenvironment'><input type='radio' id='1ccnowenvironment' value='1' name='ccnowenvironment' <?= ($this->ccnowenvironment==1? 'checked="checked"':'') ?>><?= JText::_('SCCCNOW_LIVE') ?></label>
                            </fieldset>
                        </div>
                    </div>
				</td></tr> 
				<tr><td><?php echo JText::_("SCCCNOW_CLIENT_ID");?></td>
				<td><input type="text" name="ccnowclientid" value="<?php echo $this->ccnowclientid; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCCCNOW_API_KEY");?></td>
				<td><input type="text" name="ccnowapikey" value="<?php echo $this->ccnowapikey; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCCCNOW_RETURN-API_KEY");?></td>
				<td><input type="text" name="ccnowreturnapikey" value="<?php echo $this->ccnowreturnapikey; ?>" size="100" /></td></tr>
				
				
				<?php 
					foreach ($languages as $l){
						echo "<tr><td>$l->title_native ".JText::_("SCCCNOW_CURRENCY")."</td><td>";
						echo "<select name=\"ccnowcurrency[$l->lang_code]\">";
						foreach ($ccnowcurrency as $key=>$f) {
							echo "<option value='$key'".($key==$this->ccnowcurrency["$l->lang_code"]?" selected":"").">$f</option>";
						}
						echo "</select></td></tr>" ;
					}
				?>
				
				
				<?php 
					
					foreach ($languages as $l){
					echo "<tr><td>";
					echo "$l->title_native ". JText::_("SCCCNOW_RETURN_URL");
					echo "</td><td>";
				    echo "<input type=\"text\" name=\"returnurl[$l->lang_code]\" value=\"".$this->returnurl["$l->lang_code"]."\" size=\"100\" />";
					echo "</td></tr>";
					}
				?>
				
				
				<input type="hidden" name="option" value="com_simplecaddy" />
				<input type="hidden" name="action" value="pluginconfig"/>
				<input type="hidden" name="pluginname" value="scccnow"/>
				<input type="hidden" name="task" />
				<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
			</table>
		</form>
		<?php
	}
	
	function save($stay=false, $orderingFilter = '', $ignore = '') { // mandatory function to save any variables
		$this->bind($_REQUEST); // get all the fields from the admin form
		$this->returnurl=serialize($this->returnurl);
		$this->ccnowcurrency=serialize($this->ccnowcurrency);
		$b=$this->store(); // store the relevant fields only
		if ($b) {
			$msg=JText::_("SCCCNOW_CONFIG_SAVED");
		}
		else
		{
			$dbmsg=$this->_db->getErrorMsg();
			$msg=JText::_("SCCCNOW_CONFIG_NOT_SAVED") . $dbmsg;
		}
		$mainframe=JFactory::getApplication();
        $mainframe->enqueueMessage($msg);
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=sccnow");
	}

    function cancel() {
        $mainframe=JFactory::getApplication();
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
    }
    function main() {
        $mainframe=JFactory::getApplication();
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
    }
}

//The Content plugin Loadmodule
class plgContentScccnow extends JPlugin {
	var $debugshow="hidden"; // just for testing purposes
	var $_plugin_number	= 0;

	function __construct( &$subject, $config ) {
        parent::__construct( $subject, $config );
        $this->loadLanguage(); // necessary or not, let's make sure we get the language file
	}

	public function _setPluginNumber() {
		$this->_plugin_number = (int)$this->_plugin_number + 1; // only the first occurrence of the plugin should load css
	}

	function onContentPrepare($context, &$article, &$params, $page = 0 ) {
		if (!JComponentHelper::isEnabled('com_simplecaddy', true)) { // check for the component install
			echo "<div style='color:red;'>The SimpleCaddy component is not installed or is not enabled</div>";
			return;
		}

        $regex = '/{(scccnow)\s*(.*?)}/i'; // the plugin code to get from content

        $parms=array();
        $matches = array();
        preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
        foreach ($matches as $elm) {
 			$this->_setPluginNumber();
 			if ($this->_plugin_number==1) { // get the stylesheet only ONCE per page
				JHTML::stylesheet('components/com_simplecaddy/css/simplecaddy.css' );
				// on windows servers this may need to be changed to 
				// JHTML::stylesheet('components\com_simplecaddy\css\simplecaddy.css' );
 			}
 			if ($this->_plugin_number>1 ) { // there should be only ONE payment plugin per page...
	            $article->text = preg_replace($regex, JText::_("SC_EXTRA_PLUGINS_REMOVED"), $article->text, 1);
		 		return true;		
 			}
			$line=str_replace("&nbsp;", " ", $elm[2]);
            $line=str_replace(" ", "&", $line);
            $line=strtolower($line);
            parse_str( $line, $parms );

            if (!isset($parms['type'])) { // no type provided, or just forgot...
            	$parms["type"]="checkout";
            }
        	// get all different types of display here, define manually to avoid hacking of the code
        	switch (strtolower($parms['type']) ) {
        		case "checkout": // the default if nothing has been provided
                	$html=$this->showccnowscreen($parms);
                	break;
				default: // anything else provides an error message
                	$html=JText::_("SC_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). "({$parms['type']})";
        	}
            $article->text = preg_replace($regex, $html, $article->text, 1);
        }
        return true;
	}
	
	function showccnowScreen($parms) { // display of any CCNow specific stuff goes here
		$cfg=new scccnowconfiguration();
		if (!$cfg->ccnowclientid) {
			$html = "CCNow client id is not entered, please add this to the configuration first. ccnow cannot be used!";
			return $html;
		}
		if (!$cfg->ccnowapikey) {
			$html = "CCNow API Key is not entered, please add this to the configuration first. ccnow cannot be used!";
			return $html;
		}
		
		$ucurrency = unserialize($cfg->ccnowcurrency);
		
		$lang =& JFactory::getLanguage();
		$currency = $ucurrency[$lang->getTag()];
		if ($currency == "DISABLED") {
			$html = "CCNow is disabled for this site language";
			return $html;
		}
		setcookie("ccnowlanguage", $lang->getTag(), time()+600);
		$environment = $cfg->ccnowenvironment;
		
		
		$ordercode=JRequest::getVar("data"); // the data contains the ordercode when you finish the details page
				
		$orders=new orders(); 
		$orderid=$orders->getOrderIdFromCart($ordercode);
		$order=new order();
		$order->load($orderid);
		
		$orderfields=unserialize($order->customfields);
		
		
		// add the details to the order
		$gtotal=0; //define the grand total
		$pptax=0; // define the tax for CCNow
		$taxrate=0;
        
       
		//CCNow Account and Security Fingerprint Fields
		$html = "
<form id='ccnow' method='POST' action='https://www.ccnow.com/cgi-local/transact.cgi'>
";		$html .="<input type=\"hidden\" name=\"x_login\" value=\"$cfg->ccnowclientid\">
";
		$html .="<input type=\"hidden\" name=\"x_version\" value=\"1.0\">
";
		$html .="<input type=\"hidden\" name=\"x_fp_sequence\" value=\"$orderid\">
";
		
		
		
		
		//CCNow Product Information		
		$fieldnumber=0; // CCNow field numbering variable
        $odetails=new orderdetail();
        $lst=$odetails->getDetailsByOrderId($orderid);
		foreach ($lst as $product) {
            // create a post field and field value for CCNow
            if($product->total>0) { 
            	switch ($product->prodcode) {
            		case "shipping":
						if ($currency == "YEN")	
							$html .="<input type=\"hidden\" name=\"x_shipping_amount\" value=\"".round($product->unitprice)."\">
";
						else
							$html .="<input type=\"hidden\" name=\"x_shipping_amount\" value=\"".number_format($product->unitprice, 2,".", "")."\">
";
 						break;
    				case "tax":
						if ($currency == "YEN")	
							$html .="<input type=\"hidden\" name=\"x_tax_amount\" value=\"".round($product->unitprice)."\">
";
						else
							$html .="<input type=\"hidden\" name=\"x_tax_amount\" value=\"".number_format($product->unitprice, 2,".", "")."\">
";
    					break;
    				default:
						$fieldnumber = $fieldnumber +1 ; //increment the field number (could also be done with $fieldnumber++)
						$html .="<input type=\"hidden\" name=\"x_product_sku_".$fieldnumber."\" value=\"".$product->prodcode."\">
";
						$html .="<input type=\"hidden\" name=\"x_product_title_".$fieldnumber."\" value=\"".$product->shorttext."\">
";
						$html .="<input type=\"hidden\" name=\"x_product_quantity_".$fieldnumber."\" value=\"".$product->qty."\">
";
						if ($currency == "YEN")	
							$html .="<input type=\"hidden\" name=\"x_product_unitprice_".$fieldnumber."\" value=\"".round($product->unitprice)."\">
";
						else
							$html .="<input type=\"hidden\" name=\"x_product_unitprice_".$fieldnumber."\" value=\"".number_format($product->unitprice, 2,".", "")."\">
";
						$html .="<input type=\"hidden\" name=\"x_product_url_".$fieldnumber."\" value=\"Produc URL HERE\">
";
				}
            }
            else // price <0 so transfer it as a discount amount instead of a product
            {
				if ($currency == "YEN")	
							$html .="<input type=\"hidden\" name=\"x_discount_amount\" value=\"".round(abs($product->total))."\">
";
						else
							$html .="<input type=\"hidden\" name=\"x_discount_amount\" value=\"".number_format(abs($product->total), 2,".", "")."\">
";
 						break;
            }
            $gtotal += $product->total;
		}
		
		//CCNow Transaction Data
		if ($currency == "YEN")	
			$html .="<input type=\"hidden\" name=\"x_amount\" value=\"".round($gtotal)."\">
";
		else
			$html .="<input type=\"hidden\" name=\"x_amount\" value=\"".number_format($gtotal, 2,".", "")."\">
";
 		
		$html .="<input type=\"hidden\" name=\"x_currency_code\" value=\"$currency\">
";
		if ($cfg->ccnowenvironment==1) { // live
			if($orderfields["paymentmethod"]=="Credit Card")
			{
			$html .="<input type=\"hidden\" name=\"x_method\" value=\"CC\">
";          
			}
			else if($orderfields["paymentmethod"]=="PayPal")
			{
			$html .="<input type=\"hidden\" name=\"x_method\" value=\"PAYPAL\">
";          
			}
			else
			{
			$html .="<input type=\"hidden\" name=\"x_method\" value=\"NONE\">
";          
			}
		}
			
        else
        { // sandbox
            $html .="<input type=\"hidden\" name=\"x_method\" value=\"TEST\">
";
        }
		
		//CCNow Account and Security Fingerprint Fields - Generate hash
		$html .="<input type=\"hidden\" name=\"x_fp_arg_list\" value=\"x_login^x_fp_arg_list^x_fp_sequence^x_amount^x_currency_code\">
";
		if ($currency == "YEN")	
			$ccnowhash = md5($cfg->ccnowclientid."^x_login^x_fp_arg_list^x_fp_sequence^x_amount^x_currency_code^".$orderid."^".round($gtotal)."^".$currency."^".$cfg->ccnowapikey);
		else
			$ccnowhash = md5($cfg->ccnowclientid."^x_login^x_fp_arg_list^x_fp_sequence^x_amount^x_currency_code^".$orderid."^".number_format($gtotal, 2,".", "")."^".$currency."^".$cfg->ccnowapikey);
		$html .="<input type=\"hidden\" name=\"x_fp_hash\" value=\"$ccnowhash\">
";
		
		//CCNow Customer Billing Name and Address
		$html .="<input type=\"hidden\" name=\"x_name\" value=\"".$orderfields["first_name"]." ".$orderfields["last_name"]."\">
";
		$html .="<input type=\"hidden\" name=\"x_company\" value=\"".$orderfields["company"]."\">
";
		$html .="<input type=\"hidden\" name=\"x_address\" value=\"".$orderfields["address1"]."\">
";
		$html .="<input type=\"hidden\" name=\"x_address2\" value=\"".$orderfields["address2"]."\">
";
		$html .="<input type=\"hidden\" name=\"x_city\" value=\"".$orderfields["city"]."\">
";
		$html .="<input type=\"hidden\" name=\"x_country\" value=\"".$orderfields["ccnowcountry"]."\">
";
		if($orderfields["ccnowcountry"]=="US")
	    $html .="<input type=\"hidden\" name=\"x_state\" value=\"".$orderfields["ccnowusstate"]."\">
";
		else if($orderfields["ccnowcountry"]=="CA")
		$html .="<input type=\"hidden\" name=\"x_state\" value=\"".$orderfields["ccnowcastate"]."\">
";
		else
        $html .="<input type=\"hidden\" name=\"x_statename\" value=\"".$orderfields["codepostal"]."\">
";
	    $html .="<input type=\"hidden\" name=\"x_zip\" value=\"".$orderfields["postal_code"]."\">
";
	    $html .="<input type=\"hidden\" name=\"x_phone\" value=\"".$orderfields["telephone"]."\">
";
	    $html .="<input type=\"hidden\" name=\"x_email\" value=\"".$orderfields["email"]."\">
";
	    $html .="<input type=\"hidden\" name=\"x_invoice_num\" value=\"".$ordercode."\">
";
		// CCNow requires you use their logo to check out. Check the CCNow site for other button types
 		// look here for more buttons from CCNow http://www.ccnow.com/help/shoppingcart/retail_logos.html
       
		/** customizers, do your stuff here!
			Here is the place.
		*/

 		$html .= '<p style="text-align: center">
		<button type="submit"><img src="http://www.ccnow.com/en_US/i/logo/CCNow_VMADDJ_385x38_hw.png" width="385" height="38" alt="Buy from CCNow | Online Retailer | www.ccnow.com" border="0" /><br/>'.JText::_('SC_CCNOWTEXT').'</button></p>';
		$html .="<script type=\"text/javascript\">function myfunc () {var frm = document.getElementById(\"ccnow\");frm.submit();}window.onload = myfunc;</script>";
		$html .='<p style="text-align:center">You will be redirected shortly</p>';
        


        // additional ccnow info
        // be careful to 'escape' any " with \ !

		// otherwise we could use a simple submit button:
		//$html .='<input type="submit" value="ccnow">  ';
		$html .= "</form>";
		return $html;
	}
	
	function showccnowreturn($parms) {
		$cfg=new simplecaddyconfiguration();
		$html = "";
//		$html .= sprintf("<pre>%s</pre>", print_r($_POST, 1));
		// the values from ccnow we are interested in are the [custom] and [txn_id] values.
		
        $orderid=JRequest::getVar("custom"); // orderid coming back from PP
        $txn_id=JRequest::getVar("txn_id"); // pp transaction ID
        
		$cfg=new sc_configuration();
        $statuses=explode("\r\n", $cfg->get("ostatus"));
        $status=$statuses[count($statuses)-1]; // set the status to the last one in the list
        
        $scorder=new order();
        $scorder->load($orderid);
        $scorder->paymentcode="ccnow: ".$txn_id;
        $scorder->status=$status;
        // obviously there are other changes you could make to an order upon successful payment...
        $scorder->store();
		
		// now send success email
		$em=new email();
		$em->mailorder($orderid);
        $dlpageurl= $pcfg->dlpage;
		$mainframe=JFactory::getApplication();
		$dlpageurl .= "&dlkey=$scorder->ordercode";
		$mainframe->redirect($dlpageurl);	
		return $html;
	}
}