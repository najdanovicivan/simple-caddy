<?php
/**
 * @copyright Ivan Najdanović 2013
 * SimpleCaddy Albanian Country Selector
 * @version 2.0.4 for Joomla 2.5
 */
  
// No direct access allowed to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
    require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
    return;
}

/**
 * This plugin has three parts:
 * top part is getting the SimpleCaddy functions library
 * in the function onContentPrepare are the Joomla functions to manage the plugin in the content
 * the function showalbanianScreen displays and manages Albanian Country Selector specifics
 * 
 */

class scalbaniancountryconfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $countrylabel;
	var $countryclass;
	var $countrylength;
	
	function __construct(){
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_scalbaniancountry', JPATH_ADMINISTRATOR);
		$db	= JFactory::getDBO();
		try {
            parent::__construct( '#__sc_albaniancountry', 'id', $db );
		}
		catch (Exception $e) {
			$query= "CREATE TABLE `#__sc_albaniancountry` (
			`id`  int NULL AUTO_INCREMENT ,
			`countrylabel`  varchar(255) NULL ,
			`countryclass`  varchar(255) NULL ,
			`countrylength`  int NULL ,
			PRIMARY KEY (`id`)
			);";
			$this->_db->setQuery($query);
            $this->_db->execute();
			// now reconstruct, since it failed above...
            parent::__construct( '#__sc_albaniancountry', 'id', $db );
		}
        // get the first and only useful record in the db here
        $query=$this->_db->getQuery(true);
        $query->select("id");
        $query->from("`$this->_tbl`");
        $this->_db->setQuery($query);
        $id=$this->_db->loadResult(); // this sets the first ID as the one to use.
        $this->load($id); // load the first id
	}

	function _showconfig() { // mandatory function name
		$lang = JFactory::getLanguage(); // joomla's backend does not load frontend languages by itself
		$extension = 'plg_content_scalbaniancountry'; // so we need to do this manually here
		$lang->load($extension);
				
		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
		$this->load();
		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCALBANIANCOUNTRY_CONFIGURATION" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SCALBANIANCOUNTRY_SAVE"), false); 
		JToolbarHelper::cancel();
        JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false,  false );
		?>
		<form name="adminForm" id="adminForm">
			<table class="adminform">
				<tr><td><?php echo JText::_("SCALBANIANCOUNTRY_COUNTRYLABEL");?></td>
				<td><input type="text" name="countrylabel" value="<?php echo $this->countrylabel; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCALBANIANCOUNTRY_COUNTRYCLASS");?></td>
				<td><input type="text" name="countryclass" value="<?php echo $this->countryclass; ?>" size="100" /></td></tr>
				<tr><td><?php echo JText::_("SCALBANIANCOUNTRY_COUNTRYLENGTH");?></td>
				<td><input type="text" name="countrylength" value="<?php echo $this->countrylength; ?>" size="100" /></td></tr>
				
				<input type="hidden" name="option" value="com_simplecaddy" />
				<input type="hidden" name="action" value="pluginconfig"/>
				<input type="hidden" name="pluginname" value="scalbaniancountry"/>
				<input type="hidden" name="task" />
				<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
			</table>
		</form>
		<?php
	}

    function save($src="", $orderingFilter = '', $ignore = '') { // mandatory function to save any variables
		$this->bind($_REQUEST); // get all the fields from the admin form
		$b=$this->store(); // store the relevant fields only
		if ($b) {
			$msg=JText::_("SCALBANIANCOUNTRY_CONFIG_SAVED");
		}
		else
		{
			$dbmsg=$this->_db->getErrorMsg();
			$msg=JText::_("SCALBANIANCOUNTRY_CONFIG_NOT_SAVED") . $dbmsg;
		}
		$mainframe=JFactory::getApplication();
        $mainframe->enqueueMessage($msg);
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=scalbaniancountry");
	}
	
	function cancel() {
		$mainframe=JFactory::getApplication();
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
	}
    function main() {
        $mainframe=JFactory::getApplication();
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
    }
}

//The Content plugin Loadmodule
class plgContentscalbaniancountry extends JPlugin {
	var $debugshow="hidden"; // just for testing purposes
	var $_plugin_number	= 0;

    function __construct( &$subject, $config ) {
        parent::__construct( $subject, $config );
        $this->loadLanguage(); // necessary or not, let's make sure we get the language file
    }

    public function _setPluginNumber() {
        $this->_plugin_number = (int)$this->_plugin_number + 1; // only the first occurrence of the plugin should load css
    }

    function onContentPrepare($context, &$article, &$params, $page = 0 ) {
        if (!JComponentHelper::isEnabled('com_simplecaddy', true)) { // check for the component install
            echo "<div style='color:red;'>The SimpleCaddy component is not installed or is not enabled</div>";
            return "";
        }

        $regex = '/{(sccountryalbanian)\s*(.*?)}/i'; // the plugin code to get from content

        $parms=array();
        $matches = array();
        preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
        foreach ($matches as $elm) {
 			$this->_setPluginNumber();
 			if ($this->_plugin_number==1) { // get the stylesheet only ONCE per page
				JHTML::stylesheet('components/com_simplecaddy/css/simplecaddy.css' );
				// on windows servers this may need to be changed to 
				// JHTML::stylesheet('components\com_simplecaddy\css\simplecaddy.css' );
 			}
 			if ($this->_plugin_number>1 ) { // there should be only ONE payment plugin per page...
	            $article->text = preg_replace($regex, JText::_("SC_EXTRA_PLUGINS_REMOVED"), $article->text, 1);
		 		return true;		
 			}
			$line=str_replace("&nbsp;", " ", $elm[2]);
            $line=str_replace(" ", "&", $line);
            $line=strtolower($line);
            parse_str( $line, $parms );

            if (!isset($parms['type'])) { // no type provided, or just forgot...
            	$parms["type"]="details";
            }
        	// get all different types of display here, define manually to avoid hacking of the code
        	switch (strtolower($parms['type']) ) {
        		
        		case "details": // the default if nothing has been provided
                	$html=$this->showalbanianscreen($parms);
                	break;
        		default: // anything else provides an error message
                	$html=JText::_("SC_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). "({$parms['type']})";
        	}
            $article->text = preg_replace($regex, $html, $article->text, 1);
        }
        return true;
	}
	
	function showalbanianScreen($parms) { // display of any CCNow specific stuff goes here
		$cfg=new scalbaniancountryconfiguration();
		$html = "<div class='form-group required'>";
        $html .= "<label for='country' class='col-sm-3 col-md-3 col-lg-3 control-label'>$cfg->countrylabel</label>";
		$html .= "<div class='col-sm-9 col-md-9 col-lg-9'>";
		$html .="
		<select name='country' id='country' class='form-control'>
		<option value='Shqipëria'>Shqipëria</option>
		<option value='Kosova'>Kosova</option>
		<option value='Maqedonia'>Maqedonia</option>
		</select>";
		$html .= "</div>";
		$html .= "</div>";
		return $html;
	}
	
}