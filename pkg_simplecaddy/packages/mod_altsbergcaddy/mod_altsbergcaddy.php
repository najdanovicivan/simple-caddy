<?php
/**
 * Package SimpleCaddy 2.0 for Joomla 2.5
 * @Copyright Henk von Pickartz 2006-2013
 */
/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );
jimport('joomla.application.component.router');
require_once (JPATH_SITE.'/components/com_content/helpers/route.php');
//$mainframe=JFactory::getApplication();

if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
    require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
    return;
}

$nextcid=$params->get("nextcid");

$url = JRoute::_(ContentHelperRoute::getArticleRoute($nextcid)) ;
$a=getAltsbergCaddy($url);

echo $a;

function getAltsbergCaddy($url) {
	$cfg=new sc_configuration();
	$tsep=$cfg->get('thousand_sep');
	$dsep=$cfg->get('decimal_sep');
	$decs=$cfg->get('decimals');
	$currency=$cfg->get('currency');
	$curralign=$cfg->get('curralign');

	$html = "<div class='sctopcart'>";

	$cart2=new cart2();
	$cart=$cart2->readCart();
	if (!$cart) {
		$html .="<a href='$url' class='sctopcartlink'><i class='fa fa-shopping-cart'></i> ".JText::_('SC_CART_EMPTY')."</a>";
		$html.= "</div>";
		return $html;
	}
	$gtotal=0;
	$nombre=0;
	$emptycart=true;


	$nombre=$cart2->getCartQuantities();

	$carttotal=$cart2->getCartTotal();
	if ($nombre==0) {
		$html .="<a href='$url' class='sctopcartlink'><i class='fa fa-shopping-cart'></i> ".JText::_('SC_CART_EMPTY')."</a>";


	}
	else
	{
		$total=number_format($carttotal, $decs, $dsep, $tsep);
		$stotal=($curralign==1?JText::_($currency)."&nbsp;$total":"$total&nbsp;".JText::_($currency));

		$html .="<a href='$url' class='sctopcartlink'> <i class='fa fa-shopping-cart'></i><span class='sctopcartcart'> ".JText::_('SC_CART')." -</span> <strong>$nombre ".(($nombre==1)?JText::_('SC_ITEM'):JText::_('SC_ITEMS'))."</strong></a>";

		//$html .="<div class='row' style='font-weight:600;'><div class='col-xs-6'>".JText::_('SC_TOTAL')."</div><div class='col-xs-6' style='text-align:right;'>".$stotal."</div>";


	}
	$html.= "</div>";
	return $html;
}
?>
