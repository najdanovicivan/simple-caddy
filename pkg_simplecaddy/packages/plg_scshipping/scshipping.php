<?php
/**
 * @copyright Henk von Pickartz 2011-2014
 * SimpleCaddy Shipping processor
 * @version 2.0.5 for Joomla 3.3.+
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access allowed to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
// Import Joomla! Plugin library file
jimport('joomla.plugin.plugin');
if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
	require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
	return;
}


/**
 * This plugin has three parts:
 * top part is getting the SimpleCaddy functions library
 * in the function onContentPrepare are the Joomla functions to manage the plugin in the content
 *
 */

class scshippingglobalconfiguration extends JTable {
	var $id;
	var $controlfield;
	var $shippingprodcode;

	function __construct() {
		$db	= JFactory::getDBO();
		try {
		   	parent::__construct( '#__sc_shipping_config', 'id', $db );
		}
		catch (Exception $e) {
			$query= "CREATE TABLE `#__sc_shipping_config` (
			`id`  int NULL AUTO_INCREMENT ,
			`controlfield`  varchar(32) NULL ,
			`shippingprodcode`  varchar(32) NULL ,
			PRIMARY KEY (`id`)
			);";
			$this->_db->setQuery($query);
			$this->_db->execute();
			parent::__construct( '#__sc_shipping_config', 'id', $db );
		}
		$query=$this->_db->getQuery(true);
		$query->select("`id`");
		$query->from("`$this->_tbl`");
		$this->_db->setQuery($query);
		$id=$this->_db->loadResult(); // this sets the first ID as the one to use.
		$this->load($id); // load the first id
	}

	function show() {
		$fields=new fields();
		$fieldlist=$fields->getFieldNames(); // get all the fields, standard and custom
		foreach ($fieldlist as $key=>$value) { // need key and value identical
			$fieldlist2[$value]=$value;
		}
		$query=$this->_db->getQuery(true);
		$query->select("`id`");
		$query->from("`$this->_tbl`");
		$this->_db->setQuery($query);
		$id=$this->_db->loadResult(); // this sets the first ID as the one to use.
		$this->load($id); // load the first id
		?>
		<div style="margin: 15px 15px 15px 15px;">
			<div>
				<?php echo JText::_("SCSHIP_GLOBAL_CONFIG");?>
			</div>
			<p>
			<form name="frmshipconfig">
				<div style="text-align: right;">
					<button onclick="document.frmshipconfig.submit();"><?php echo JText::_("SCSHIP_SAVE_GLOBAL");?></button>
				</div>
				<table class="table">
					<tr><td style="width: 150px;"><?php echo JText::_("SCSHIP_CONTROLFIELD");?></td><td>
							<?php
							echo JHTML::_('select.genericlist', $fieldlist2, 'controlfield',  'class="inputbox"', 'value', 'text', $this->controlfield);
							?>
						</td></tr>
					<tr>
						<td><?php echo JText::_("SCSHIP_PRODCODE");?></td>
						<td><input type="text" size="60" name="shippingprodcode" value="<?php echo $this->shippingprodcode;?>" /></td>
					</tr>
				</table>
				<input type="hidden" name="option" value="com_simplecaddy" />
				<input type="hidden" name="action" value="pluginconfig"/>
				<input type="hidden" name="pluginname" value="scshipping"/>
				<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
				<input type="hidden" name="boxchecked" />
				<input type="hidden" name="task" value="saveglobal" />
			</form>
			</p>
		</div>
		<?php
	}

	function save($src="", $orderingFilter = '', $ignore = '') {
		$this->bind($_REQUEST); // get all the fields from the admin form
		$b=$this->store(); // store the relevant fields only
		if ($b) {
			$msg=JText::_("SCSHIP_GLOBAL_CONFIG_SAVED");
		}
		else
		{
			$dbmsg=$this->_db->getErrorMsg();
			$msg=JText::_("SCSHIP_GLOBAL_CONFIG_NOT_SAVED") . $dbmsg;
		}
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage($msg);
		$mainframe->redirect("index.php?option=com_simplecaddy&action=pluginconfig&pluginname=scshipping&task=showglobal&tmpl=component");
	}
}

class scshippingconfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $zone_name;
	var $points_lower;
	var $points_upper;
	var $cost;

	function __construct(){
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_scshipping', JPATH_ADMINISTRATOR);
		$db	= JFactory::getDBO();
		try {
	   		parent::__construct( '#__sc_shipping', 'id', $db );
		}
		catch (Exception $e) {
			$query= "CREATE TABLE `#__sc_shipping` (
			`id`  int NULL AUTO_INCREMENT ,
			`points_lower`  INT(11) NULL ,
			`points_upper`  INT(11) NULL ,
			`zone_name`  varchar(32) NULL ,
			`cost`  FLOAT NULL ,
			PRIMARY KEY (`id`)
			)
			;";
			$this->_db->setQuery($query);
			$this->_db->execute();
			parent::__construct( '#__sc_shipping', 'id', $db );
		}
	}

	function preview() {
		$tmp=new  scshippingglobalconfiguration();
		$tmp->show();
	}

	function addnew() {
		$this->editzone();
	}

	function edit() {
		$input=JFactory::getApplication()->input;
		$toedit=$input->get("cid", "", "ARRAY");
		$this->editzone($toedit[0]);
	}

	function remove() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$todelete=$input->get("cid", "", "ARRAY");
		foreach($todelete as $key=>$id) {
			$this->delete($id);
		}
		$url="index.php?option=com_simplecaddy&action=pluginconfig&task=showpluginconfig&pluginname=scshipping";
		$mainframe->redirect($url);
	}

	function cancel() {
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
	}

	function pluginlist() {
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=plugins");
	}

	function get($key, $default = null) { // retrieves voucher key from storage
		$query=$this->_db->getQuery(true);
		$query->select("`id`");
		$query->from("`$this->_tbl`");
		$query->where("`zone_name` = '$key'");
		$this->_db->setQuery($query);
		$id=$this->_db->loadResult();
		$this->load($id);
	}

	function getlist($sort=null, $sorttype="ASC") {
		$query=$this->_db->getQuery(true);
		$query->select("*");
		$query->from("`$this->_tbl`");
		if ($sort) $query->order("$sort $sorttype");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}

	function getCostByDestinationPoints($destination, $points) {
		$query=$this->_db->getQuery(true);
		$query->select("`cost`");
		$query->from("`$this->_tbl`");
		$query->where("`zone_name`='$destination'");
		$query->where("( (`points_lower` < $points) and (`points_upper` >= $points) )");

		$this->_db->setQuery($query);
		$cost=$this->_db->loadResult();
		return $cost;
	}

	function showglobal() {
		JHTML::_('behavior.modal');
		$scfg=new scshippingglobalconfiguration();
		$scfg->show();
	}

	function saveglobal() {
		$scfg=new scshippingglobalconfiguration();
		$scfg->save();
	}

	function _showconfig() { // mandatory function name
		JHTML::_('behavior.modal');
		$lang = JFactory::getLanguage(); // joomla's backend does not load frontend languages by itself
		$extension = 'plg_content_scshipping'; // so we need to do this manually here
		$lang->load($extension);
		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCC_SIMPLECADDY_SHIPPING_ZONES" ), 'generic.png');
		JToolBarHelper::addNew( 'addnew', JText::_("SCSHIP_ADDNEW") );
		JToolBarHelper::custom('edit',"edit", "edit", JText::_("SCSHIP_EDIT"));

		$bar = JToolbar::getInstance('toolbar');
		$url="index.php?option=com_simplecaddy&action=pluginconfig&pluginname=scshipping&task=showglobal&tmpl=component";
		// Add a preview button.
		$bar->appendButton('Popup', 'preview', 'Basic config', $url . '&task=preview');

		JToolbarHelper::deleteList();
		JToolBarHelper::custom('pluginlist', 'back', '', 'Main', false);

		$rows=$this->getlist('zone_name, points_lower');
		?>

		<form name="adminForm" method="post" id="adminForm">
		<table class="table">
		<tr>
		<th style="width: 15px;text-align: center;">
			<?php echo JHTML::_('grid.checkall'); ?>
		</th>
		<th><?php echo JText::_("SCSHIP_ZONE");?></th>
		<th><?php echo JText::_("SCSHIP_LOWER");?></th>
		<th><?php echo JText::_("SCSHIP_UPPER");?></th>
		<th><?php echo JText::_("SCSHIP_COST");?></th>
		</tr>
		<?php
			$k = 0;
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			?>
			<tr class="<?php echo "row$k"; ?>">
			<td style="width: 15px;text-align: left;">
				<?php echo JHtml::_('grid.id', $i, $row->id); ?>
			</td>
			<td>
			<a href="#edit" onclick="return listItemTask('cb<?php echo $i;?>','edit')">
			<?php echo $row->zone_name;?></a>
			</td>
			<td><?php echo $row->points_lower;?></td>
			<td><?php echo $row->points_upper;?></td>
			<td><?php echo $row->cost;?></td>
			</tr>
			<?php
			$k= 1-$k;
		}
		?>
		</table>

		<input type="hidden" name="option" value="com_simplecaddy" />
		<input type="hidden" name="action" value="pluginconfig"/>
		<input type="hidden" name="pluginname" value="scshipping"/>
		<input type="hidden" name="boxchecked" />
		<input type="hidden" name="task" />
		</form>
		<?php
	}

	function editzone($id=null) {
		$this->load($id);
		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
//		JHTML::script('components/com_simplecaddy/js/datetimepicker.js' );
		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCSHIP_SIMPLECADDY_EDIT_ZONE" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SCSHIP_SAVE"), false);
		JToolbarHelper::cancel();
		$scfg=new scshippingglobalconfiguration();
		$fieldname=$scfg->controlfield;
		echo $fieldname;
		$field=new fields();
		$field->getFieldByName($fieldname);
		$szonelist=$field->fieldcontents;
		$zonelist=explode(";", $szonelist);
		?>
		<form name="adminForm" method="post" id="adminForm">
		<table class="admintable">
		<tr><th style="width:  150px;">&nbsp;</th><th>&nbsp;</th></tr>
		<tr><td><?php echo JText::_("SCSHIP_ZONE");?></td>
		<td>
		<select name="zone_name">
		<?php
 			foreach ($zonelist as $key=>$value) {
 				echo "<option value='$value'".($value==$this->zone_name?" selected":"").">$value</option>";
 			}
		?>
		</select>
		</td>
		</tr>
		<tr><td><?php echo JText::_("SCSHIP_LOWER");?></td><td><input type="text" name="points_lower" value="<?php echo $this->points_lower;?>" /></td></tr>
		<tr><td><?php echo JText::_("SCSHIP_UPPER");?></td><td><input type="text" name="points_upper" value="<?php echo $this->points_upper;?>" /></td></tr>
		<tr><td><?php echo JText::_("SCSHIP_COST");?></td><td><input type="text" name="cost" value="<?php echo $this->cost;?>" /></td></tr>
		</table>
		<input type="hidden" name="option" value="com_simplecaddy" />
		<input type="hidden" name="action" value="pluginconfig"/>
		<input type="hidden" name="pluginname" value="scshipping"/>
		<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
		<input type="hidden" name="boxchecked" />
		<input type="hidden" name="task" />
		</form>
		<?php
	}


	function save($src="", $orderingFilter = '', $ignore = '') { // mandatory function to save any variables
		$this->bind($_REQUEST); // get all the fields from the admin form
		$b=$this->store(); // store the relevant fields only
		if ($b) {
			$msg=JText::_("SCSHIP_CONFIG_SAVED");
		}
		else
		{
			$dbmsg=$this->_db->getErrorMsg();
			$msg=JText::_("SCSHIP_CONFIG_NOT_SAVED") . $dbmsg;
		}
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage($msg);
		$mainframe->redirect("index.php?option=com_simplecaddy&action=pluginconfig&task=showpluginconfig&pluginname=scshipping");
	}
}

//The Content plugin Loadmodule
class plgContentScshipping extends JPlugin {
	var $debugshow="hidden"; // just for testing purposes
	var $_plugin_number	= 0;

	function __contruct( &$subject, $config ) {
        parent::__construct( $subject, $config );
        $this->loadLanguage(); // necessary or not, let's make sure we get the language file
	}

	public function _setPluginNumber() {
		$this->_plugin_number = (int)$this->_plugin_number + 1; // only the first occurrence of the plugin should load css
	}

	function onContentSimpleCaddyShipping($context, &$article, &$params, $page = 0 ) {
		if (!JComponentHelper::isEnabled('com_simplecaddy')) { // check for the component install
			echo "<div style='color:red;'>The SimpleCaddy component is not installed or is not enabled</div>";
			return "";
		}
		$mainframe = JFactory::getApplication();
		$input     = $mainframe->input;
		$view=$input->get("view");
		// check if we are displaying standard content. If not, get outta here
		$canProceed = ( ($context == 'com_content.article') and ($view=="article") );
		if (!$canProceed) {
			return "";
		}

        $regex = '/{(scshipping)\s*(.*?)}/i'; // the plugin code to get from content

        $parms=array();
        $matches = array();
        preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
        foreach ($matches as $elm) {
 			$this->_setPluginNumber();
 			if ($this->_plugin_number==1) { // get the stylesheet only ONCE per page
			//	JHTML::stylesheet('components/com_simplecaddy/css/simplecaddy.css' );
			// shipping displays nothing so no loading of css required
 			}
 			if ($this->_plugin_number>1 ) { // there should be only ONE payment plugin per page...
	            $article->text = preg_replace($regex, JText::_("SC_EXTRA_PLUGINS_REMOVED"), $article->text, 1);
		 		return true;
 			}
			$line=str_replace("&nbsp;", " ", $elm[2]);
            $line=strip_tags($line);
            $line=str_replace(" ", "&", $line);
            $line=strtolower($line);
            parse_str( $line, $parms );

            if (!isset($parms['type'])) { // no type provided, or just forgot...
            	$parms["type"]="default";
            }
        	// get all different types of display here, define manually to avoid hacking of the code
        	switch (strtolower($parms['type']) ) {
       			case "default": // the default if nothing has been provided
                	$html=$this->addShipping($parms);
                	break;
        		default: // anything else provides an error message
                	$html=JText::_("SC_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). "({$parms['type']})";
        	}
            $article->text = preg_replace($regex, $html, $article->text, 1);
        }
        return true;
	}

	function addShipping($parms) { // must be done through a content plugin so we may be able to select different shipping options later
		$html="";
		$input=JFactory::getApplication()->input;
		$ordercode=$input->get("data"); // the data contains the ordercode when you finish the details page
		if (@!$ordercode) {
			return "";
		}

		$orders=new orders();
		$orderid=$orders->getOrderIdFromCart($ordercode); // now we have an order ID
		$order= new order();
		$order->load($orderid);
		$cfields=unserialize($order->customfields); // returns an array with all fields

		$scfg=new scshippingglobalconfiguration();
		$controlfield=$scfg->controlfield;

		$orderdestination=$cfields[$controlfield]; // get the destination from the fields by $controlfield

		$tmp=new products();
		$cart2=new cart2();
		$prodcodes=$cart2->getCartproductCodes();

		$shippingpoints=0;
		foreach ($prodcodes as $code=>$qty) {
			$p=$tmp->getproductByProdCode($code);
			$points=$p->shippoints * $qty;
			$shippingpoints += $points;
		}
//		$html .= "Points $shippingpoints";
		$cfg=new scshippingconfiguration();
		$cost=$cfg->getCostByDestinationPoints($orderdestination, $shippingpoints);
		$order->shipCost=$cost;
		$order->shipper="";
		$order->store();
		return $html;
	}
}