<?php
// No direct access.
defined('_JEXEC') or die;

class productoption extends JTable { // individual options
	var $id;
	var $optgroupid="";
	var $formula="";
	var $caption="";
	var $description="";
	var $defselect;
	var $disporder;

	function __construct() {
		$db	= JFactory::getDBO();
		parent::__construct( '#__sc_productoptions', 'id', $db );
	}


	function getbygroup($optgroupid) {
		$query=$this->_db->getQuery(true);
		$query->select("*");
		$query->from("$this->_tbl");
		$query->where("`optgroupid` = '$optgroupid'");
		$query->order("`disporder` asc");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}
}
