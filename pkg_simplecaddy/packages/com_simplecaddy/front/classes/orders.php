<?php
// No direct access.
defined('_JEXEC') or die;

class orders {

	function getmenuitem() {
		$m=new stdClass();
		$m->title=JText::_('SC_ORDERS');
		$m->link="index.php?option=com_simplecaddy&action=orders&task=show";
		$m->image="components/com_simplecaddy/images/orders.png";
		$m->alt=JText::_('SC_MANAGE_ORDERS');
		$m->text=JText::_('SC_ORDERS');
		$m->active=true;
		return $m;
	}



	function store_new_order($cart) {
		if (count($cart)==0) return false;
//   	$db	= JFactory::getDBO();
		//get statuses
		$cfg=new sc_configuration();
		$statuses=explode("\n", trim($cfg->get("ostatus")));
//    	$useshipping=$cfg->get("shippingenabled");
		// get the first status from the list
		$status=(isset($statuses[0])?trim($statuses[0]):JText::_('SC_NO_STATUS') );

		$juser=JFactory::getUser();

        if (!isset($_POST['name']))
            $_POST["name"] = $_POST["first_name"]." ".$_POST["last_name"];

		//create order info from the details page
		$o=new order();
		$o->bind($_POST);
		$o->id=null; // ensure a new order is created here
		$o->j_user_id=$juser->id; // add the user id
		$o->orderdt=time();
		$o->status=$status;
		$o->customfields=serialize($_POST);

		$o->ordercode=substr(md5(time()),0,15); // should be pretty unique for an order
		$o->orderlink=JURI::base( true )."/index.php?option=com_simplecaddy&ordercode=$o->ordercode"; // incomplete so that other functions can redirect elsewhere with this code

        //Store the current site language
        $lang = JFactory::getLanguage();
        $o->language = $lang->getTag();

		$orderid=$o->save();

		$gtotal=0;
		$autodec=$cfg->get("autodecfromstore");
		foreach ($cart as $key=>$product) {
			unset($odet);
			$odet=new orderdetail();
			$odet->id=null;
			$odet->orderid=$orderid;
			$odet->prodcode=$product->prodcode;
			$odet->qty=$product->quantity;
			$odet->unitprice=$product->finalprice;
			$odet->total=$product->quantity*$product->finalprice;
			$odet->shorttext=$product->prodname;
			$odet->option=$product->option;
			$odet->store();
			$gtotal=$gtotal+$odet->total;
			if ($autodec==1) { // auto decrement from store
				$pr=new products();
				$pr->decfromstore($product->prodcode, $product->quantity);
			}
		}
		$o = new order(); // reload the order
		$o->load($orderid);

		$o->total=$gtotal; // update its grand total
		$o->store(); // store the order with new total
		return $orderid;
	}


	function getAllOrders($field=null, $type='', $special=0, $filter=null, $archive=0) {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$db	= JFactory::getDBO();

		$filter_order=$input->getCmd("filter_order");
		$filter_order_dir=$input->getCmd("filter_order_Dir");

        $filter=$input->getCmd("search");

        $language=$input->getCmd("language");

		$query=$db->getQuery(true);
		$query->select("*");
		$query->from('#__sc_orders AS o');

        $query->select('l.title AS language_title, l.image AS language_image')
            ->join('LEFT', $db->quoteName('#__languages') . ' AS l ON l.lang_code = o.language');


        $query->where("`archive`='$archive'");

		if ($filter) {
			if (is_numeric($filter)) {
				$query->where("o.id = '$filter'") ;
			}
			else
			{
				$query->where("name like '%$filter%'");
			}
		}

		if ($language && $language != "*"){
            $query->where("o.language = '$language'") ;
        }

		if ($filter_order) {
			$query->order("`$filter_order` $filter_order_dir");
		}
		$db->setQuery($query);
		$lst=$db->loadObjectList();

		$limit=$input->get("limit");
		if (!$limit) {
			$cfg=JFactory::getConfig();
			$limit=$cfg->get("list_limit");
		}

		$limitstart = $input->get("limitstart");
		$total=count($lst);

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$db->setQuery($query, $limitstart, $limit);
		$lst=$db->loadObjectList();
		$res=array();
		$search	= $mainframe->getUserStateFromRequest( 'search', 'search', '', 'string' );
		$search	= strtolower($search);
		$lists["order_Dir"]="$filter_order_dir";
		$lists["order"]="$filter_order";
		$lists['search']=$search;
        $lists['language']=$language;
		$res["total"]=$total;
		$res['search'] = $search;
		$res['lst']=$lst;
		$res['nav']=$pageNav;
		$res['lists']=$lists;
		return $res;
	}

	function getOrderIdFromCart($ordercode) {
		$db=JFactory::getDbo();
		$query=$db->getQuery(true);
		$query->select("`id`");
		$query->from("#__sc_orders");
		$query->where("`ordercode`='$ordercode' limit 1");

		$db->setQuery($query);
		$id=$db->loadResult();
		return $id;
	}

	function getorder($id) {
		$db	= JFactory::getDBO();
		$query=$db->getQuery(true);
		$query->select("*");
		$query->from("#__sc_orders");
		$query->where("id='$id'");
		$db->setQuery($query);
		$p=$db->loadObject();
		return $p;
	}

	function getOrderDetails($orderid) {
		$db	= JFactory::getDBO();
		$query=$db->getQuery(true);
		$query->select("*");
		$query->from("#__sc_odetails");
		$query->where("orderid='$orderid'");

		$db->setQuery($query);
		$lst=$db->loadObjectList();
		return $lst;
	}

	function getODetails($id) {
		global $mosConfig_list_limit;
		$mainframe=JFactory::getApplication();
		$db	= JFactory::getDBO();
		$query=$db->getQuery(true);
		$query->select("*");
		$query->from("#__sc_odetails");
		$query->where("orderid='$id'");
		$db->setQuery($query);
		$lst=$db->loadObjectList();

		$limit = intval( $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', $mosConfig_list_limit ) );
		$limitstart = intval( $mainframe->getUserStateFromRequest( "view{items}limitstart", 'limitstart', 0 ) );
		$total=count($lst);

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$db->setQuery($query, $limitstart, $limit);
		$lst=$db->loadObjectList();
		$res=array();
		$res['lst']=$lst;
		$res['nav']=$pageNav;
		return $res;
	}

	function RemoveOrders($cid=null) {
		$db	= JFactory::getDBO();
		//remove the orders
		$cids = implode( ',', $cid );
		$query=$db->getQuery(true);
		$query->delete("#__sc_orders");
		$query->where("id IN ( $cids )");
		$db->setQuery( $query );
		$db->execute();
		// remove the order items
		$query=$db->getQuery(true);
		$query->delete("#__sc_odetails");
		$query->where("orderid IN ( $cids )");
		$db->setQuery( $query );
		$db->execute();
	}

	function saveOrder() {
		//save an edited order. the only field changed is the status!
		$db	= JFactory::getDBO();
		$input=JFactory::getApplication()->input;
		$id=$input->get("id");
		$status=$input->getCmd("edtostatus");
//		$archive=$input->getCmd("archive");
		$order=$this->getorder($id);
		$order->status=$status;
		$db->updateObject("#__sc_orders", $order, "id");
		return $id;
	}

//---------------------------------------------------------------------------------------------
	function remove() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$search=$input->getCmd("search");
		$cid=$input->get( 'cid' ,array(), "RAW" ); // use RAW to get an array as input value
		$this->removeOrders($cid);
		$mainframe->enqueueMessage(JText::_('SC_ORDERDELETED'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=orders&task=show&search=$search");
	}

	function show() {
		$alist=$this->getAllOrders();
		display::header();
		require_once(JPATH_ADMINISTRATOR."/components/com_simplecaddy/views/listorders.php");
	}

	function email() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$oid=$input->getCmd("oid");
		$search=$input->getCmd("search");
		$email=new email();
		$email->mailorder($oid);
		$mainframe->enqueueMessage(JText::_('SC_EMAIL_SENT'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=orders&task=edit&id=$oid&search=$search");
	}

	function save() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$search=$input->getCmd("search");
		$field=$input->getCmd("field");
		$order=$input->getCmd("order");
		$cid[0]=$input->getCmd("id");
		$this->saveorder();
		$mainframe->enqueueMessage(JText::_('SC_ORDERSAVED'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=orders&task=show&field=$field&order=$order&search=$search");
	}

	function close() {
		$this->cancel();
	}
	function cancel(){
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$search=$input->getCmd("search");
		$field=$input->getCmd("field");
		$order=$input->getCmd("order");
		$mainframe->redirect("index.php?option=com_simplecaddy&action=orders&task=show&field=$field&order=$order&search=$search");
	}

	function view(){
		$this->edit();
	}

	function edit() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$cid=$input->get("cid", array(), "array");
		$scorder=$this->getorder($cid[0]);
		$details=$this->getODetails($cid[0]);
		$items=$details["lst"];

		display::header();
		JToolBarHelper::title( JText::_( "SC_ORDER" ), 'generic.png');
		JToolBarHelper::save( 'save', JText::_("SC_SAVE_CLOSE") );
		JToolBarHelper::apply();
		JToolBarHelper::cancel();
		require_once(JPATH_ADMINISTRATOR."/components/com_simplecaddy/views/editorder.php");
	}

	function apply(){
		$id=$this->saveorder();
		$this->edit($id);
	}

	function viewarchive() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$search=$input->getCmd("search");
		$field=$input->getCmd("field");
		$order=$input->getCmd("order");
		$alist=$this->getAllOrders($field, $order, null, $search, 1);
		display::ShowOrders($alist, $field, $order);
	}

	function export() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$cids=$input->get("cid", array(), "RAW");
		$o=new order();
		$o->ordertostring($cids);
		display::header();
		JToolBarHelper::title( JText::_( "SC_ORDER" ), 'generic.png');
		JToolBarHelper::cancel();
		display::showExport();
	}

	function exportall() {
		$mainframe=JFactory::getApplication();
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$o=new order();
		$o->ordertostring();
		display::header();
		JToolBarHelper::title( JText::_( "SC_ORDER" ), 'generic.png');
		JToolBarHelper::cancel();
		display::showExport();

	}
}
