<?php
// No direct access.
defined('_JEXEC') or die;

class fields extends JTable {
	var $id=null;
	var $name="";
	var $caption="";
	var $type="text";
	var $length=0;
	var $classname="inputbox";
	var $required=0;
	var $ordering;
	var $published=1;
	var $checkfunction="checkfilled";
	var $fieldcontents;

	function __construct() {
		$db	= JFactory::getDBO();
		parent::__construct( '#__sc_fields', 'id', $db );
	}

	function getmenuitem() {
		$m=new stdClass();
		$m->title=JText::_('SC_FIELDS');
		$m->link="index.php?option=com_simplecaddy&action=fields&task=show";
		$m->image="components/com_simplecaddy/images/fields.png";
		$m->alt=JText::_('SC_MANAGE_FIELDS');
		$m->text=JText::_('SC_FIELDS');
		$m->active=true;
		return $m;
	}


	function getField($id) {
		$this->load($id);
	}

	function cancel() {
		$this->show();
	}

	function show() {
		$arows=$this->getAllFields();
		require_once(JPATH_ADMINISTRATOR."/components/com_simplecaddy/views/listfields.php");
	}

	function getFieldByName($name) {
		$query=$this->_db->getQuery(true);
		$query->select("`id`");
		$query->from("`$this->_tbl`");
		$query->where("`name` = '$name'");
		$this->_db->setQuery($query);
		$id=$this->_db->loadResult();
		$this->load($id);
	}

	function getFieldNames() { // internal use only
		$query="select `name` from ".$this->_tbl;
		$this->_db->setQuery($query);
		return $this->_db->loadColumn();
	}

	function getAllFields() {
		global $mosConfig_list_limit;
		$mainframe=JFactory::getApplication();
		$query="select count(*) as total from ".$this->_tbl;
		$this->_db->setQuery($query);
		$total=$this->_db->loadResult();

		$limit = intval( $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', $mosConfig_list_limit ) );
		$limitstart = intval( $mainframe->getUserStateFromRequest( "view{scp}limitstart", 'limitstart', 0 ) );
		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );
		$query="select * from ".$this->_tbl. " order by `ordering` ASC ";
		$this->_db->setQuery($query, $pageNav->limitstart, $pageNav->limit);
		$lst=$this->_db->loadObjectList();
		$res['lst']=$lst;
		$res['nav']=$pageNav;
		return $res;
	}

	function getPublishedFields() {
		$query=$this->_db->getQuery(true);
		$query->select("*");
		$query->from("$this->_tbl");
		$query->where("published=1");
		$query->order("`ordering` asc");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}

	function getPublishedFieldsArray() {
		$query="select name from ".$this->_tbl." where published=1 order by `ordering` asc ";
		$this->_db->setQuery($query);
		$lst=$this->_db->loadColumn();
		return $lst;
	}

	function saveField() {
		$mainframe=JFactory::getApplication();
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$forbiddenchars=array(" ", ".", ",", "/", "$", "\\");
		$this->bind($_REQUEST);
		$this->name=strtolower($this->name);
		$this->name=str_replace($forbiddenchars,"", $this->name);
		$this->store();
		return $this->id;
	}

	function publish($pks = NULL, $state = 1, $userId = 0) {
		$mainframe=JFactory::getApplication();
		if($mainframe->isClient("site")) return; // no site usage, admin only
		$input=$mainframe->input;
		$cid=$input->get("cid", null, "ARRAY");
		$cids = implode( ',', $cid );
		$query=$this->_db->getQuery(true);
		$query->update($this->_tbl);
		$query->set("`published` = 1 ");
		$query->where(" `id` IN ( $cids )" );
		$this->_db->setQuery( $query );
		$this->_db->execute();
		$mainframe->enqueueMessage(JText::_('SC_FIELD_PUBLISHED'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=fields&task=show");
	}

	function unPublish() {
		$mainframe=JFactory::getApplication();
		if($mainframe->isClient("site")) return;
		$input=$mainframe->input;
		$cid=$input->get("cid", null, "ARRAY");
		$cids = implode( ',', $cid );
		$query=$this->_db->getQuery(true);
		$query->update($this->_tbl);
		$query->set("`published` = 0 ");
		$query->where(" `id` IN ( $cids )" );
		$this->_db->setQuery( $query );
		$this->_db->execute();
		$mainframe->enqueueMessage(JText::_('SC_FIELD_UNPUBLISHED'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=fields&task=show");
	}

	function remove() {
		$mainframe=JFactory::getApplication();
		if($mainframe->isClient("site")) return; // no site usage, admin only
		$input=$mainframe->input;
		$cid=$input->get("cid", null, "ARRAY");
		$cids = implode( ',', $cid );
		$query=$this->_db->getQuery(true);
		$query->delete($this->_tbl);
		$query->where("id IN ( $cids )");
		$this->_db->setQuery( $query );
		$this->_db->execute();
		$mainframe->enqueueMessage(JText::_('SC_FIELDDELETED'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=fields&task=show");
	}

	//*******************************************
	function apply() {
		$id=$this->saveField();
		$this->load($id);
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage(JText::_('SC_FIELDSAVED'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=fields&task=edit&cid[]=$id");
	}

	function addnew() {


	}

	function edit() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		if($mainframe->isClient("site")) return; // no site usage, admin only
		$cid=$input->get("cid", null, "ARRAY");
		$this->load($cid[0]);
		display::header();
		$edit = (isset($this->id));
		$text = ( $edit ? JText::_( 'EDIT' ) : JText::_( 'NEW' ) );
		JToolBarHelper::title( JText::_( "SIMPLECADDY_$text" ), 'generic.png');

		JToolBarHelper::save( 'save', JText::_("SC_SAVE_CLOSE") );
		JToolBarHelper::apply();
		if ( $edit ) {
			// for existing items the button is renamed `close`
			JToolBarHelper::cancel( 'cancel', 'Close' );
		} else {
			JToolBarHelper::cancel();
		}

		require_once(JPATH_ADMINISTRATOR."/components/com_simplecaddy/views/editfield.php");
//		display::editField($this);
	}

	function save($src=null, $orderingFilter = '', $ignore = '') {
		$mainframe=JFactory::getApplication();
		$this->saveField();
		$mainframe->enqueueMessage(JText::_('SC_FIELDSAVED'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=fields&task=show");

	}
}
