<?php
// No direct access.
defined('_JEXEC') or die;

class options extends JTable {
	var $id;
	var $optgroupid;
	var $description;
	var $formula;
	var $caption;
	var $defselect;
	var $disporder;
	var $image;
	var $classname;

	var $redirect;
	var $message;

	function __construct() {
		$db	= JFactory::getDBO();
		parent::__construct( '#__sc_productoptions', 'id', $db );
	}

	function redirect() {
	}

	function redirect2() {
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage( $this->message);
		$mainframe->redirect($this->redirect);
	}

	function getindoption($optgrid) {
		$query=$this->_db->getQuery(true);
		$query->select("*");
		$query->from("`$this->_tbl`");
		$query->where("`optgrid` = '$optgrid'");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}

	function getindoptionids($optgrid=array()) {
		if ( count($optgrid) == 0 ) return false;
		$optgrids=implode(",", $optgrid);
		$query=$this->_db->getQuery(true);
		$query->select("`id`");
		$query->from("`$this->_tbl`");
		$query->where("`optgroupid` IN ( $optgrids )");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadColumn();
		return $lst;
	}

	function deleteindoption($optionid) {
		$query=$this->_db->getQuery(true);
		$query->delete("`$this->_tbl`");
		$query->where("`id`='$optionid'");
		$this->_db->setQuery($query);
		$this->_db->execute();
	}

	function showindoptions() {
		$input=JFactory::getApplication()->input;

		$id=$input->get("optgrid");
		$productid=$input->get("productid");
		$po=new productoption();
		$lst=$po->getbygroup($id);
		display::showindoptions($lst, $id, $productid);
	}

	function saveoptions() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$optgrid=$input->get("optgrid");
		$productid=$input->get("productid");
		$optionid=$input->get("optionid", null, "ARRAY");
		$optionshorttext=$input->get("optionshorttext", null, "ARRAY");
		$optionformula=$input->get("optionformula", null, "ARRAY");
		$optionclassname=$input->get("optionclassname", null, "ARRAY");
		$optionimage=$input->get("optionimage", null, "ARRAY");
		$optioncaption=$input->get("optioncaption", null, "ARRAY");
		$optionimage=$input->get("optionimage", null, "ARRAY");
		$optionclassname=$input->get("optionclassname", null, "ARRAY");
		$optiondefselect=$input->get("optiondefselect");
		$optiondisporder=$input->get("optiondisporder", null, "ARRAY");

		// first, get rid of the existing options, if any
		$query="delete from `".$this->_tbl."` where `optgroupid` = '$optgrid' ";
		$query=$this->_db->getQuery(true);
		$query->delete("`$this->_tbl`");
		$query->where("`optgroupid` = '$optgrid'");
		$this->_db->setQuery($query);
		$this->_db->execute();

		// now recreate the new options
		foreach($optionid as $key=>$value) {
			$this->id=null;
			$this->optgroupid=$optgrid;
			$this->description=$optionshorttext[$key];
			$this->formula=$optionformula[$key];
			$this->classname=$optionclassname[$key];
			$this->image=$optionimage[$key];
			$this->caption=$optioncaption[$key];
			$this->image=$optionimage[$key];
			$this->classname=$optionclassname[$key];
			$this->defselect=($key==$optiondefselect?"1":"0");
			$this->disporder=$optiondisporder[$key];
			$this->store();
		}
		$this->redirect="index.php?option=com_simplecaddy&action=options&task=showindoptions&optgrid=$optgrid&tmpl=component&productid=$productid";
		$this->message=JText::_("SC_INDIVIDUAL_OPTIONS_SAVED");
		$this->redirect2();
	}


	function saveoptions0() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$optgrid=$input->get("optgrid");
		$productid=$input->get("productid");
		$optionid=$input->get("optionid", null, "ARRAY");
		$optionshorttext=$input->get("optionshorttext", null, "ARRAY");
		$optionformula=$input->get("optionformula", null, "ARRAY");
		$optioncaption=$input->get("optioncaption", null, "ARRAY");
		$optiondefselect=$input->get("optiondefselect");
		$optiondisporder=$input->get("optiondisporder", null, "ARRAY");

		// first, get rid of the existing options, if any
		$query="delete from `".$this->_tbl."` where `optgroupid` = '$optgrid' ";
		$query=$this->_db->getQuery(true);
		$query->delete("`$this->_tbl`");
		$query->where("`optgroupid` = '$optgrid'");
		$this->_db->setQuery($query);
		$this->_db->execute();

		// now recreate the new options
		foreach($optionid as $key=>$value) {
			$this->id=null;
			$this->optgroupid=$optgrid;
			$this->description=$optionshorttext[$key];
			$this->formula=$optionformula[$key];
			$this->caption=$optioncaption[$key];
			$this->defselect=($key==$optiondefselect?"1":"0");
			$this->disporder=$optiondisporder[$key];
			$this->store();
		}
		$this->redirect="index.php?option=com_simplecaddy&action=options&task=showindoptions&optgrid=$optgrid&tmpl=component&productid=$productid";
		$this->message=JText::_("SC_INDIVIDUAL_OPTIONS_SAVED");
		$this->redirect2();
	}

	function saveoptiongroup() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only

		$prodid=$input->get("prodid");
		$this->id=$input->get("id");
		$this->title=$input->get("title");
		$this->showas=$input->get("showas");
		$this->prodcode=$input->get("prodcode");
		$this->disporder=$input->get("disporder");
		$this->store();

		$this->message="Option group saved";
		$this->redirect="index.php?option=com_simplecaddy&action=products&task=edit&cid[0]=$prodid";
		$this->redirect2();
	}
}
