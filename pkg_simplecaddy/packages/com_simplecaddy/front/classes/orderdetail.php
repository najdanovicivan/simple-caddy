<?php
// No direct access.
defined('_JEXEC') or die;

class orderdetail extends JTable {
	var $id;
	var $orderid;
	var $prodcode;
	var $qty;
	var $unitprice;
	var $total;
	var $shorttext;
	var $option;

	function __construct() {
		$db	= JFactory::getDBO();
		parent::__construct( '#__sc_odetails', 'id', $db );
	}

	function getDetailsByOrderId($orderid){
		$query=$this->_db->getQuery(true);
		$query->select("*");
		$query->from("`$this->_tbl`");
		$query->where("`orderid` = '$orderid'");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}

	function isinorder($orderid, $prodcode) {
		$query=$this->_db->getQuery(true);
		$query->select("`id`");
		$query->from("`$this->_tbl`");
		$query->where("`orderid` = '$orderid'");
		$query->where("`prodcode`='$prodcode' limit 1");

		$this->_db->setQuery($query);
		$id=$this->_db->loadResult();
		$this->load($id);
	}
}
