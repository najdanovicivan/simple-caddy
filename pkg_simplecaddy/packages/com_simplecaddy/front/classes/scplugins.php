<?php
// No direct access.
defined('_JEXEC') or die;

class scplugins {
	function show() {
		$lst=$this->getlist();
		require_once(JPATH_ADMINISTRATOR."/components/com_simplecaddy/views/listplugins.php");
//		display::showPluginlist($lst);
	}

	function getmenuitem() {
		$m=new stdClass();
		$m->title=JText::_('SC_PLUGINS');
		$m->link="index.php?option=com_simplecaddy&action=scplugins&task=show";
		$m->image="components/com_simplecaddy/images/plugins.png";
		$m->alt=JText::_('SC_MANAGE_PLUGINS');
		$m->text=JText::_('SC_PLUGINS');
		$m->active=true;
		return $m;
	}


	function getlist() {
		$db= JFactory::getDbo();
		$query=$db->getQuery(true);
		$query->select("`extension_id`, `enabled`, `element`, `name`");
		$query->from("`#__extensions`");
		$query->where("`params` like '%scregistration%'");
		$db->setQuery($query);
		$lst=$db->loadObjectList();
		return $lst;
	}

	function showpluginconfig() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$pluginname=$input->getCmd("pluginname");

		$db= JFactory::getDbo();
		$query=$db->getQuery(true);
		$query->select("*");
		$query->from("`#__extensions`");
		$query->where("`element` = '$pluginname' ");
		$db->setQuery($query);
		$plugin=$db->loadObject();

		$path=JPATH_PLUGINS."/$plugin->folder/$pluginname/$pluginname.php";

		if (! file_exists($path) ) {
			$msg=JText::_("SC_NO_PLGCONFIG").$path;
			$mainframe= JFactory::getApplication();
			$mainframe->enqueueMessage($msg);
			$mainframe->redirect("index.php?option=com_simplecaddy&action=plugins&task=show");
		}
		require_once($path);
		$pluginclass="{$pluginname}configuration";
		$plg=new $pluginclass();
		$plg->_showconfig();
	}

	function pluginfunction($pluginname, $functionname, $param=null) {
		$input=JFactory::getApplication()->input;
		$pluginname=$input->getCmd("pluginname");
		$db= JFactory::getDbo();
		$query=$db->getQuery(true);
		$query->select("*");
		$query->from("`#__extensions`");
		$query->where("`element` = '$pluginname' ");
		$db->setQuery($query);
		$plugin=$db->loadObject();

		$path=JPATH_PLUGINS."/$plugin->folder/$pluginname/$pluginname.php";
		require_once($path);
		$pluginclass="{$pluginname}configuration";
		$plg=new $pluginclass();
		$plg->$functionname($param);
	}

	function publish(){
		$this->enableplugin();
	}

	function unpublish() {
		$this->enableplugin();
	}

	function enableplugin() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$enabled=$input->getCmd("enable");
		$extension_id=$input->getCmd("id");

		$db =  JFactory::getDbo();
		$query=$db->getQuery(true);
		$query->update("`#__extensions`");
		$query->set("`enabled` = '$enabled'");
		$query->where("`extension_id` = '$extension_id'");
		$db->setQuery($query);
		$db->execute();
		$mainframe= JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
	}
}
