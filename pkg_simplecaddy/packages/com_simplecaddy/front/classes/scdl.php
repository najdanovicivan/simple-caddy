<?php
// No direct access.
defined('_JEXEC') or die;

class scdl_items extends JTable {
	var $id;
	var $filename;
	var $paymentkey;
	var $datetime;

	function __construct() {
		$db	= JFactory::getDBO();
		parent::__construct( '#__sc_downloads', 'id', $db );
	}

	function getlist($paymentkey) {
		$query=$this->_db->getQuery(true);
		$query->select("`id`,`filename`");
		$query->from("$this->_tbl");
		$query->where("`paymentkey`='$paymentkey'");
		$this->_db->setQuery($query);
		$s=$this->_db->loadObjectList();
		return $s;
	}

}

class scdl {
	var $redirect=null;
	var $message=null;
	var $data;

	function redirect() {
		$mainframe=JFactory::getApplication();
		if($this->redirect)
			$mainframe->redirect("$this->redirect", "$this->message");
	}

	function view() {
		$input=JFactory::getApplication()->input;
		$key=$input->getCmd("dlkey");

		if (empty($key)) {
			$this->display("keyform");
			return;
		}

		$cfg=new sc_configuration();
		$dlcid=$cfg->get("downloadcid");
		$this->redirect="index.php?option=com_content&view=article&id=$dlcid&dlkey=$key";
		$this->message="";
	}

	function display($view='default') {
		include_once("views".DS."$view.php");
	}

	function createdlkey($orderid) {
		$order=new order();
		$order->load($orderid);
		$dlkey=$order->ordercode;
		$oi=new orderdetail();
		$lst=$oi->getDetailsByOrderId($orderid);
		$now=time();
		foreach ($lst as $detail) {
			// get the product
			$product=new products();
			$product->getproductByProdCode($detail->prodcode);
			if ($product->downloadable==1) {
				$dl=new scdl_items();
				$dl->id=null; // ensure new item
				$dl->datetime=$now;
				$dl->filename=$product->filename;
				$dl->paymentkey=$dlkey;
				$dl->store();
			}
		}
		return $dlkey;
	}
}
