<?php
// No direct access.
defined('_JEXEC') or die;

class optiongroups extends JTable { // option groups
	var $id;
	var $productid;
	var $prodcode;
	var $title;
	var $showas;
	var $disporder;

	var $redirect;
	var $message;

	function __construct() {
		$db	= JFactory::getDBO();
		parent::__construct( '#__sc_prodoptiongroups', 'id', $db );
	}

	function redirect() {
	}

	function redirect2() {
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage( $this->message);
		$mainframe->redirect($this->redirect);
	}

	function addgroup($prodcode) {
		$product=new products();
		$product->getproductByProdCode($prodcode);
		$this->id=null;
		$this->title=stripslashes( JText::_('SC_UNTITLED_OPTION'));
		$this->showas=1;
		$this->prodcode=$prodcode;
		$this->productid=$product->id;
		$this->disporder=0;
		$this->store();
	}

	function remove() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$id=$input->get('optgrid');
		$prodid=$input->get('productid');
		$this->delete($id);
		$this->message="Option group deleted";
		$this->redirect="index.php?option=com_simplecaddy&action=products&task=edit&cid[0]=$prodid";
		$this->redirect2();
	}

	function getgroups($productid) {
		$query="select * from ".$this->_tbl." where `productid` = '$productid' order by `disporder` asc ";
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}

	function getgroupsbyid($pid) {
		$query=$this->_db->getQuery(true);
		$query->select("*");
		$query->from("$this->_tbl");
		$query->where("`productid` = '$pid'");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}

	function getgroupids($aproductids=array()) {
		$pcs=implode("','", $aproductids);
		$query=$this->_db->getQuery(true);
		$query->select("`id`");
		$query->from("$this->_tbl");
		$query->where("`productid` IN ( '$pcs' )");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadColumn();
		return $lst;
	}

	function deletegroup($id) {
		$query=$this->_db->getQuery(true);
		$query->delete("`$this->_tbl`");
		$query->where("`id`='$id'");
		$this->_db->setQuery($query);
		$this->_db->execute();
	}

	function show() {
		$input = JFactory::getApplication()->input;
		$id=$input->getCmd('optgrid');
		$productid=$input->getCmd('productid');
		$this->load($id);
		display::showoptgroup($this, $productid);
	}

	function saveoptiongroup() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$prodid=$input->get("productid");
		$this->id=$input->get("id");
		$this->title=$input->get("title");
		$this->showas=$input->get("showas");
		$this->prodcode=$input->get("prodcode");
		$this->disporder=$input->get("disporder");
		$this->productid=$prodid;
		$this->store();
		$this->message=JText::_("SC_OPTIONGROUP_SAVED");
		$this->redirect="index.php?option=com_simplecaddy&action=products&task=edit&cid[0]=$prodid";
		$this->redirect2();
	}
}
