<?php
// No direct access.
defined('_JEXEC') or die;

class optionsshowas {
	var $type;

	function __construct() {
		$this->type[1]=JText::_('SC_HORIZ_RADIO');
		$this->type[2]=JText::_('SC_DROPDOWN');
		$this->type[3]=JText::_('SC_STANDARDLIST');
		$this->type[4]=JText::_('SC_VERT_RADIO');
		$this->type[5]=JText::_('SC_SINGLELINE');
		$this->type[6]=JText::_('SC_CALENDAR');
		$this->type[7]=JText::_('SC_IMG_SWATCH');
	}

}
