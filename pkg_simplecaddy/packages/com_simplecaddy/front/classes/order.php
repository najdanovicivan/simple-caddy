<?php
// No direct access.
defined('_JEXEC') or die;

class order extends JTable {
	var $id;
	var $name;
	var $email;
	var $address;
	var $codepostal;
	var $city;
	var $telephone;
	var $ordercode="";
	var $orderdt;
	var $total;
	var $tax;
	var $status;
	var $customfields;
	var $ipaddress;
	var $archive=0;
	var $shipRegion;
	var $shipCost;
	var $shipper;
	var $discount;
	var $j_user_id;
	var $orderlink;
	var $paymentcode;

	function __construct() {
		$db	= JFactory::getDBO();
		parent::__construct( '#__sc_orders', 'id', $db );
	}

	function save($src="", $orderingFilter = '', $ignore = ''){
		$this->store();
		return $this->_db->insertid();
	}

	function orderheadcsv() {
		//$this->to
	}

	function ordertostring($cids=array()) {
		if (count($cids)==0) {
			$query=$this->_db->getQuery(true);
			$query->select("id");
			$query->from("$this->_tbl");
			$this->_db->setQuery($query);
			$cids=$this->_db->loadColumn();
		}
		$field=new fields();
		$aflds=$field->getFieldNames();
		$afields=unserialize($this->customfields);
		$this->afields=$aflds;
		foreach ($aflds as $key=>$value) {
			$this->$value=$afields["$value"];
		}
		$csv="";
		$fldsep=",";
		$recsep="\r\n";
		$csvheader = "orderid".$fldsep."orderdate".$fldsep."total".$fldsep."tax".$fldsep."Shipping Cost".$fldsep."Shipping Region".$fldsep."status";
		foreach ($aflds as $key=>$value) {
			$csvheader .= $fldsep . "$value";
		}

		$csvheader .= $fldsep . "productcode".$fldsep."qty".$fldsep."unitprice".$fldsep."total".$fldsep."shorttext".$fldsep."option".$recsep;
		$f=fopen("components/com_simplecaddy/exports/export.txt", "w+");
		fwrite($f, $csvheader);
		$detlin="";
		foreach ($cids as $key=>$orderid) {
			$this->load($orderid);
			$csvline="$this->id".$fldsep."$this->orderdt".$fldsep."$this->total".$fldsep."$this->tax".$fldsep."$this->shipCost".$fldsep."$this->shipRegion".$fldsep."$this->status".$fldsep;
			foreach ($aflds as $key1=>$value) {
				$csvline .= $this->$value . $fldsep ;
			}

			$detlin="";
			$details=new orders();
			$lst=$details->getOrderDetails($this->id);
			$afields=unserialize($this->customfields);
			foreach ($lst as $d) {
				$detlin .= $csvline . $d->prodcode . $fldsep . $d->qty . $fldsep . $d->unitprice . $fldsep . $d->total . $fldsep . $d->shorttext . $fldsep . $d->option ;
				foreach ($aflds as $key2=>$value) {
					$detlin .= $fldsep . @$afields["$value"] ;
				}
				$detlin .= $recsep;
				fwrite($f, $detlin);
				$detlin="";
			}
		}

		fclose($f);

		$csv=$csvheader;
		$csv .= $detlin;

		return $csv;
	}

	function setOrderTotals($orderid) {
		$this->load($orderid);
		$dets=new orderdetail();
		$lst=$dets->getDetailsByOrderId($orderid);
		$tax=0;
		$shipping=0;
		$total=0;
		foreach ($lst as $d) {
			switch($d->prodcode) {
				case "shipping":
					$shipping += $d->unitprice;
					break;
				case "tax":
					$tax += $d->unitprice;
					break;
				default:
					$total += $d->total;
			}
		}
		$this->tax=$tax;
		$this->shipCost=$shipping;
		$this->total=$total;
		$this->store();
	}
}
