<?php
/**
* @package SimpleCaddy 2.0 for Joomla 2.5
* @copyright Copyright (C) 2006-2012 Henk von Pickartz. All rights reserved.
* General class file
*/
// ensure this file is being included by a parent file
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

class sccontent extends JTable {
	var $id;
	var $introtext;
	var $fulltext;
	var $alias;
	var $catid;

	function __construct() {
		$db	= JFactory::getDBO();
	   	parent::__construct( '#__content', 'id', $db );
	}


	function getlist() {
		$query="select * from `$this->_tbl` where (`introtext` like '%{simplecaddy%' or `fulltext` like '%{simplecaddy%') and `state` >=0 ";
		$query=$this->_db->getQuery(true);
		$query->select("*");
		$query->from("$this->_tbl");
		$query->where("(`introtext` like '%{simplecaddy%' or `fulltext` like '%{simplecaddy%')", "OR");
		$query->where("(`introtext` like '%{sc%' or `fulltext` like '%{sc%')", "OR");
		$query->where("`state` >=0", "AND");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}
}

class sccontents {
	var $redirect;
	var $message;

	function redirect() {
        $mainframe=JFactory::getApplication();
        if ($this->redirect!="") $mainframe->redirect($this->redirect, $this->message);
	}

	function getmenuitem() {
		$m=new stdClass();
		$m->title=JText::_('SC_CONTENT');
		$m->link="index.php?option=com_simplecaddy&action=sccontents&task=show";
		$m->image="components/com_simplecaddy/images/sccontentmngr.png";
		$m->alt=JText::_('SC_MANAGE_CONTENT');
		$m->text=JText::_('SC_CONTENT');
		$m->active=true;
		return $m;
	}

	function show() {
		$scc=new sccontent();
		$lst=$scc->getlist();
		contentdisplay::showlist($lst);
	}
}