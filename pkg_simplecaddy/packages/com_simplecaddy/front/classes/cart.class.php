<?php
/**
* @package SimpleCaddy 3.0 for Joomla 2.5
* @copyright Copyright (C) 2006-2012 Henk von Pickartz. All rights reserved.
* Cart specific functions
*/
// ensure this file is being included by a parent file
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

class cart { // the frontend standard cart processor
	var $redirect;
	var $message;

	function redirect() {
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage($this->message);
		$mainframe->redirect(JRoute::_($this->redirect, false) );
	}

	function add() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$Itemid = $input->get( 'Itemid' );
		$thiscid=$input->get("thiscid");
		$nextcid=$input->get("nextcid");

		$cfg=new sc_configuration();
		$itemseparator = $cfg->get('itemseparator');

		$acpoption=$input->get( 'edtoption', null, "ARRAY");
		$acpoption2=$input->get( 'edtoption2', null, "array" );
		$picname=$input->get("picname");
		$minqty=$input->get("minqty");

		$cartProd=new CartProduct();
		$cartProd->option="";
		$cartProd->formula="";
		$cartProd->caption="";
		$cartProd->prodcode=$input->get('edtprodcode');

		if (is_array($acpoption)) {
			$prodoptions=new productoption();
			foreach ($acpoption as $key=>$value) {
				if (@$acpoption2[$key]<>$key) {
					$cpoption=explode(":", $value);
					$prodoptions->load($cpoption[1]);
					$cartProd->option .=  $acpoption[$cpoption[1]];
					$cartProd->option .= "$itemseparator" . $prodoptions->description;
					$cartProd->formula .= $prodoptions->formula;
					$cartProd->caption .= "$itemseparator" . $prodoptions->caption;
					$cartProd->md5id .= md5($cartProd->prodcode . $prodoptions->description . $picname);
				}
				else
				{
					$cartProd->option .= "$itemseparator" . $value;
					$cartProd->md5id .= md5($cartProd->prodcode . $cartProd->option );
				}
			}
		}
		else
		{
			$cartProd->md5id=$cartProd->prodcode.$acpoption;
		}
		if (@$picname) {
			@$cartProd->option .= "$itemseparator" . $picname;
		}

		$cartProd->prodname=$input->get( 'edtshorttext', "", "string");
		// added security => retrieve original product from DB and not from session vars
		$product=new products();
		$p=$product->getproductByProdCode($cartProd->prodcode);
		$cartProd->unitprice=$p->unitprice; // retrieved from DB, not from session
		$cartProd->quantity=abs($input->get( 'edtqty' ) ); // restrict to positive integer values, fractions not allowed
		if ($cartProd->quantity < $minqty ) $cartProd->quantity = $minqty;
		$cartProd->finalprice= matheval("$cartProd->unitprice $cartProd->formula");

		// check for minimum order quantities
		if ($cfg->get("checkoos")==1) { // check if we need to check
			$product=new products();
			$p=$product->getproductByProdCode($cartProd->prodcode);
			$cart=new cart2();
			$cp=$cart->getCartProduct($p);
			if ( ($p->av_qty + $cp->quantity) < $cartProd->quantity) { // if less than minimum quantity ordered
				$lasturl="index.php?option=com_simplecaddy&action=showcart&Itemid=$Itemid";
				$mainframe->enqueueMessage(JText::_("SC_MINQTY_WARNING"));
				$mainframe->redirect($lasturl); // stay on the same page and issue a warning message
			}
		}

		$cart2=new cart2();
		$cart2->addCartProduct($cartProd); // add the product to the cart
		$usestdprod=$cfg->get("usestdproduct");
		if ($usestdprod==1) {
			AddStandard();
		}
		if ($nextcid) {
			$url=JRoute::_(ContentHelperRoute::getArticleRoute( $nextcid ), false);
//			$mainframe->enqueueMessage($url);
			$mainframe->redirect($url);
		}
		else
		{
			$thiscid=$input->get("thiscid");
			$url=JRoute::_(ContentHelperRoute::getArticleRoute( $thiscid ), false);
//			$mainframe->enqueueMessage($url);
			$mainframe->redirect($url);
		}
	}

	function changeqty() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$Itemid = $input->get( 'Itemid' );
		$thiscid=$input->get("thiscid");
		$nextcid=$input->get("nextcid");

		$prodcode=$input->get( 'edtprodcode');
		$product=new products();
		$p=$product->getproductByProdCode($prodcode);

		$cartProd=new CartProduct();
		$cartProd->md5id=$input->get( 'edtid');
		$cartProd->prodcode=$prodcode;
		$cartProd->quantity=abs($input->getInt( 'edtqty' )); // restrict to positive integer values
		$cartProd->unitprice=$p->unitprice; // retrieve price from DB not from session vars
		$cartProd->finalprice= matheval("$cartProd->unitprice $cartProd->formula"); // recalculate price

		$cfg=new sc_configuration();
		if ($cfg->get("checkoos")==1) {
			// check for available quantity before making the change in the cart
			$product=new products();
			$p=$product->getproductByProdCode($cartProd->prodcode);
			if ($p->av_qty < $cartProd->quantity) {
				$lasturl="index.php?option=com_simplecaddy&action=showcart&Itemid=$Itemid";
				$mainframe->enqueueMessage(JText::_("SC_MINQTY_WARNING"));
				$mainframe->redirect($lasturl);
			}
		}

		$cart2=new cart2();
		$cart2->setCartProductQty($cartProd);

		$cfg = new sc_configuration();
		$usestdprod=$cfg->get("usestdproduct");
		if ($usestdprod==1) {
			AddStandard();
		}
		$url = ContentHelperRoute::getArticleRoute($thiscid) ;
		$this->redirect=$url;
	}

	function scempty() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$thiscid=$input->get("thiscid");
		//$nextcid=$input->get("nextcid");
		$cart2=new cart2();
		$cart2->destroyCart();
		$url =ContentHelperRoute::getArticleRoute($thiscid) ;
		$this->redirect=$url;
		$this->message=JText::_("SC_CART_EMPTIED");
	}

	function skip() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		//$thiscid=$input->get("thiscid");
		$nextcid=$input->get("nextcid");
		$url =ContentHelperRoute::getArticleRoute($nextcid) ;
		$this->redirect=$url;
	}

	function checkout() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$thiscid=$input->get("thiscid");
		$nextcid=$input->get("nextcid");
		$cart=new cart2();
		$cfg=new sc_configuration();
		$mintocheckout=$cfg->get("mincheckout");
		// check for minimum amount before checkout, default = 0 => any amount is enough
		if (!$mintocheckout) $mintocheckout=0;
		$carttotal= $cart->getCartTotal();
		if ( $carttotal < $mintocheckout ) {
			$txt=JText::_('SC_LESS_THAN_MIN_AMOUNT', $mintocheckout );
			$url = ContentHelperRoute::getArticleRoute($thiscid) ;
			$mainframe->enqueueMessage($txt);
			$mainframe->redirect($url);
		}
		$url =ContentHelperRoute::getArticleRoute($nextcid) ;
		$data=$input->get("data");
		if (strpos($url, "?")>0) {
			$url .= "&data=".$data;
		}
		else
		{
			$url .= "?data=".$data;
		}
		$this->redirect=$url;
	}

	function gotopayment() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		//$thiscid=$input->get("thiscid");
		$nextcid=$input->get("nextcid");
		$data=$input->get("data");
		$sp=new sccontent();
		$sp->load($nextcid);
		$url =ContentHelperRoute::getArticleRoute($nextcid) ;
		if (strpos($url, "?")>0) {
			$url .= "&data=".$data;
		}
		else
		{
			$url .= "?data=".$data;
		}
		$this->redirect=$url;
	}

	function allconfirm() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$nextcid=$input->get("nextcid");
		$thiscid=$input->get("thiscid");
		$errors=checkerrors();
		$cfg=new sc_configuration();
		if ($errors==0) {
			$cart=new cart2();
			$mycart=$cart->readCart();
			// store the order in db
			$order=new orders();
			$orderid = $order->store_new_order($mycart);
			$scdl=new scdl();
			$dlkey=$scdl->createdlkey($orderid); // inscribes the downloadables in the *sc_downloads table and returns the download key
//			$cart=new cart2();
			//		$cart->destroyCart(); // empty all session vars of the cart, no visual return
			$url =ContentHelperRoute::getArticleRoute($nextcid) ;
			// now go to shipping, taxes or checkout... anyway deliver the order code as well
			if (strpos($url, "?")>0) {
				$url .= "&data=".$dlkey;
			}
			else
			{
				$url .= "?data=".$dlkey;
			}
			$this->redirect=$url;
		}
		else // some required info is missing or incorrect
		{
			$fields=new fields();
			$fieldlist=$fields->getPublishedFields();
			$fielddata=$_POST; // get posted fields back
			$encfielddata=base64_encode(serialize($fielddata));
			$sp=new sccontent();
			$sp->load($thiscid);
			$url = ContentHelperRoute::getArticleRoute($thiscid) ;
			if (strpos($url, "?")>0) {
				$url .= "&data=".urlencode($encfielddata);
			}
			else
			{
				$url .= "?data=".urlencode($encfielddata);
			}
			$mainframe->enqueueMessage(JText::_('SC_REQUIRED_MISSING'));
			$mainframe->redirect($url );
		}

	}
}


class CartProduct {
    var $prodcode;
    var $prodname;
    var $option;
    var $quantity=1;
    var $unitprice;
    var $formula="";
    var $md5id="";
    var $caption="";
    var $finalprice;

    function __construct($aa=null) // sets the CartProduct class variables
    {
    	if ($aa!=null) { // could be null in case directly created
 	       foreach ($aa as $k=>$v) {
				$this->$k = $aa[$k];
			}
		}
    }
}

class cart2 {
	var $redirect;
	var $message;

	function destroyCart() {
		if (isset($_SESSION)) {
			@$_SESSION['SimpleCart']=""; // empty the cart
		}
	}

	function readCart() { // reads cart from session cookie
		if (!isset($_SESSION)) {
			session_start(); // cart needs a session cookie
		}
		$data = @$_SESSION['SimpleCart']; // suppress any legitimate warnings, after all the cart might be empty
		if (!$data) return; // no data in the cart
//		$data=urldecode($data);
	    $parser = xml_parser_create();
	    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
	    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
	    xml_parse_into_struct($parser, $data, $values, $tags);
	    xml_parser_free($parser);
		$tdb=array();
	    foreach ($tags as $key=>$val) {
	        if ($key == "product") {
	            $ranges = $val;
	            for ($i=0; $i < count($ranges); $i+=2) {
	                $offset = $ranges[$i] + 1;
	                $len = $ranges[$i + 1] - $offset;
	                $tdb[] = @$this->parseProduct(array_slice($values, $offset, $len));
	            }
	        } else {
	            continue;
	        }
	    }
	    return $tdb;
	}

	function parseProduct($mvalues) {
	    for ($i=0; $i < count($mvalues); $i++) {
	        $prod[$mvalues[$i]["tag"]] = urldecode($mvalues[$i]["value"]);
	    }
	    return new CartProduct($prod);
	}

	function dumpCartArray() {
		$dump=$this->readCart();
		echo "Array content of Cart<pre>\n";
		print_r($dump);
		echo "</pre>";
	}

	function dumpCartXML() {
		$cartArray=$this->readCart();
		$cartXML=$this->makeCartXML($cartArray);
		printf("<pre>%s</pre>", htmlspecialchars($cartXML));
	}

	function setCartProductQty($p) {
		$cartArray=$this->readCart();
		$todelete=false;
		if (count($cartArray)>0) {
			foreach ($cartArray as $key=>$prod) {
				if ($prod->md5id == $p->md5id) {
					$prod->quantity = $p->quantity;
					$cartArray[$key]=$prod; // put back into the array for php4 compatibility
					if ($p->quantity==0) { // nothing left
						$todelete=true;
						$which=$key;
						break; // we have found the product, no need to do the rest
					}
				}
			}
			if ($todelete) {
				array_splice($cartArray, $which, 1);
			}
		}
		$cartXML=$this->makeCartXML($cartArray);
		$this->writeCart($cartXML);
		return $cartXML;
	}

	function getCartTotal($exclude=array()) {
		$total=0;
		$cartArray=$this->readCart();
		if (count($cartArray)==0) return $total;
		foreach ($cartArray as $key=>$prod) {
			if (!in_array($prod->prodcode, $exclude)) $total=$total + ($prod->quantity * $prod->finalprice);
		}
		return $total;
	}

	function getCartproductCodes($exclude=array()) {
		$res=array();
		$cartArray=$this->readCart();
		if (count($cartArray)==0) return $res;
		foreach ($cartArray as $key=>$prod) {
			if (!in_array($prod->prodcode, $exclude)) $res[$prod->prodcode]=$prod->quantity;
		}
		return $res;
	}

	function getCartNumbers() {
		$total=0;
		$cartArray=$this->readCart();
		return count($cartArray);
	}

    function getCartQuantities() {
        $total=0;
		$cartArray=$this->readCart();
        $number=count($cartArray);
		if ($number==0) {
    		return $total;
		}
		foreach ($cartArray as $key=>$prod) {
			$total=$total + $prod->quantity;
		}
		return $total;
    }

	function addCartProduct($p) {
		$cartArray=$this->readCart();
		$toadd=true; // consider we dont have it yet
		if (count($cartArray)>0) {
		foreach ($cartArray as $key=>$prod) {
			if (($prod->md5id == $p->md5id)) {
				$prod->quantity = $prod->quantity+$p->quantity;
				$toadd=false;
				break; // we have found the product, no need to do the rest
				}
			}
		}
		if ($toadd) {
			$cartArray[]=$p; // add the product to the array
		}

		$cartXML=$this->makeCartXML($cartArray);
		$this->writeCart($cartXML);
		return $cartXML;
	}

	function getCartProduct($p) {
		$cartArray=$this->readCart();
		$toadd=true; // consider we dont have it yet
		if (count($cartArray)>0) {
		foreach ($cartArray as $key=>$prod) {
			if (($prod->md5id == $p->md5id)) {
				$toadd=false;
				break; // we have found the product, no need to do the rest
				}
			}
		}
		if ($toadd) {
			return $prod;
		}
		return false;
	}

	function removeCartProduct($p) {
		$cartArray=$this->readCart();
		$todelete=false;
		if (count($cartArray)>0) {
			foreach ($cartArray as $key=>$prod) {
				if (($prod->prodcode == $p->prodcode) and ($prod->option==$p->option)) {
					$prod->quantity = $prod->quantity-1;
					if ($prod->quantity==0) { // nothing left
						$todelete=true;
						$which=$key;
						break; // we have found the producty, no need to do the rest
					}
				}
			}
			if ($todelete) {
				array_splice($cartArray, $key, 1);
			}
		}
		$cartXML=$this->makeCartXML($cartArray);
		$this->writeCart($cartXML);
		return $cartXML;
	}

	function makeCartXML($cartArray){ // make xml string from the array with cart contents
		$cartxml="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		$cartxml.="\n<cart>";
		$tmp=new CartProduct();
		$pv=get_object_vars($tmp);
		if (count($cartArray)>0){ // make sure we have something to add
			foreach ($cartArray as $key=>$product) {
				$cartxml .= "\n\t<product>";
				foreach ($pv as $key=>$v) {
					$xmlkey=urlencode($key);
					$cartxml .= "\n\t\t<$key>".urlencode($product->$key)."</$key>";
				}
				$cartxml .= "\n\t</product>";
			}
		}
		$cartxml .= "\n</cart>";
		return $cartxml;
	}

	function isInCart($prodcode) {
		$res=false;
		$cartArray=$this->readCart();
		$todelete=false;
		if (count($cartArray)>0) {
			foreach ($cartArray as $key=>$prod) {
				if (($prod->prodcode == $prodcode) ) {
					$res=true;
					break; // we have found the producty, no need to do the rest
				}
			}
		}
		return $res;
	}

	function writeCart($cartXML) { // write cart back to session cookie
	if (!isset($_SESSION)) session_start();
		$_SESSION['SimpleCart']=$cartXML;
	}
}
?>