<?php
// No direct access.
defined('_JEXEC') or die;

class scmodule extends JTable {
	function __construct() {
		$db	= JFactory::getDBO();
		parent::__construct( '#__sc_modules', 'id', $db );
	}

	function getlist() {
		$query="select * from $this->_tbl order by ordering asc";
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}
}

class scmodules {

	function getmodules() {
		$m=new scmodule();
		$lst=$m->getlist();
		return $lst;
	}
}