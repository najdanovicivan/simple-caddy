<?php
// No direct access.
defined('_JEXEC') or die;

class config_key extends JTable {
	var $id;
	var $keyword;
	var $description;
	var $setting;
	var $cfgset;
	var $type;
	var $indopts;
	var $sh;
	var $sv;
	var $pagename;

	function __construct() {
		$db	= JFactory::getDBO();
		parent::__construct( '#__sc_config', 'id', $db );
	}
}

class sc_configuration {
	var $cfgset=0;

	function __construct($cfgset=0) {
		$this->cfgset=$cfgset;
	}

	function getmenuitem() {
		$m=new stdClass();
		$m->title=JText::_('SC_CONFIGURATION');
		$m->link="index.php?option=com_simplecaddy&action=sc_configuration&task=show";
		$m->image="components/com_simplecaddy/images/config.png";
		$m->alt=JText::_('SC_CONFIGURATION');
		$m->text=JText::_('SC_CONFIGURATION');
		$m->active=true;
		return $m;
	}

	function cancel() {
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage(JText::_("SC_CONFIGURATION_REVERT"));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=configuration&task=show");
	}

	function get($kw) {
		$db	= JFactory::getDBO();
		$query=$db->getQuery(true);
		$query->select("`setting`");
		$query->from("`#__sc_config`");
		$query->where("keyword='$kw'");
		$query->where("cfgset='$this->cfgset'");
		$db->setQuery($query);
		return trim($db->loadResult());
	}

	function getAll() {
		$db	= JFactory::getDBO();
		$query=$db->getQuery(true);
		$query->select("*");
		$query->from("`#__sc_config`");
		$query->where("`cfgset`='$this->cfgset'");
		$query->order("pagename asc");
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	function saveconfig() {
		$mainframe = JFactory::getApplication();
		$input     = $mainframe->input;
		$this->cfgset=$input->get("cfgset");
		$db	= JFactory::getDBO();
		$req=$_REQUEST;

        foreach ($req as $key=>$value) {
            if (substr($key, 0, 9) == "emailcid-") {
                $emailContendIds[substr($key, 9)] = $value;
            }
            else if (substr($key, 0, 15) == "email_fromname-") {
                $emailFromNames[substr($key, 15)] = $value;
            }
            else if (substr($key, 0, 11) == "email_from-") {
                $emailFrom[substr($key, 11)] = $value;
            }
            else if (substr($key, 0, 14) == "email_subject-") {
                $emailSubject[substr($key, 14)] = $value;
            }
        }

        $emailCids=serialize($emailContendIds);
        $query=$db->getQuery(true);
        $query->update("`#__sc_config`");
        $query->set("`setting`='$emailCids'");
        $query->where("keyword='emailcid'");
        $query->where("`cfgset`='$this->cfgset' limit 1;");
        $db->setQuery($query);
        $db->execute();

        $emailFrn=serialize($emailFromNames);
        $query=$db->getQuery(true);
        $query->update("`#__sc_config`");
        $query->set("`setting`='$emailFrn'");
        $query->where("keyword='email_fromname'");
        $query->where("`cfgset`='$this->cfgset' limit 1;");
        $db->setQuery($query);
        $db->execute();

        $emailFrm=serialize($emailFrom);
        $query=$db->getQuery(true);
        $query->update("`#__sc_config`");
        $query->set("`setting`='$emailFrm'");
        $query->where("keyword='email_from'");
        $query->where("`cfgset`='$this->cfgset' limit 1;");
        $db->setQuery($query);
        $db->execute();

        $emailSub=serialize($emailSubject);
        $query=$db->getQuery(true);
        $query->update("`#__sc_config`");
        $query->set("`setting`='$emailSub'");
        $query->where("keyword='email_subject'");
        $query->where("`cfgset`='$this->cfgset' limit 1;");
        $db->setQuery($query);
        $db->execute();

        foreach ($req as $key=>$value) {
			if (substr($key, 0, 3)=="edt") // only edt* fields
			{
				$cfg=new config_key();
				$setting=substr($key,3,32);
				$query=$db->getQuery(true);
				$query->update("`#__sc_config`");
				$query->set("`setting`='$value'");
				$query->where("keyword='$setting'");
				$query->where("`cfgset`='$this->cfgset' limit 1;");
				$db->setQuery($query);
				$db->execute();
			}
		}
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage(JText::_("SC_CONFIGURATION_SAVED"));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=configuration&task=show");
	}

	function show() {
		$mainframe=JFactory::getApplication();
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		jimport( 'joomla.methods' );
		jimport ('joomla.html.html.bootstrap');

		$options = array(
			'onActive' => 'function(title, description){
		        description.setStyle("display", "block");
		        title.addClass("open").removeClass("closed");
		    }',
			'onBackground' => 'function(title, description){
		        description.setStyle("display", "none");
		        title.addClass("closed").removeClass("open");
		    }',
			'useCookie' => 'true', // note the quotes around true, since it must be a string. But if you put false there, you must not use qoutes otherwise JHtmlTabs will handle it as true
			'active' => 'tab1_j31_0',
		);

		$cfg=$this->getAll();
		JToolBarHelper::title( JText::_( 'SIMPLECADDY_CONFIGURATION' ));
		JToolBarHelper::custom( 'saveconfig', 'save.png', 'save_f2.png', 'Save', false,  false );
		JToolBarHelper::cancel();
		JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false,  false );
		display::header();
		?>
		<form method="post" name="adminForm" action="index.php" id="adminForm">
			<?php
			echo JHtml::_('bootstrap.startTabSet', 'ID-Tabs-J31-Group', $options);
			$currentpage='';
			$i=0;
			foreach ($cfg as $conf) {
				if ($currentpage<>$conf->pagename) {
					if ($currentpage) {
						echo JHtml::_('bootstrap.endTab');
					}
					$currentpage=$conf->pagename;
					echo JHtml::_('bootstrap.addTab', 'ID-Tabs-J31-Group', "tab1_j31_$i", JText::_($currentpage));
					$i++;
				}

				echo "\n<div style='display:inline'>".JText::_($conf->description)."</div>";
				switch ($conf->type) {
					case "text":
                        if ($conf->keyword == "emailcid" || $conf->keyword == "email_fromname" || $conf->keyword == "email_from" || $conf->keyword == "email_subject"){

                            $unserializedSettings = unserialize($conf->setting);

                            $db = JFactory::getDbo();
                            $query="SELECT sef, title_native, lang_code FROM #__languages ORDER BY sef ASC";
                            $db->setQuery($query);
                            $languages = $db->loadObjectList();
                            foreach ($languages as $l){

                                echo "\n<div style='display:inline'>" . JText::_($conf->description) . " ".$l->title_native . "</div>";
                                echo "<div>";
                                $value = $unserializedSettings[$l->lang_code];
                                echo "<input type=\"text\" name=\"$conf->keyword-$l->lang_code\" value=\"$value\" size=\"$conf->sh\">";
                                echo "</div>";
                            }

                        }else {
                            echo "<div>";
                            echo "<input type=\"text\" name=\"edt$conf->keyword\" value=\"$conf->setting\" size=\"$conf->sh\">";
                            echo "</div>";
                        }
						break;
					case "textarea":
						echo "<div>";
						echo "<textarea name=\"edt$conf->keyword\" cols=\"$conf->sh\" rows=\"$conf->sv\">$conf->setting</textarea>";
						echo "</div>";
						break;
					case "richtext":
						echo "<div>";
						editorArea( 'editor1', $conf->setting, "edt$conf->keyword", '100%', '350', '75', '20' ) ;
						echo "</div>";
						break;
					case "yesno":
						echo "<div class='control-group'>";
						echo '<div class="controls">';
						echo '<fieldset class="radio btn-group">';
						echo "<label for='0edt$conf->keyword'><input type='radio' id='0edt$conf->keyword' value='0' name='edt$conf->keyword'". ($conf->setting==0? 'checked="checked"':'') ." >No</label>";
						echo "<label for='1edt$conf->keyword'><input type='radio' id='1edt$conf->keyword' value='1' name='edt$conf->keyword'". ($conf->setting==1? 'checked="checked"':'') ." >Yes</label>";
						echo '</fieldset>';
						echo "</div>";
						echo "</div>";
						break;
					case "list":
						echo "<div>";
						echo "<select name='edt$conf->keyword'>";
						$txtoptlist=trim($conf->indopts);
						$pairoptlist=explode("\r\n",$txtoptlist);
						foreach ($pairoptlist as $k=>$value) {
							$aline=explode(":", trim($value));
							echo "<option value='".$aline[1]."'".($conf->setting==$aline[1]?" selected":"").">".$aline[0]."</option>\n";
						}
						echo "</select>";
						echo "</div>";
						break;
				}
				echo "\n";
			}
			?>
			<?php echo JHtml::_('bootstrap.endTabSet');	?>
			<input type="hidden" name="option" value="com_simplecaddy" />
			<input type="hidden" name="action" value="sc_configuration" />
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="cfgset" value="<?php echo $this->cfgset;?>" />
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="hidemainmenu" value="0" />
		</form>
		<?php
	}
}
