<?php
// No direct access.
defined('_JEXEC') or die;

class products extends JTable {
	var $id=null;
	var $prodcode="";
	var $shorttext="";
	var $av_qty=0;
	var $unitprice=0;
	var $published=0;
	var $showas=1;
	var $options="";
	var $optionstitle="";
	var $category="";
	var $shippoints=0;
	var $downloadable=0;
	var $filename;
	var $shiplength;
	var $shipwidth;
	var $shipheight;
	var $shipweight;
	var $userid;

	function __construct() {
		$db	= JFactory::getDBO();
		parent::__construct( '#__sc_products', 'id', $db );
	}

	function getmenuitem() {
		$m=new stdClass();
		$m->title=JText::_('SC_PRODUCTS');
		$m->link="index.php?option=com_simplecaddy&action=products&task=show";
		$m->image="components/com_simplecaddy/images/products.png";
		$m->alt=JText::_('SC_MANAGE_PRODUCTS');
		$m->text=JText::_('SC_PRODUCTS');
		$m->active=true;
		return $m;
	}

	function close(){
		$this->cancel();
	}
	function cancel(){
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=products&task=show");
	}

	function show() {
		$input=JFactory::getApplication()->input;
		$search=$input->get("search");
		$alist=$this->getAllProducts($search);
		require_once(JPATH_ADMINISTRATOR."/components/com_simplecaddy/views/listproducts.php");
	}

	function decstore() {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$pid=$input->get( 'pid' );
		$qty=$input->get( 'qty' );
		$oid=$input->get( 'order' );
		$this->decfromstore($pid, $qty);
		$mainframe->redirect("index.php?option=com_simplecaddy&action=orders&task=edit&cid[]=$oid");
	}

	function add() {
		$this->edit();
	}

	function edit() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$cid=$input->get( 'cid', array(0), 'ARRAY' );
		$product= new products();
		$product->getproduct($cid[0]);
		display::header();
		JHTML::_('behavior.modal');
		JToolBarHelper::title( JText::_( "SC_SIMPLECADDY_EDIT" ), 'generic.png');
		JToolBarHelper::save( 'save', JText::_("SC_SAVE_CLOSE") );
		JToolBarHelper::apply();
		JToolBarHelper::cancel( 'cancel', 'Close' );
		require_once(JPATH_ADMINISTRATOR."/components/com_simplecaddy/views/editproduct.php");
	}

	function apply() {
		$id=$this->saveproduct();
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage(JText::_('SC_PRODUCTSAVED'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=products&task=edit&cid[]=$id");
	}

	function save($src=null, $orderingFilter = '', $ignore = ''){
		$this->saveproduct();
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage(JText::_('SC_PRODUCTSAVED'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=products&task=show");
	}

	function getProduct($id) {
		$this->load($id);
	}

	function getProductCodeList() {
		$query=$this->_db->getQuery(true);
		$query->select("`prodcode`, `shorttext`");
		$query->from("$this->_tbl");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}

	function getAllProducts($filter=null) {
		$mainframe = JFactory::getApplication();
		$input     = $mainframe->input;

		$limitstart=$input->get("limitstart",0);
		$limit=$input->get("limit");

		if (!$limit) {
			$cfg=JFactory::getConfig();
			$limit=$cfg->get("list_limit");
		}

		$filter_order=$input->getCmd('filter_order');
		$filter_order_dir=$input->getCmd('filter_order_Dir');

		$query=$this->_db->getQuery(true);
		$query->select("count(*) as total");
		$query->from("$this->_tbl");
		if($filter) $query->where("category = '$filter'");
		$this->_db->setQuery($query);
		$total=$this->_db->loadResult();

		$pageNav = new JPagination( $total, $limitstart, $limit );
		$query=$this->_db->getQuery(true);
		$query->select("*");
		$query->from("$this->_tbl");

		if ($filter) $query->where("category = '$filter'");
		if ($filter_order) $query->order("$filter_order $filter_order_dir ");
		$this->_db->setQuery($query, $limitstart, $limit);
		$lst=$this->_db->loadObjectList();

		$query=$this->_db->getQuery(true);
		$query->select("distinct category");
		$query->from("$this->_tbl");
		$this->_db->setQuery($query);
		$lstcategories=$this->_db->loadObjectList();
		$categories[]=array('value'=>"", 'text'=>"", "");

		if ($lstcategories) {
			foreach ($lstcategories as $cat) {
				$categories[]=array('value'=>$cat->category, 'text'=>$cat->category, $filter );
			}
		}
		else
		{
			$categories[]=array('value'=>'None defined', 'text'=>"No categories selectable");
		}
		$lists['category'] = JHTML::_('select.genericlist',  $categories, 'search', 'class="inputbox" size="1" onchange="document.adminForm.submit( );"', 'value', 'text', $filter);
		$lists["order_Dir"]="$filter_order_dir";
		$lists["order"]="$filter_order";

		$res['lst']=$lst;
		$res['total']=$total;
		$res['nav']=$pageNav;
		$res['lists']=$lists;
		return $res;
	}

	function getPublishedProducts() {
	//	$query="select * from ".$this->_tbl." where published=1";
	//	$query .= " order by `category`, `shorttext` asc ";
		$query=$this->_db->getQuery(true);
		$query->select("*");
		$query->from("$this->_tbl");
		$query->where("published=1");
		$query->order("category, shorttext asc");
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}

	function saveproduct() {
		$forbiddenchars=array(" ", ".", ",", "/", "$", "\\");
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$id=$input->getCmd('id');
		$this->load($id);
		$this->bind($_REQUEST);
		$this->prodcode=strtolower($this->prodcode);
		$this->prodcode=str_replace($forbiddenchars, "", $this->prodcode);
		$user=JFactory::getUser();
		if ($this->userid==0) $this->userid=$user->id;
		$this->store();
		if ($this->id) { //previously saved product
			// save the optiongroups as well to reflect possible changes in prodcode
			$optgroups=new optiongroups();
			$lst=$optgroups->getgroupsbyid($this->id);
			foreach ($lst as $optgroup) {
				$og=new optiongroups();
				$og->load($optgroup->id);
				$og->prodcode=$this->prodcode;
				$og->store();
			}
		}
		return $this->id;
	}

	function duplicate() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$cid=$input->get("cid", null, "ARRAY");
		$pid=$cid[0];
		$this->load($pid);
		$this->id=null;
		$this->shorttext = JText::_("SC_COPY_OF") .$this->prodcode ." ". $this->shorttext;
		$this->store();
		$newid=$this->_db->insertid();

		$this->load($newid);
		$optgroups=new optiongroups();
		$lst=$optgroups->getgroupsbyid($pid);

		foreach ($lst as $optgroup) {
			$og=new optiongroups();
			$og->load($optgroup->id);
			$og->id=null;
			$og->productid=$newid;
			$og->store();
			$newogid=$og->_db->insertid();
			$opt=new options();
			$lst=$opt->getindoptionids(array($optgroup->id) );
			foreach ($lst as $key=>$value) {
				$o=new options();
				$o->load($value);
				$o->id=null;
				$o->optgroupid=$newogid;
				$o->store();
			}
		}
		$this->prodcode=null; // prep for duplication
		$mainframe->redirect("index.php?option=com_simplecaddy&action=products&task=edit&cid[]=$newid&dup=1");
	}

	function getproductByProdCode($prodcode) {
		$query=$this->_db->getQuery(true);
		$query->select("*");
		$query->from("$this->_tbl");
		$query->where("prodcode='$prodcode'");
		$this->_db->setQuery($query);
		$p=$this->_db->loadObject();
		$this->load(@$p->id);
		return $p;
	}

	function publish($pks = NULL, $state = 1, $userId = 0) {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$cid=$input->get( 'cid' , array(), "", "ARRAY" );
		$cids = implode( ',', $cid );
		$query=$this->_db->getQuery(true);

		$query->update("`$this->_tbl`");
		$query->set("`published` = 1");
		$query->where("`id` IN ( '$cids' )");
		$this->_db->setQuery( $query );
		$this->_db->execute();
		echo $this->_db->getQuery();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=products&task=show");
	}

	function unPublish() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$cid=$input->get( 'cid', array(), 'ARRAY' );
		$cids = implode( ',', $cid );
		$query=$this->_db->getQuery(true);
		$query->update("$this->_tbl");
		$query->set("published = 0 "  );
		$query->where("id IN ( $cids )");
		$this->_db->setQuery( $query );
		$this->_db->execute();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=products&task=show");
	}

	function remove() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only
		$cid=$input->get("cid", array(), "ARRAY");
		$cids = implode( ',', $cid );
		$query=$this->_db->getQuery(true);
		$query->select("`prodcode`");
		$query->from("`$this->_tbl`");
		$query->where("`id` in ( $cids )");
		$this->_db->setQuery( $query );

		$prodcodes = $this->_db->loadColumn();

		$og=new optiongroups();
		$gids=$og->getgroupids($cid);
		$io=new options();
		$lst=$io->getindoptionids($gids);
		foreach ($lst as $key=>$value) {
			$io->deleteindoption($value);
		}
		foreach ($gids as $key=>$value) {
			$og->deletegroup($value);
		}

		$query=$this->_db->getQuery(true);
		$query->delete("$this->_tbl");
		$query->where("`id` IN ( $cids )");
		$this->_db->setQuery( $query );
		$this->_db->execute();
		JFactory::getApplication()->enqueueMessage(JText::_('SC_PRODUCTDELETED'));
		$mainframe->redirect("index.php?option=com_simplecaddy&action=products&task=show");
	}

	function decfromstore($pid, $qty) {
		$query = "UPDATE ".$this->_tbl." set av_qty= av_qty - $qty WHERE prodcode = '$pid' limit 1";
		$this->_db->setQuery( $query );
		$this->_db->query();
	}

	function addoptgroup() {
		$mainframe=JFactory::getApplication();
		$input = $mainframe->input;
		if($mainframe->isClient("site")) return false; // no site usage, admin only

		$prodid=$input->get("id");
		$prodcode=$input->get("prodcode");
		$og=new optiongroups();
		$og->addgroup($prodcode);
		$mainframe->redirect("index.php?option=com_simplecaddy&action=products&task=edit&cid[0]=$prodid");

	}
}
