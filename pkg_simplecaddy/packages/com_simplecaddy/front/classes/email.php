<?php
// No direct access.
defined('_JEXEC') or die;

class email {
	function mailorder($orderid=null) {
		if (!$orderid) return false;
		$cfg=new sc_configuration();
		$tsep=$cfg->get('thousand_sep');
		$dsep=$cfg->get('decimal_sep');
		$decs=$cfg->get('decimals');
		//$currency=$cfg->get('currency');
		//$curralign=$cfg->get('curralign');
		$dateformat=$cfg->get('dateformat');
		$timeformat=$cfg->get('timeformat');
		$mode=1; // always html
		$usecontentasemail=$cfg->get("usecidasemail");

// create html orderheader
		$db	= JFactory::getDBO();
		$query=$db->getQuery(true);
		$query->select("#__sc_orders.*, #__sc_orders.total as gtotal");
		$query->from("#__sc_orders");
		$query->where("#__sc_orders.id='$orderid'");
		$db->setQuery($query);
		$header=$db->loadObject();

		if ($header->emailsent == 1)
		    return true;

		$hhtml = ""; // header html
		$hhtml .= "\n<br />".JText::_('SC_ORDER');
		$hhtml .= "\n<br />".date("$dateformat $timeformat", $header->orderdt);
		$hhtml .= "\n<br />$header->name";
		$hhtml .= "\n<br />$header->email";
		$hhtml .= "\n<br />".nl2br($header->address);
		$hhtml .= "\n<br />$header->codepostal";
		$hhtml .= "\n<br />$header->city";
		$hhtml .= "\n<br />$header->telephone";
		$hhtml .= "\n<br />$orderid";

// create html order details block
		$odetails=new orderdetail();
		$detailslist=$odetails->getDetailsByOrderId($orderid);

		$dhtml = "<p>"; // detail html
		$dhtml .= "<table width='100%' border='1'>\n";
		$dhtml .= "<tr><th>".JText::_('SC_CODE')."</th><th>".JText::_('SC_DESCRIPTION')."</th><th>".JText::_('SC_PRICE_PER_UNIT')."</th><th>".JText::_('SC_QUANTITY')."</th><th>".JText::_('SC_TOTAL')."</th></tr>";
		foreach ($detailslist as $detail) {
			$dhtml .= "<tr><td>$detail->prodcode</td>\n";
			$dhtml .= "<td>$detail->shorttext - $detail->option</td>\n";
			$dhtml .= "<td>".number_format($detail->unitprice, $decs, $dsep, $tsep)."</td>\n";
			$dhtml .= "<td>$detail->qty</td>\n";
			$dhtml .= "<td><strong>".number_format($detail->qty*$detail->unitprice, $decs, $dsep, $tsep)."</strong></td>\n";
		}

		$dhtml .= "<tr><td colspan='2'><td colspan='2'>".JText::_('SC_TOTAL')."</td>";
		$dhtml .= "<td>".number_format($header->gtotal, $decs, $dsep, $tsep)."</td></tr>\n";
		$dhtml .= "</table>\n";
		$dhtml .= "</p>";

        //get the current site language
        $lang = JFactory::getLanguage();

		if ($usecontentasemail==1) {

            ///unserialize "emailcid" to array
            $emailContendIds = unserialize($cfg->get("emailcid"));
            //get contentId for language by tag
            $contentemail=$emailContendIds[$lang->getTag()];


			$query=$db->getQuery(true);
			$query->select('introtext');
			$query->from("#__content");
			$query->where("id = '$contentemail'");
			$db->setQuery($query);
			$emailcontent=$db->loadResult();
			$fields=new fields();
			$fieldslist=$fields->getPublishedFields() ;// the custom fields defined for this system
			$thefields=unserialize($header->customfields); // the fields filled by customers also contain standard fields
			foreach ($fieldslist as $key=>$customfield) {
				$emailcontent=str_replace("#".$customfield->name."#", $thefields[$customfield->name], $emailcontent); // replace custom tags with the field names
			}
			$emailcontent=str_replace("#orderheading#",$hhtml, $emailcontent); // replace the headertag with header html
			$emailcontent=str_replace("#orderdetails#",$dhtml, $emailcontent); // replace detail tag with detail html
			$emailcontent=str_replace("#orderid#",$orderid, $emailcontent); // replace orderid tag with the order ID
			$emailcontent=str_replace("#ordercode#",$header->ordercode, $emailcontent); // replace orderid tag with the order ID
			$emailcontent=str_replace("#orderdt#",date("$dateformat $timeformat", $header->orderdt), $emailcontent); // replace orderid tag with the order ID
			$emailbody=$emailcontent;
		}
		else
		{
			$emailbody=$hhtml.$dhtml; // simply add one after the other without processing anything else
		}

		// all #variables# from the body also work for the subject

        //unserialize "email_subject" to array
        $emailsubjects = unserialize($cfg->get("email_subject"));
        //get email_subject for language by tag
        $emailsubject=$emailsubjects[$lang->getTag()];

		$fields=new fields();
		$fieldslist=$fields->getPublishedFields() ;// the custom fields defined for this system
		$thefields=unserialize($header->customfields); // the fields filled by customers, contain all fields
		foreach ($fieldslist as $key=>$customfield) {
			$emailsubject=str_replace("#".$customfield->name."#", $thefields[$customfield->name], $emailsubject); // replace custom tags with the field names
		}

		$emailsubject=str_replace("#name#", $header->name, $emailsubject);
		$emailsubject=str_replace("#ordertotal#", number_format($header->gtotal, $decs, $dsep, $tsep), $emailsubject);



        //unserialize "email_from" to array
        $emailFromAddresses = unserialize($cfg->get("email_from"));
        //get email_from for language by tag
        $emailFromAddress=$emailFromAddresses[$lang->getTag()];

        //unserialize "email_fromname" to array
            $emailFromNames = unserialize($cfg->get("email_fromname"));
		//get email_fromname for language by tag
		$emailFromName=$emailFromNames[$lang->getTag()];

		// standard Joomla mail engine
		$mailer = JFactory::getMailer();
		$mailer->setSender(array($emailFromAddress, $emailFromName));
		$mailer->setSubject(stripslashes($emailsubject));
		$mailer->setBody($emailbody );
		$mailer->IsHTML($mode);
		$mailer->addRecipient(trim($header->email));
		$mailer->Send();


		// standard Joomla mail engine
		$mailer2 = JFactory::getMailer();
		$mailer2->setSender(array($emailFromAddress, $emailFromName));
		$mailer2->setSubject(stripslashes($emailsubject));
		$mailer2->setBody($emailbody );
		$mailer2->IsHTML($mode);

		$emailcopies=$cfg->get('email_copies'); // the complete address list is already trimmed
		$aemailcopies=explode("\n", $emailcopies);

		foreach ($aemailcopies as $key=>$emailaddress) {
            $langMail = explode("=", $emailaddress);
            if ( count($langMail) > 1){
                if ($langMail[0] == $lang->getTag())
                    $mailer2->addRecipient(trim($langMail[1]));
            }else
                $mailer2->addRecipient(trim($emailaddress)); // trim each address from any \n ...
		}
		$mailer2->Send();

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $fields = array(
           $db->quoteName('emailsent') . ' = 1'
        );
        $conditions = array(
             $db->quoteName('id') . ' = ' . $orderid
        );
        $query->update($db->quoteName('#__sc_orders'))->set($fields)->where($conditions);
        $db->setQuery($query);
        $db->execute();

		return false;
	}
}
