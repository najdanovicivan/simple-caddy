<?php
/**
* @package SimpleCaddy 3.0 for Joomla 3.6+
* @copyright Copyright (C) 2006-2012 Henk von Pickartz. All rights reserved.
* SimpleCaddy frontend processing plant
* @license     GNU General Public License version 2 or later; see LICENSE.txt
*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if (version_compare( JVERSION, '3.6', '>=' ) == 0 ) {
	$mainframe->redirect("index.php", JText::_("SC_VERSION_TOO_LOW"));
}

define("debug", 0);

jimport('joomla.application.component.router');

// Load user_profile plugin language
$lang = JFactory::getLanguage();
$lang->load('com_simplecaddy', JPATH_SITE);

$mainframe=JFactory::getApplication();

require_once( JPATH_COMPONENT_SITE . '/simplecaddy.html.php' );
require_once( JPATH_COMPONENT_SITE . '/simplecaddy.class.php' );
require_once( JPATH_SITE . '/components/com_content/helpers/route.php');

$user = JFactory::getUser();
$input=$mainframe->input;

$Itemid = $input->get( 'Itemid' );
$data=$input->get("data");

$action=$input->get( 'action');
$task=$input->get( 'task');
$thiscid=$input->get("thiscid");
$nextcid=$input->get("nextcid");

//printf("<pre>%s</pre>", print_r($input, 1));
switch ($action) {
	case "view_prod":
		$name=$input->get("name", null, "string");
		$a=new products();
		$alist=$a->getPublishedProducts();
		$cfg=new sc_configuration();
		$scats=$cfg->get("prodcats");
		$catlist=explode("\r\n", $scats);
		$content=new sccontent();
		$clist=$content->getlist();
		display::view_prod($alist, $name, $catlist, $clist);
		break;
	default:
		if (class_exists($action)) {
			$c=new $action();
			if (!$task) $task="view";
			if (method_exists($action, $task)) {
				$c->$task();
				if ($c->redirect != "") $c->redirect();
			}
			else
			{
				if (debug) echo " task $task does not exist in class $action, trying plugin";
				$pl=new scplugins();
				$pluginname=$input->get("pluginname");
				if ($pluginname != "") {
					$pl->pluginfunction($pluginname, $task);
				}
				else{
					if (debug) echo " task $task is not a plugin task";
				}
			}
		}
		else{
			if (debug) echo " class $action does not exist";
		}
}

function AddStandard_example () {
	$cfg=new sc_configuration();
	$stdprod=$cfg->get("cart_fee_product");
	if ($stdprod != "") {
		$tmp=new products();
		$sp=$tmp->getproductByProdCode($stdprod);

		$cartProd=new CartProduct();
		$cartProd->option="";
		$cartProd->prodcode=$stdprod;
		$cartProd->prodname=$sp->shorttext . " 0%";
		$cartProd->unitprice=$sp->unitprice;
		$cartProd->quantity=1;
		$cartProd->finalprice=$sp->unitprice; // left on 0 for no discount for starters
		$cartProd->id=uniqid("S");
		$cart2=new cart2();
		$cart2->removecartProduct($cartProd);
		$c=$cart2->readcart();
		$qties=$cart2->getCartQuantities();
		$total=$cart2->getCartTotal();

		if ( ($qties>=2) and ($qties<5) ) {
			$cartProd->finalprice = -1 * ( $total * 0.05 );
			$cartProd->prodname=$sp->shorttext . " 5%";
		}
		if ( ($qties>=5) and ($qties<14) ) {
			$cartProd->finalprice = -1 * ( $total * 0.10 );
			$cartProd->prodname=$sp->shorttext . " 10%";
		}
		if ( ($qties>=14) ) {
			$cartProd->finalprice = -1 * ( $total * 0.20 );
			$cartProd->prodname=$sp->shorttext . " 20%";
		}

		if (count($c)>0) { // makes ure we have something in the cart
			$cart2->addCartProduct($cartProd);
		}
	}
}

function AddStandard() {
	$cfg=new sc_configuration();
	$stdprod=$cfg->get("cart_fee_product");
	if ($stdprod != "") {
		$tmp=new products();
		$sp=$tmp->getproductByProdCode($stdprod);
		$cartProd=new CartProduct();
		$cartProd->option="";
		$cartProd->prodcode=$stdprod;
		$cartProd->prodname=$sp->shorttext;
		$cartProd->unitprice=$sp->unitprice;
		$cartProd->quantity=1;
		$cartProd->finalprice=$sp->unitprice;
		$cartProd->id=uniqid("S");
		$cart2=new cart2();
		$cart2->removecartProduct($cartProd);
		$c=$cart2->readcart();
		if (count($c)>0) {
			$cart2->addCartProduct($cartProd);
		}
	}
}

function checkerrors () {
	$errors=0;
	// this is a very simple check, you can add any kind of checking method to refine and enhance your security
	// first start by getting the published fields
	$fields=new fields();
	$fieldlist=$fields->getPublishedFields();
	// now check if they are required, and if so, check if they are filled
	// default function is "checkfilled" see below!
	foreach ($fieldlist as $field) {
		if ($field->required == 1 ){ // required field
			// now get the required function, this is set in the DB for each field
			if (function_exists($field->checkfunction)) { //check if you defined this function
				$errors = $errors + call_user_func($field->checkfunction, $field);
			}
		}
	}
	return $errors;
}

function matheval($equation){
    $equation = preg_replace("/[^0-9+\-.*\/()%]/","",$equation);
    // fix percentage calcul when percentage value < 10
    $equation = preg_replace("/([+-])([0-9]{1})(%)/","*(1\$1.0\$2)",$equation);
    // calc percentage
    $equation = preg_replace("/([+-])([0-9]+)(%)/","*(1\$1.\$2)",$equation);
    // you could use str_replace on this next line
    // if you really, really want to fine-tune this equation
    $equation = preg_replace("/([0-9]+)(%)/",".\$1",$equation);
    if ( $equation == "" )
    {
      $return = 0;
    }
    else
    {
      eval("\$return=" . $equation . ";" );
    }
    return $return;
}


// the basic function for checking if a field has been filled
function checkfilled($field) {
	$mainframe = JFactory::getApplication();
	$input     = $mainframe->input;

    if (trim($input->get($field->name, '', 'RAW'))=="") { // trim the field and compare
		echo "<div class='errormsg'>".JText::_('SC_REQUIRED_FIELD')." <b>$field->caption</b> ".JText::_('SC_IS_EMPTY')."</div>";
		return 1; // add one to the errors total
	}
	else
	{
		return 0; // adds nothing to the errors total
	}
}


?>