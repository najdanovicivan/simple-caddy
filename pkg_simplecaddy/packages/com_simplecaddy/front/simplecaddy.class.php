<?php
/**
* @package SimpleCaddy 2.0 for Joomla 3.3+
* @copyright Copyright (C) 2006-2012 Henk von Pickartz. All rights reserved.
* General class file
*/
// ensure this file is being included by a parent file
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

foreach (glob(JPATH_SITE."/components/com_simplecaddy/classes/*.php") as $filename)
{
	require_once ($filename);
}

class about {
	function show() {
		display::ShowAbout();
	}
}


?>
