<?php
// No direct access.
defined('_JEXEC') or die;

	JToolBarHelper::title( JText::_( 'SIMPLECADDY_ORDERS' ));
	JToolBarHelper::deleteList();
	JToolBarHelper::editList();
	JToolBarHelper::custom( 'export', 'archive', 'archive' , 'Export', true );
	JToolBarHelper::custom( 'exportall', 'archive', 'archive' , 'Export all', false );
	JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false );

	$cfg=new sc_configuration();
	$currency=$cfg->get("currency");
	$tsep=$cfg->get("thousand_sep");
	$decsep=$cfg->get("decimal_sep");
	$decs=$cfg->get("decimals");
	$datefmt=$cfg->get("dateformat");
	$timefmt=$cfg->get("timeformat");
	$pageNav=$alist['nav'];
	$rows=$alist['lst'];
	$lists=$alist['lists'];
	$language= $lists['language'];


    $db = JFactory::getDbo();
    $query="SELECT sef, title_native, lang_code FROM #__languages ORDER BY sef ASC";
    $db->setQuery($query);
    $languages = $db->loadObjectList();
	?>
	<form method="post" name="adminForm" id="adminForm">

        <div>
            <label for="search"><?php echo JText::_( 'SC_FILTER' ); ?></label>
            <input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
            <button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
            <button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
        </div>
        <div>
            <label for="language"><?php echo JText::_( 'SC_LANGUAGE' ); ?>:</label>
            <select name="language" onchange="document.adminForm.submit();">
                <?php
                echo "<option value='*' ".(strlen($language)<1?" selected":"").">All</option>";
                 foreach ($languages as $l){
                    echo "<option value='$l->lang_code' ".($l->lang_code==$language?" selected":"").">$l->title_native</option>";
                }?>
            </select>
        </div>

		<table class="table table-hover">
			<thead>
			<tr>
				<th width="20"><?php echo JHTML::_('grid.checkall'); ?></th>
				<th class="title" nowrap="nowrap"><?php echo JHTML::_('grid.sort', JText::_("ID"), 'id' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th class="title" nowrap="nowrap"><?php echo JHTML::_('grid.sort', JText::_("SC_NAME"), 'name' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th class="title" nowrap="nowrap"><?php echo JHTML::_('grid.sort', JText::_("SC_EMAIL"), 'email' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th class="title tdright" nowrap="nowrap"><?php echo JHTML::_('grid.sort', JText::_("SC_DATE"), 'orderdt' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th class="title tdright" nowrap="nowrap"><?php echo JHTML::_('grid.sort', JText::_("SC_TOTAL"), 'total' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th class="title tdcenter" nowrap="nowrap"><?php echo JHTML::_('grid.sort', JText::_("SC_ORDER_STATUS"), 'status' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
                <th class="title tdcenter" nowrap="nowrap"><?php echo JHTML::_('grid.sort', JText::_("SC_LANGUAGE"), 'status' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th class="title" nowrap="nowrap">&nbsp;</th>


			</tr>
			</thead>
			<?php
			$k = 0;
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td width="20">
					<?php echo JHtml::_('grid.id', $i, $row->id); ?>
				</td>
				<td width="20">
					<a href="index.php?option=com_simplecaddy&action=orders&task=edit&cid[0]=<?php echo $row->id; ?>&field=&order=&search=">
						<?php echo $row->id; ?>
					</a>
				</td>
				<td width="10%">
					<a href="index.php?option=com_simplecaddy&action=orders&task=edit&cid[0]=<?php echo $row->id; ?>&field=&order=&search=">
						<?php echo $row->name; ?>
					</a>
				</td>
				<td width="10%">
					<?php echo "<a href='mailto:$row->email'>$row->email</a>"; ?>
				</td>
				<td width="10%" class="tdright">
					<?php
					echo date("$datefmt $timefmt", $row->orderdt); ?>
				</td>
				<td width="10%" class="tdright">
					<?php
					echo number_format($row->total + $row->tax, $decs, $decsep, $tsep);
					?>
				</td>
				<td>
					<?php echo "<span class='".strtolower($row->status)."'>$row->status</span>"; ?>
				</td>
                <td>
                    <?php echo JLayoutHelper::render('joomla.content.language', $row); ?>
                </td>
				<td>
					&nbsp;
				</td>
				<?php
				$k = 1 - $k; }

				?>
			</tr>
		</table>
		<div>
			<span><?php echo JText::_("SC_NUM_OF_ORDERS"). $alist["total"];?></span>
			<span><?php echo $pageNav->getListFooter();?></span>
			<span><?php echo JText::_("SC_LIMIT"). $pageNav->getLimitBox();?></span>
		</div>
		<input type="hidden" name="option" value="com_simplecaddy" />
		<input type="hidden" name="action" value="orders" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="filter_order" value="" />
		<input type="hidden" name="filter_order_Dir" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
	</form>
