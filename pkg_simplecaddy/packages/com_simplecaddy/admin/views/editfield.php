<?php
// No direct access.
defined('_JEXEC') or die;

?>
<form method="post" name="adminForm" id="adminForm">
	<table class="table borderless">
		<tbody>
		<tr>
			<td style="width:300px;">
				<label for="name" title="<?php echo JText::_("SC_HELP_FIELDNAME");?>"><?php echo JText::_('SC_FIELDNAME');?></label>
			</td>
			<td>
				<input type="text" name="name" id="name" value="<?php echo $this->name; ?>" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="caption" title="<?php echo JText::_("SC_HELP_CAPTION");?>"><?php echo JText::_('SC_FIELDCAPTION');?></label>
			</td>
			<td>
				<input type="text" name="caption" id="caption" value="<?php echo $this->caption; ?>"/>
			</td>
		</tr>
		<tr>
			<td>
				<label for="type" title="<?php echo JText::_("SC_HELP_TYPE");?>"><?php echo JText::_('SC_FIELDTYPE');?></label>
			</td>
			<td>
				<select name="type" id="type">
					<option value="text" <?php echo $this->type == "text" ?"selected" : ""; ?>><?php echo JText::_('SC_FIELDTYPE_TEXT');?></option>
					<option value="textarea" <?php echo $this->type == "textarea" ? "selected" : ""; ?>><?php echo JText::_('SC_FIELDTYPE_AREA');?></option>
					<option value="radio" <?php echo $this->type == "radio" ? "selected" : ""; ?>><?php echo JText::_('SC_FIELDTYPE_YESNO');?></option>
					<option value="checkbox" <?php echo $this->type == "checkbox" ? "selected" : ""; ?>><?php echo JText::_('SC_FIELDTYPE_CHECKBOX');?></option>
					<option value="date" <?php echo $this->type == "date" ? "selected" : ""; ?>><?php echo JText::_('SC_FIELDTYPE_DATE');?></option>
					<option value="dropdown" <?php echo $this->type == "dropdown" ? "selected" : ""; ?>><?php echo JText::_('SC_FIELDTYPE_DROPDOWN');?></option>
					<option value="divider" <?php echo $this->type == "divider" ? "selected" : ""; ?>><?php echo JText::_('SC_FIELDTYPE_DIVIDER');?></option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<label for="fieldcontents" title="<?php echo JText::_("SC_HELP_FIELDCONTENTS");?>"><?php echo JText::_('SC_FIELDCONTENTS');?></label>
			</td>
			<td>
				<input type="text" name="fieldcontents" id="fieldcontents" value="<?php echo $this->fieldcontents; ?>"/>
			</td>
		</tr>
		<tr>
			<td>
				<label for="length" title="<?php echo JText::_("SC_HELP_LENGTH");?>"><?php echo JText::_('SC_FIELDLENGTH');?></label>
			</td>
			<td>
				<input type="text" name="length" id="length" value="<?php echo $this->length; ?>"/>
			</td>
		</tr>
		<tr>
			<td>
				<label for="published" title="<?php echo JText::_("SC_HELP_PUBLISHED");?>"><?php echo JText::_('SC_PUBLISHED');?></label>
			</td>
			<td class="control-group">
				<div class="controls" id="published">
					<fieldset class="radio btn-group">
						<label for="0-published"><input type="radio" id="0-published" value="0" name="published" <?php echo ($this->published==0?' checked="checked"':'');?>><?php echo JText::_('JNO');?></label>
						<label for="1-published"><input type="radio" id="1-published" value="1" name="published" <?php echo ($this->published==1?' checked="checked"':'');?> ><?php echo JText::_('JYES');?></label>
					</fieldset>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<label for="classname" title="<?php echo JText::_("SC_HELP_CLASSNAME");?>"><?php echo stripslashes( JText::_('SC_FIELDCLASS'));?></label>
			</td>
			<td>
				<input type="text" name="classname" id="classname" value="<?php echo $this->classname; ?>" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="ordering" title="<?php echo JText::_("SC_HELP_ORDER");?>"><?php echo stripslashes( JText::_('SC_FIELDORDERING'));?></label>
			</td>
			<td>
				<input type="text" name="ordering" id="ordering" value="<?php echo $this->ordering; ?>" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="required" title="<?php echo JText::_("SC_HELP_REQUIRED");?>"><?php echo JText::_('SC_FIELDREQUIRED');?></label>
			</td>
			<td class="control-group">
				<div class="controls" id="required">
					<fieldset class="radio btn-group">
						<label for="0-required"><input type="radio" id="0-required" value="0" name="required" <?php echo ($this->required==0?' checked="checked"':'');?>><?php echo JText::_('JNO');?></label>
						<label for="1-required"><input type="radio" id="1-required" value="1" name="required" <?php echo ($this->required==1?' checked="checked"':'');?> ><?php echo JText::_('JYES');?></label>
					</fieldset>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<label for="checkfunction" title="<?php echo JText::_("SC_HELP_CHECKFUNCTION");?>"><?php echo stripslashes( JText::_('SC_FIELD_CHECKFUNCTION'));?></label>
			</td>
			<td>
				<input type="text" name="checkfunction" id="checkfunction" value="<?php echo $this->checkfunction; ?>" />
			</td>
		</tr>
		</tbody>
	</table>
	<input type="hidden" name="id" value="<?php echo $this->id; ?>" />
	<input type="hidden" name="option" value="com_simplecaddy" />
	<input type="hidden" name="action" value="fields" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="hidemainmenu" value="0" />
</form>
