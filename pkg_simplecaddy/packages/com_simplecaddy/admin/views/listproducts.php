<?php
// No direct access.
defined('_JEXEC') or die;
$arows=$alist;

	$mainframe=JFactory::getApplication();
	$input=$mainframe->input;
	display::header();
	JToolBarHelper::title( JText::_( 'SIMPLECADDY_PRODUCTS' ));
	JToolBarHelper::addNew();
	JToolBarHelper::custom( 'duplicate', 'copy.png', 'copy_f2.png', 'Duplicate', true );
	JToolBarHelper::publishList();
	JToolBarHelper::unpublishList();
	JToolBarHelper::deleteList();
	JToolBarHelper::editList();
	JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false );
	$cfg=new sc_configuration();
	$currency=$cfg->get("currency");
	$tsep=$cfg->get("thousand_sep");
	$decsep=$cfg->get("decimal_sep");
	$decs=$cfg->get("decimals");
	$pageNav=$arows['nav'];
	$rows=$arows['lst'];
	$lists=$arows['lists'];
	$search=$input->get("search");
	?>
	<form method="post" name="adminForm" action="index.php" id="adminForm">
		<div>
			<?php echo JText::_( "SC_FILTER" ); ?>:
			<?php echo $lists['category'];?>
		</div>
		<table class="table table-hover table-striped">
			<thead>
			<tr>
				<th style="width:20px;"><?php echo JHTML::_('grid.checkall'); ?></th>
				<th style="width:70px;" class="title"><?php echo JHTML::_('grid.sort', JText::_("SC_CODE"), 'prodcode' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th class="title"><?php echo JHTML::_('grid.sort', JText::_("SC_DESCRIPTION"), 'shorttext' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th style="width:80px;" class="title"><?php echo JHTML::_('grid.sort', JText::_("SC_PRODUCT_CATEGORY"), 'category' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th style="width:80px;" class="title tdright"><?php echo JHTML::_('grid.sort', JText::_("SC_PRICE_PER_UNIT"), 'unitprice' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th style="width:50px;" class="title tdright"><?php echo JHTML::_('grid.sort', JText::_("SC_NUM_IN_STORE"), 'av_qty' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th style="width:40px;" class="title tdcenter"><?php echo JHTML::_('grid.sort', JText::_("SC_PUBLISHED"), 'published' , @$lists['order_Dir'], @$lists['order']  ); ?></th>
				<th class="title">&nbsp;</th>

			</tr>
			</thead>
			<tbody>
			<?php
			$k = 0;
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
					<?php echo JHtml::_('grid.id', $i, $row->id); ?>
				</td>
				<td>
					<a href="#edit" onclick="return listItemTask('cb<?php echo $i;?>','edit')">
						<?php
						echo $row->prodcode; ?>
					</a>
				</td>
				<td>
					<?php echo $row->shorttext; ?>
				</td>
				<td>
					<?php echo $row->category; ?>
				</td>
				<td class="tdright">
					<?php
					echo number_format($row->unitprice, $decs, $decsep, $tsep);
					?>
				</td>
				<td class="tdright">
					<?php echo $row->av_qty; ?>
				</td>
				<td class="tdcenter">
					<?php
					$published 	= JHTML::_('jgrid.published', $row->published, $i );
					echo $published;
					?>
				</td>
				<td>
					&nbsp;
				</td>
				<?php
					$k = 1 - $k; }
				?>
			</tr>
			</tbody>
		</table>
		<div>
			<span><?php echo JText::_("SC_NUM_OF_PRODUCTS"). $alist["total"];?></span>
			<span><?php echo $pageNav->getListFooter();?></span>
			<span><?php echo JText::_("SC_LIMIT"). $pageNav->getLimitBox();?></span>
		</div>
		<input type="hidden" name="option" value="com_simplecaddy" />
		<input type="hidden" name="action" value="products" />
		<input type="hidden" name="task" id="task" value="" />
		<input type="hidden" name="boxchecked" value="" />
		<input type="hidden" name="filter_order" value="" />
		<input type="hidden" name="filter_order_Dir" value="" />
		<input type="hidden" name="hidemainmenu" value="0" />
	</form>
