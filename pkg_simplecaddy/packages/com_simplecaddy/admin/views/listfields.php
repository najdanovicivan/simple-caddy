<?php
// No direct access.
defined('_JEXEC') or die;

	display::header();
	JToolBarHelper::title( JText::_( 'SIMPLECADDY_CHECKOUT_FIELDS' ));
	JToolBarHelper::publishList();
	JToolBarHelper::unpublishList();
	JToolBarHelper::deleteList();
	JToolBarHelper::editList();
	JToolBarHelper::addNew();
	JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false );
	$pageNav=$arows['nav'];
	$rows=$arows['lst'];
	?>
	<form method="post" name="adminForm" action="index.php" id="adminForm">
		<table class="table table-hover">
			<tr>
				<th style="width:20px;">
					<?php echo JHTML::_('grid.checkall'); ?>
				</th>
				<th class="title" style="width: 150px;"><?php echo JText::_('SC_FIELDNAME') ?></th>
				<th class="title" style="width: 180px;"><?php echo JText::_('SC_FIELDCAPTION') ?></th>
				<th class="title tdright" style="width: 90px;"><?php echo JText::_('SC_FIELDTYPE') ?></th>
				<th class="title tdright" style="width: 50px;"><?php echo JText::_('SC_FIELDLENGTH') ?></th>
				<th class="title tdcenter" style="width: 150px;"><?php echo JText::_('SC_FIELDCLASS') ?></th>
				<th class="title tdcenter" style="width: 40px"><?php echo JText::_('SC_FIELDORDERING') ?></th>
				<th class="title tdcenter" style="width: 40px"><?php echo JText::_('SC_FIELDREQUIRED') ?></th>
				<th class="title"><?php echo JText::_('SC_FIELDPUBLISHED') ?></th>

			</tr>
			<?php
			$k = 0;
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
			$row = &$rows[$i];
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
					<?php echo JHtml::_('grid.id', $i, $row->id); ?>
				</td>
				<td>
					<a href="#edit" onclick="return listItemTask('cb<?php echo $i;?>','edit')">
						<?php
						echo $row->name; ?>
					</a>
				</td>
				<td>
					<?php echo $row->caption; ?>
				</td>
				<td class="tdright">
					<?php
					echo  JText::_($row->type);
					?>
				</td>
				<td class="tdright">
					<?php echo $row->length; ?>
				</td>
				<td>
					<?php echo $row->classname; ?>
				</td>
				<td class="tdright">
					<?php
					echo $row->ordering;
					?>
				</td>
				<td class="tdright">
					<?php
					echo ($row->required?JText::_('SC_YES'):JText::_('SC_NO'));
					?>
				</td>
				<td class="tdcenter">
					<?php
					$published 	= JHTML::_('jgrid.published', $row->published, $i );
					echo $published;
					?>
				</td>
				<td>
					&nbsp;
				</td>
				<?php
				$k = 1 - $k; }

				?>
			</tr>
		</table>
		<div>
			<?php
				echo $pageNav->getListFooter();
			?>
		</div>
		<input type="hidden" name="option" value="com_simplecaddy" />
		<input type="hidden" name="action" value="fields" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
	</form>
