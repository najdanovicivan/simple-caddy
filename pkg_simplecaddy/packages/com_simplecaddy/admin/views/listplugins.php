<?php
// No direct access.
defined('_JEXEC') or die;

	display::header();
	JToolBarHelper::title( JText::_( "SC_SIMPLECADDY_PLUGINS" ), 'generic.png');
	JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false );
	?>
	<form name="adminForm" id="adminForm">
		<table class="table">
			<thead>
			<tr>
				<th style='width:300px;'><?php echo JText::_("SC_PLUGIN_NAME");?></th>
				<th><?php echo JText::_("SC_ENABLED");?></th>
			</tr>
			</thead>
			<tbody>
		<?php
		foreach ($lst as $row) {
			?>
			<tr>
				<td>
					<a href="index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=<?php echo $row->element;?>"><?php echo $row->name;?></a>
				</td>
			<td>
				<?php $toenable=1-$row->enabled; ?>
			<a href='index.php?option=com_simplecaddy&action=scplugins&task=enableplugin&id=<?php echo $row->extension_id;?>&enable=<?php echo $toenable;?>'>
			<img class='plugin_enabled<?php echo $row->enabled;?>' border='0' />
			</a>
			</td>
			</tr>
		<?php
		}
		?>
			</tbody>
		</table>
		<input type="hidden" name="option" value="com_simplecaddy" />
		<input type="hidden" name="action" value="" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
	</form>
