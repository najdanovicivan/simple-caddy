<?php
// No direct access.
defined('_JEXEC') or die;

	$mainframe=JFactory::getApplication();
	$input=$mainframe->input;
	$document	= JFactory::getDocument();
	$document->addScript( JURI::root(true).'/administrator/components/com_simplecaddy/js/caddy.js');
		$cfg=new sc_configuration();
	$currency=$cfg->get("currency");
	$tsep=$cfg->get("thousand_sep");
	$decsep=$cfg->get("decimal_sep");
	$decs=$cfg->get("decimals");
	$scats=$cfg->get("prodcats");
	$curalign=$cfg->get("curralign");
	$cats=explode("\r\n", $scats);
	$optiongroups=new optiongroups();
	$lstoptgroups=$optiongroups->getgroups($product->id);
	?>
	<form method="post" name="adminForm" action="index.php" id="adminForm">
		<table class="table borderless">
			<tr>
				<th>
					<?php echo ($product->id ? JText::_('SC_EDIT') : JText::_('SC_NEW'))."&nbsp;".JText::_('SC_PRODUCT');?>
				</th>
				<th>&nbsp;</th>
			</tr>
			<tr>
				<td>
					<label for="prodcode">
						<?php echo JText::_('SC_PRODUCT_CODE');?>
					</label>
				</td>
				<td>
					<input type="text" id="prodcode" name="prodcode" value="<?php echo ($input->get("dup")==1?"":$product->prodcode); ?>" />
				</td>
			</tr>
			<tr>
				<td>
					<label for="shorttext"><?php echo JText::_('SC_PRODUCT_NAME');?>
					</label>
				</td>
				<td>
					<input type="text" id="shorttext" name="shorttext" value="<?php echo $product->shorttext; ?>" size="60"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="category">
						<?php echo JText::_('SC_PRODUCT_CATEGORY');?>
					</label>
				</td>
				<td>
					<select id="category" name="category">
						<?php
						foreach ($cats as $cat) {
							echo "<option value='$cat' ".($cat==$product->category ? ' selected' : '').">$cat</option>";
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label for="av_qty">
						<?php echo JText::_('SC_AVAILABLE_QTY');?>
					</label>
				</td>
				<td>
					<input type="text" id="av_qty" name="av_qty" value="<?php echo $product->av_qty; ?>" size="6"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="unitprice"> <?php echo JText::_('SC_PRICE_PER_UNIT');?></label>
				</td>
				<td>
					<?php
					if ($curalign==1) echo $currency;
					?>
					<input size="10" type="text" id="unitprice" name="unitprice" value="<?php echo $product->unitprice; ?>"/>
					<?php
					if ($curalign==0) echo $currency;
					echo JText::_('SC_DO_NOT_FORMAT_YOUR_PRICE');
					?>
				</td>
			</tr>
			<?php if ($cfg->get("show_shipping_fields")==1) { ?>
				<tr>
					<td>
						<label for="shippoints"> <?php echo JText::_('SC_SHIPPING_POINTS');?>
						</label>
					</td>
					<td>
						<input type="text" id="shippoints" name="shippoints" value="<?php echo $product->shippoints; ?>" size="5"/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="shiplength">
							<?php echo JText::_('SC_SHIPPING_LENGTH');?>
						</label>
					</td>
					<td>
						<input type="text" id="shiplength" name="shiplength" value="<?php echo $product->shiplength; ?>" size="5"/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="shipwidth">
							<?php echo JText::_('SC_SHIPPING_WIDTH');?>
						</label>
					</td>
					<td>
						<input type="text" id="shipwidth" name="shipwidth" value="<?php echo $product->shipwidth; ?>" size="5"/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="shipheight">
							<?php echo JText::_('SC_SHIPPING_HEIGHT');?>
						</label>
					</td>
					<td>
						<input type="text" id="shipheight" name="shipheight" value="<?php echo $product->shipheight; ?>" size="5"/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="shipweight">
							<?php echo JText::_('SC_SHIPPING_WEIGHT');?>
						</label>
					</td>
					<td>
						<input type="text" id="shipweight" name="shipweight" value="<?php echo $product->shipweight; ?>" size="5"/>
					</td>
				</tr>
			<?php } ?>
			<?php if ($cfg->get("use_downloadables")==1) { ?>
				<tr>
					<td><?php echo JText::_('SC_DOWNLOADABLE');?></td>
					<td class="control-group">
						<div class="controls" id="downloadable">
							<fieldset class="radio btn-group">
								<label for="0-downloadable"><input type="radio" id="0-downloadable" value="0" name="downloadable" <?php echo ($product->downloadable==0?' checked="checked"':'');?>><?php echo JText::_('JNO');?></label>
								<label for="1-downloadable"><input type="radio" id="1-downloadable" value="1" name="downloadable" <?php echo ($product->downloadable==1?' checked="checked"':'');?> ><?php echo JText::_('JYES');?></label>
							</fieldset>
						</div>
					</td>

				</tr>
				<tr>
					<td>
						<label for="filename">
							<?php echo JText::_('SC_FILENAME');?>
						</label>
					</td>
					<td>
						<input size="20" type="text" id="filename" name="filename" value="<?php echo $product->filename; ?>"/>
					</td>
				</tr>
			<?php } ?>
			<tr>
				<td>
					<?php echo JText::_('SC_PUBLISHED');?>
				</td>
				<td class="control-group">
					<div class="controls" id="published">
						<fieldset class="radio btn-group">
							<label for="0-published"><input type="radio" id="0-published" value="0" name="published" <?php echo ($product->published==0?' checked="checked"':'');?>><?php echo JText::_('JNO');?></label>
							<label for="1-published"><input type="radio" id="1-published" value="1" name="published" <?php echo ($product->published==1?' checked="checked"':'');?> ><?php echo JText::_('JYES');?></label>
						</fieldset>
					</div>
				</td>
			</tr>
		</table>
		<?php // check if the product exists before adding options
		if ($product->id) { ?>
			<table class="table table-condensed" ><tr><th><?php echo JText::_('SC_OPTIONS');?>&nbsp;<input type="button" name="addbtn" onclick="submitbutton('addoptgroup')" value="<?php echo JText::_('Add Option');?>" /></th><th width="270"><?php echo JText::_('SC_SHOW_AS');?></th><th width="60"><?php echo JText::_('SC_ORDER');?></th><th width="120"><?php echo stripslashes( JText::_('SC_IND_OPTIONS'));?></th><th>&nbsp;</th></tr>
				<?php
				$showas=new optionsshowas();

				foreach ($lstoptgroups as $optgroup) {
					echo "\n<tr>";
					echo "<td><a rel=\"{handler: 'iframe', size: {x: 900, y: 550}, onClose: function() {}}\" class='modal' href='index.php?option=com_simplecaddy&action=optiongroups&task=show&optgrid=$optgroup->id&productid=$product->id&tmpl=component'>$optgroup->title</a></td>";
					echo "<td>";
					echo $showas->type[$optgroup->showas];
					echo "</td>";
					echo "<td>$optgroup->disporder</td>";
					if (($optgroup->showas !=5) and ($optgroup->showas !=6)) { // exclude the ones without options
						echo "<td><a rel=\"{handler: 'iframe', size: {x: 875, y: 550}, onClose: function() {}}\" class='modal' href='index.php?option=com_simplecaddy&action=options&task=showindoptions&optgrid=$optgroup->id&tmpl=component&productid=$product->id'>".JText::_('SC_IND_OPTIONS')."</a></td>";
					}
					else
					{ // no options? just display an empty cell for table alignment
						echo "<td>&nbsp;</td>";
					}
					echo "<td><a class='button' href='index.php?option=com_simplecaddy&action=optiongroups&task=remove&optgrid=$optgroup->id&productid=$product->id'>".JText::_('Remove option')."</td>";
					echo "</tr>";
				}
				?>
			</table>
			<?php
		}
		else
		{
			echo JText::_("SC_SAVE_FIRST");
		}
		?>

		<input type="hidden" name="id" value="<?php echo $product->id; ?>" />
		<input type="hidden" name="option" value="com_simplecaddy" />
		<input type="hidden" name="action" value="products" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
	</form>
