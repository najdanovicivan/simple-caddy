<?php
// No direct access.
defined('_JEXEC') or die;

$cfg=new sc_configuration();
$currency=$cfg->get("currency");
$tsep=$cfg->get("thousand_sep");
$decsep=$cfg->get("decimal_sep");
$decs=$cfg->get("decimals");
$align=$cfg->get("curralign"); // before amount==1
$standardfields=array();
$statuses=explode("\n", $cfg->get("ostatus"));
$useshipping=$cfg->get("shippingenabled");


?>
<form method="post" name="adminForm" id="adminForm">
	<div style="vertical-align: top;">
		<div class="orderblock">
			<table class="table table-hover borderless">
				<thead>
				<tr>
					<th class="title" style="width: 250px;">
						<?php echo JText::_('SC_ORDER');?></th><th><?php echo $scorder->id;?>
					</th>
				</tr>
				</thead>
				<tr>
					<td><?php echo JText::_('SC_DATE');?></td>
					<td>
						<?php echo date("d-m-Y H:i:s", $scorder->orderdt); ?>
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_USERID');?></td>
					<td><?php echo $scorder->j_user_id; ?></td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_IP_ADDRESS');?></td>
					<td>
						<?php
						$iplink = '&nbsp;<a href="http://whois.domaintools.com/'.$scorder->ipaddress.'" target="_blank" class="scbutton">'.JText::_("SC_CHECKIP")."</a>";
						echo $scorder->ipaddress;
						echo $iplink;
						?>
						<input type="hidden" name="ipaddress" value="<?php echo $scorder->ipaddress;?>" />
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_SHIP_REGION');?></td>
					<td>
						<?php
						echo $scorder->shipRegion; ?>
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_SHIPPER');?></td>
					<td>
						<?php
						echo $scorder->shipper; ?>
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_SHIP_COST');?></td>
					<td>
						<?php
						if ($align==1) echo $currency. "&nbsp;";
						echo number_format($scorder->shipCost, $decs, $decsep, $tsep);
						if ($align==0) echo "&nbsp;". $currency;
						?>
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_SUBTOTAL');?></td>
					<td>
						<?php
						if ($align==1) echo $currency. "&nbsp;";
						echo number_format($scorder->total, $decs, $decsep, $tsep);
						if ($align==0) echo "&nbsp;". $currency;
						?>
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_TAX');?></td>
					<td>
						<?php
						if ($align==1) echo $currency. "&nbsp;";
						echo number_format($scorder->tax, $decs, $decsep, $tsep);
						if ($align==0) echo "&nbsp;". $currency;
						?>
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_TOTAL');?></td>
					<td>
						<?php
						if ($align==1) echo $currency. "&nbsp;";

						echo number_format($scorder->total + $scorder->tax + $scorder->shipCost, $decs, $decsep, $tsep);
						if ($align==0) echo "&nbsp;". $currency;
						?>
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_PAYMENT_ID');?></td>
					<td>
						<?php
						echo $scorder->paymentcode;
						?>
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_ORDERCODE');?></td>
					<td>
						<?php
						echo $scorder->ordercode;
						?>
					</td>
				</tr>
			</table>
		</div>
		<div class="orderblock">
			<table class="table table-hover borderless">
				<?php
				if (@$scorder->customfields) {
					echo "<thead><tr><th>".JText::_('Custom fields')."</th><th>&nbsp;</th></tr></thead><tbody>";
					$fields=new fields();
					$fieldlist=$fields->getPublishedFieldsArray();
					$acfields=unserialize($scorder->customfields);
					foreach ($fieldlist as $key=>$cfield) {
						if (!in_array($cfield, $standardfields)) { // show only the fields that are not hardcoded
							$scfield=str_replace(array("[", "]"), "", $cfield);
							if (isset($acfields[$scfield])) {

								if (is_array($acfields[$scfield]))
								{
									echo "<tr>";
									echo "<td>$cfield</td>";
									echo "<td>==".@print_r($acfields[$scfield], 1)."==</td>";
									echo "</tr>";
								}
								else
								{
									echo "<tr>";
									echo "<td>$cfield</td>";
									echo "<td>".@$acfields[$cfield]." ";
									echo "</td></tr>";
								}
							}
						}
					}
				}
				?>
				<tr>
					<td><a href="index.php?option=com_simplecaddy&action=orders&task=email&oid=<?php echo $scorder->id?>" class="scbutton"><?php echo JText::_("SC_RESEND_ORDER_CONFIRMATION_EMAIL");?></a></td>
				</tr>
				<tr>
					<td><?php echo JText::_('SC_ORDER_STATUS');?></td>
					<td>
						<?php
						echo "<select name='edtostatus'>";
						foreach ($statuses as $status) {
							$selected=(strtolower($scorder->status)==strtolower(trim($status))?" selected":"");
							echo "<option value='".trim($status)."' $selected>$status</option>\n";

						}
						echo "</select>";
						?>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="detailblock">
		<div><?php echo JText::_('SC_DETAILS');?></div>
		<table class="table table-hover" >
			<thead>
			<tr>
				<th class="title" style="width:10%;"><?php echo JText::_('SC_CODE');?></th>
				<th class="title"><?php echo JText::_('SC_QUANTITY');?></th>
				<th class="title tdright"><?php echo JText::_('SC_PRICE_PER_UNIT');?></th>
				<th class="title tdright"><?php echo JText::_('SC_TOTAL');?></th>
				<th class="title"><?php echo JText::_('SC_PRODUCT_NAME');?></th>
				<th class="title"><?php echo JText::_('SC_PRODUCT_OPTION');?></th>
				<th class="title"><?php echo JText::_('SC_ACTION');?></th>
				<th class="title">&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$autodec=$cfg->get("autodecfromstore");
			$k = 0;
			for ($i=0, $n=count( $items ); $i < $n; $i++) {
			$row = &$items[$i];
			?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
					<?php echo $row->prodcode; ?>
				</td>
				<td width="30">
					<?php
					echo $row->qty;
					?>
				</td>
				<td width="10%" class="tdright">
					<?php
					echo number_format($row->unitprice, $decs, $decsep, $tsep);
					?>
				</td>
				<td class="tdright">
					<?php
					echo number_format($row->total, $decs, $decsep, $tsep);
					?>
				</td>
				<td width="40%">
					<?php echo $row->shorttext; ?>
				</td>
				<td>
					<?php echo $row->option; ?>&nbsp;
				</td>
				<td>
					<?php
					if ($autodec==0) echo "<a class=\"scbutton\" href=\"index.php?option=com_simplecaddy&action=products&task=decstore&pid=$row->prodcode&qty=$row->qty&order=$scorder->id\">".JText::_('SC_DECSTORE')."</a>";
					?>
				</td>
				<td>
					&nbsp;
				</td>
				<?php
				$k = 1 - $k; }
				?>
			</tr>
			</tbody>
		</table>
	</div>
	<?php
	$field=$input->get( 'field', '');
	$order=$input->get( 'order', '');
	?>
	<input type="hidden" name="id" value="<?php echo ($scorder->id?"$scorder->id":"-1"); ?>">
	<input type="hidden" name="option" value="com_simplecaddy" />
	<input type="hidden" name="action" value="orders" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="order" value="<?php echo $order; ?>" />
	<input type="hidden" name="field" value="<?php echo $field; ?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="hidemainmenu" value="0" />
</form>
