<?php
/**
* @package SimpleCaddy 2.0.5 for Joomla 3.3
* @copyright Copyright (C) 2006-2012 Henk von Pickartz. All rights reserved.
* Main display admin file
*/
// ensure this file is being included by a parent file
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

class display {
	static function showExport() {
		display::header();
		?>
		<form method="post" name="adminForm" action="index.php" id="adminForm">
			<input type="hidden" name="option" value="com_simplecaddy" />
			<input type="hidden" name="action" value="orders" />
			<input type="hidden" name="task" value="show" />
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="hidemainmenu" value="0" />
		</form>
		<?php
		echo "Your export can be downloaded : <a href='components/com_simplecaddy/exports/export.txt'>here</a>";
	}

	static function MainMenu($message="") {
		display::header();
		JToolBarHelper::title( JText::_( 'SIMPLECADDY_CONTROL_CENTER' ));
		$cfg=new sc_configuration();
		$modules=new scmodules();
		$mlist=$modules->getmodules();
		?>
			<div id="cpanel" class="cpanel" style="display: block!important;">

				<?php
				foreach ($mlist as $menuitem) {
				$mi=new $menuitem->modulename();
				$m=$mi->getmenuitem();
				if (!$m->active) continue;
	            ?>
				<div style="display:inline-block;">
					<div class="scicon">
						<a title="<?php echo $m->title; ?>" href="<?php echo $m->link;?>" >
							<img src="<?php echo $m->image;?>" alt="<?php echo $m->alt;?>" align="middle" name="image" border="0" /><br />
							<?php echo $m->text; ?>
						</a>
					</div>
				</div>

	            <?php
	            }
	            ?>
	            <div style="display:inline-block;">
					<div class="scicon">
						<a title="About" href="index.php?option=com_simplecaddy&amp;action=about&amp;task=show">
						<img src="components/com_simplecaddy/images/about.png" alt="<?php echo JText::_('SC_ABOUT'); ?>" align="middle" name="image" border="0" /><br />
						<?php echo JText::_('SC_ABOUT'); ?></a>
					</div>
				</div>

	        </div>
	<?php
	}

	static function AFFooter() {
		?>
		<div style="display: block;"></div>
		<div style="margin-top: 10px;">
			<div style="text-align: center">
				<?php echo JText::_('SC_FOR_MORE_INFORMATION_CLICK_HERE');?>
				<a href="http://atlanticintelligence.net" target="_blank"><?php echo JText::_('SC_INFORMATION');?></a>
				<br /><a href="http://demo3.atlanticintelligence.net" target="_blank"><?php echo JText::_('SC_CLICK_HERE_FOR_DEMO');?></a>
			</div>
		</div>
	<?php
	}

	static function showAbout() {
		global $mainframe;
		jimport('joomla.filesystem.path');
		display::header();
		JToolBarHelper::title( JText::_( 'SimpleCaddy' ));
		JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false );
	?>
		<form name="adminForm" id="adminForm">
			<input type="hidden" name="task" />
			<input type="hidden" name="option" value="com_simplecaddy" />
		</form>
		<div style="text-align: left">
		<h2><img src="components/com_simplecaddy/images/sc_logo15.png"  />SimpleCaddy 3.0 for Joomla 3.6+ </h2>
		<p>Adds basic shopping cart functionality to any page of Joomla content.</p>
		<p>Featuring</p>
		  <ul>
			  <li>Add to cart</li>
			  <li>Simple shop mechanism</li>
			  <li>Simple order management</li>
			  <li>Simple store management</li>
			  <li>Individual item options</li>
			  <li>Formulas to define prices of individual options</li>
			  <li>Plugins for added functionality</li>
		  </ul>
		  </div>
		  <p>SimpleCaddy (c)Henk von Pickartz, 2006-2017</p>
	<?php
	}

	static function header() {
		$document = JFactory::getDocument();
		$document->addStyleSheet("components/com_simplecaddy/css/simplecaddy.css",'text/css',"screen");
	}

    static function showoptgroup($optiongroup, $prodid) { // shows option group edit screen
        $showas=new optionsshowas();
        ?>
        <p><h3><?php echo JText::_("SC_OPTIONGROUP");?></h3></p>
       <form name="frmoptgroup" action="index.php" method="post" target="_parent" >
        <table border="0" width="100%">
        <tr><td>&nbsp;</td>
        <td align="right">
        <input type="button" onclick="document.frmoptgroup.submit();" value="<?php echo JText::_('SC_SAVE');?>" />
        </td>
        </tr>
        <tr>
        <td><?php echo JText::_('SC_GROUPTITLE');?></td>
        <td><input type="text" name="title" value="<?php echo $optiongroup->title; ?>" /></td>
        </tr>
        <tr>
        <td><?php echo JText::_('SC_DISPLAYORDER');?></td>
        <td><input name="disporder" size="1" value="<?php echo $optiongroup->disporder; ?>" type="text" /></td>
        </tr>
        <tr>
 		<td><?php echo JText::_('SC_SHOW_AS');?>
		</td>
		<td>
			<select name="showas">
            <?php
                foreach ($showas->type as $key=>$value) {
                    echo "\n<option value='$key' ".($optiongroup->showas==$key?" selected":"").">". $showas->type[$key] . " </option>";
                }
            ?>
			</select>
		</td>
        </tr>

        </table>
        <input type="hidden" name="productid" value="<?php echo $prodid;?>" />
        <input type="hidden" name="id" value="<?php echo $optiongroup->id;?>" />
        <input type="hidden" name="prodcode" value="<?php echo $optiongroup->prodcode;?>" />
        <input type="hidden" name="option" value="com_simplecaddy" />
        <input type="hidden" name="action" value="optiongroups" />
        <input type="hidden" name="task" value="saveoptiongroup" />
        </form>
        <?php
    }

    static function showindoptions(&$rows, $optgrid, $productid) { // shows individual options
		$document = JFactory::getDocument();
		$document->addScript( JURI::root(true).'/administrator/components/com_simplecaddy/js/caddy.js');
        $og=new optiongroups();
        $og->load($optgrid);
        ?>
        <form name="frmindoptions" method="post" action="index.php">
		<table style="width:800px;">
			<tr>
				<th colspan="2"><?php echo JText::_('SC_OPTIONS');?>&nbsp;<?php echo $og->title;?>
				</th>
			</tr>
			<tr>
				<td>
					<?php echo stripslashes( JText::_('SC_IND_OPTIONS'));?>
				</td>
			</tr>

        <tr>

        <td align="right">
		</td>

        </tr>
		<tr>

		<td>
			<input type="button" name="savebtn" value="<?php echo JText::_('SC_SAVE');?>" onclick="return document.frmindoptions.submit();return window.parent.SqueezeBox.close();" />
			&nbsp;
			<input type="button" name="closebtn" value="<?php echo JText::_('SC_CLOSE');?>" onclick="return window.parent.SqueezeBox.close();" />
			&nbsp;&nbsp;
			<input type="button" name="addbtn" onclick="addRow()" value="<?php echo JText::_('Add Option');?>" />
			&nbsp;
			<input type="button" name="delbtn" onclick="deleteRow()" value="<?php echo JText::_('Remove option');?>" />
			<table id="mine" style="width:780px;">
			<tr>
				<th style="width:30px;">#</th>
				<th><?php echo JText::_('Description') ?></th>
				<th><?php echo JText::_('Formula');?></th>
				<th><?php echo JText::_('Caption');?></th>
				<th><?php echo JText::_("Display order");?></th>
				<th><?php echo JText::_('Default select');?></th>
				<th>&nbsp;</th>
			</tr>
			<?php
				foreach ($rows as $key=>$line) {
					echo "<tr>";
					echo "<td><input type='checkbox' name='tid$key' id='tid$key' value='$line->id'></td>";
					echo "<td><input type='hidden' name='optionid[]' value='$line->id'>";
					echo "<input style='width:160px;' type='text' name='optionshorttext[]' value='$line->description' ></td>" ;
					echo "<td><input style='width:75px;' type='text' name='optionformula[]' value='$line->formula' ></td>" ;
					echo "<td><input style='width:160px;' type='text' name='optioncaption[]' value='$line->caption' ></td>" ;
					echo "<td><input style='width:30px;' type='text' name='optiondisporder[]' value='$line->disporder' size='1'></td>" ;
					echo "<td><input type='radio' name='optiondefselect' value='$key' ".($line->defselect=="1"?"checked":"")."></td>" ;
					echo "</tr>";
				}
			?>

			</table>
			<input type="hidden" name="rows" value="" id="rows" />
			<input type="hidden" name="rows2" value="" id="rows2" />
		</td>
		</tr>
		</table>
		<input type="hidden" name="option" value="com_simplecaddy" />
		<input type="hidden" name="optgrid" value="<?php echo $optgrid;?>" />
		<input type="hidden" name="productid" value="<?php echo $productid;?>" />
		<input type="hidden" name="action" value="options" />
		<input type="hidden" name="task" value="saveoptions" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0" />
		</form>
        <?php
    }


}