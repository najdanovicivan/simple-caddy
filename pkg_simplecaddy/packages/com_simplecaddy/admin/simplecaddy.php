<?php
/**
 * @package SimpleCaddy 2.0.5 for Joomla 3.3+
 * @copyright Copyright (C) 2006-2012 Henk von Pickartz. All rights reserved.
 * Main admin file
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$debug=0;

$mainframe = JFactory::getApplication();
$input = $mainframe->input;

require_once( JPATH_COMPONENT_ADMINISTRATOR. '/views/admin.simplecaddy.html.php' );
require_once( JPATH_COMPONENT_SITE.'/simplecaddy.class.php' );
//require_once( JPATH_COMPONENT.'/views/admin.simplecaddy.pg.html.php' );
require_once( JPATH_COMPONENT_ADMINISTRATOR. '/views/admin.simplecaddy.content.html.php' );

if (version_compare( JVERSION, '3.3', '>=' ) == 0 ) {
	$mainframe->enqueueMessage(JText::_("SC_VERSION_TOO_LOW"));
	$mainframe->redirect("index.php" );
}

$action=$input->getCmd('action');
$task=$input->getCmd( 'task' );
if ($task=="") $task="show";

$cid=$input->get( 'cid', array(0), 'array' );

if ($debug==1) {
	echo "action '$action' ";
	echo " -- task $task--";
}

if (($task=="control")) {

	display::MainMenu();
	if ($debug) echo "<p>Development debug info</p><strong>task= '$task'<br>action='$action'</strong>";
	if (!$input->getCmd('no_html')) {
		display::afFooter();
	}
	return;
}

switch ($action) {
	case "pluginconfig":
		$pl=new scplugins();
		if (method_exists($pl, $task)) {
			$pl->$task();
		}
		else {
			$pluginname=$input->get("pluginname");
			$pl->pluginfunction($pluginname, $task);
		}
		break;
	default:
		if(class_exists($action)) { // normal classes for SimpleCaddy component
			$class=new $action();
			if(method_exists($class, $task)) {
				$class->$task();
			}
			else {
				if ($debug) echo "DEBUG: method '$task' does not exist in class $action";
			}
		}
		else { // SimpleCaddy content plugins
			$path=JPATH_SITE."/plugins/content/$action/classes/$action.php";
			if (file_exists($path)) {
				require_once($path);
				$class=new $action();
				if(method_exists($class, $task)) {
					$class->$task();
				}
				else {
					if ($debug) echo "<p>method '$task' does not exist in class $action</p>";
				}
			}
			else {
				if ($debug) echo "<p>DEBUG: class/file '$path' does not exist</p>";
				display::MainMenu();
			}
		}
	}
?>
