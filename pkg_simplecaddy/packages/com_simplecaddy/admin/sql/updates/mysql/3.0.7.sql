
ALTER IGNORE TABLE `#__sc_orders`
  ADD COLUMN `shipper` VARCHAR(64),
  ADD COLUMN `discountcode` VARCHAR(255),
  ADD COLUMN `discount` FLOAT;

ALTER IGNORE TABLE `#__sc_orders`
  ADD COLUMN `emailsent` int(1) NOT NULL DEFAULT 0,
  ADD COLUMN `language` char(7) DEFAULT '*';

ALTER IGNORE TABLE `#__sc_productoptions`
  ADD COLUMN `image` varchar(255) NOT NULL,
  ADD COLUMN `classname` varchar(255) DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `#__sc_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modulename` varchar(64) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ;

INSERT IGNORE INTO `#__sc_modules` VALUES ('1', 'products', '0');
INSERT IGNORE INTO `#__sc_modules` VALUES ('2', 'scplugins', '8');
INSERT IGNORE INTO `#__sc_modules` VALUES ('3', 'orders', '1');
INSERT IGNORE INTO `#__sc_modules` VALUES ('4', 'sccontents', '2');
INSERT IGNORE INTO `#__sc_modules` VALUES ('5', 'fields', '3');
INSERT IGNORE INTO `#__sc_modules` VALUES ('6', 'sc_configuration', '4');
INSERT IGNORE INTO `#__sc_modules` VALUES ('7', 'scphocagallery', '5');
