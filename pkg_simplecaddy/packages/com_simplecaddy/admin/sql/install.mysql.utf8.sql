DROP TABLE IF EXISTS `#__sc_config`;

CREATE TABLE IF NOT EXISTS `#__sc_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `setting` text NOT NULL,
  `cfgset` int(11) NOT NULL DEFAULT '0',
  `type` enum('text','textarea','richtext','yesno','list') NOT NULL,
  `indopts` text NOT NULL,
  `sh` int(11) NOT NULL,
  `sv` int(11) NOT NULL,
  `pagename` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kw` (`keyword`)
);

CREATE TABLE IF NOT EXISTS `#__sc_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NULL,
  `caption` varchar(255) NULL,
  `type` varchar(16) NULL,
  `length` varchar(11) NULL,
  `classname` varchar(64)  NULL,
  `required` int(11) NULL,
  `ordering` int(11) NULL,
  `published` int(11) NULL,
  `checkfunction` varchar(64) NULL,
  `fieldcontents` text NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `#__sc_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modulename` varchar(64) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ;

CREATE TABLE IF NOT EXISTS `#__sc_odetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NULL,
  `prodcode` varchar(10)  NULL,
  `qty` int(11)  NULL,
  `unitprice` float  NULL,
  `total` float  NULL,
  `shorttext` varchar(255)  NULL,
  `option` varchar(255)  NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `#__sc_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text,
  `codepostal` varchar(15) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `telephone` varchar(32) DEFAULT NULL,
  `ipaddress` varchar(16) DEFAULT NULL,
  `customfields` text,
  `orderdt` int(11) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `status` varchar(16) DEFAULT NULL,
  `tax` float DEFAULT NULL,
  `archive` int(11) DEFAULT NULL,
  `shipper` varchar(64) DEFAULT NULL,
  `shipCost` varchar(10) DEFAULT NULL,
  `shipRegion` varchar(255) DEFAULT NULL,
  `j_user_id` int(11) DEFAULT NULL,
  `ordercode` varchar(255) DEFAULT NULL,
  `orderlink` varchar(255) DEFAULT NULL,
  `paymentcode` varchar(255) DEFAULT NULL,
  `discountcode` varchar(255) DEFAULT NULL,
  `discount` float DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `#__sc_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prodcode` varchar(64) DEFAULT NULL,
  `shorttext` varchar(255) DEFAULT NULL,
  `av_qty` int(11) DEFAULT NULL,
  `unitprice` float DEFAULT NULL,
  `published` int(11) DEFAULT NULL,
  `optionstitle` varchar(32) DEFAULT NULL,
  `options` text,
  `showas` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `downloadable` int(11) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `shippoints` varchar(10) DEFAULT NULL,
  `shipwidth` float DEFAULT NULL,
  `shipheight` float DEFAULT NULL,
  `shiplength` float DEFAULT NULL,
  `shipweight` float DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `#__sc_prodoptiongroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prodcode` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `showas` int(11) NOT NULL,
  `disporder` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `#__sc_productoptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `optgroupid` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `formula` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `defselect` int(11) NOT NULL,
  `disporder` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `classname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `#__sc_downloads` (
   `id` int(11) not null auto_increment,
   `filename` varchar(255),
   `paymentkey` varchar(255),
   `datetime` int(11),
   PRIMARY KEY (`id`)
);

INSERT INTO `#__sc_config` VALUES ('1', 'currency', 'SC_CURRENCY_SYMBOL', 'CAD', '0', 'text', '', '0', '0', 'SC_FINANCE');
INSERT INTO `#__sc_config` VALUES ('2', 'show_emptycart', 'SC_SHOW_EMPTY_CART_BUTTON', '1', '0', 'yesno', '', '0', '0', 'SC_FRONTEND_DISPLAY');
INSERT INTO `#__sc_config` VALUES ('3', 'thousand_sep', 'SC_THOUSANDS_SEPARATOR', ',', '0', 'text', '', '0', '0', 'SC_FRONTEND_DISPLAY');
INSERT INTO `#__sc_config` VALUES ('4', 'decimal_sep', 'SC_DECIMAL_SEPARATOR', '.', '0', 'text', '', '0', '0', 'SC_FRONTEND_DISPLAY');
INSERT INTO `#__sc_config` VALUES ('5', 'decimals', 'SC_NUMBER_OF_DECIMALS', '2', '0', 'text', '', '0', '0', 'SC_FRONTEND_DISPLAY');
INSERT INTO `#__sc_config` VALUES ('6', 'remove_button', 'SC_SHOW_REMOVE_BUTTON', '1', '0', 'yesno', '', '0', '0', 'SC_FRONTEND_DISPLAY');
INSERT INTO `#__sc_config` VALUES ('7', 'email_customer', 'SC_SEND_CONFIRMATION_EMAIL_TO_CUSTOMER', '1', '0', 'yesno', '', '0', '0', 'SC_COMMUNICATION');
INSERT INTO `#__sc_config` VALUES ('8', 'email_copies', 'SC_SEND_CONFIRMATION_EMAIL_COPIES_TO', 'me@mysite.com\r\nyou@mysite.com', '0', 'textarea', '', '40', '10', 'SC_COMMUNICATION');
INSERT INTO `#__sc_config` VALUES ('9', 'curralign', 'SC_CURRENCY_SYMBOL_POSITION', '0', '0', 'list', 'Before amount:1\r\nAfter amount:0', '0', '0', 'SC_FRONTEND_DISPLAY');
INSERT INTO `#__sc_config` VALUES ('10', 'ostatus', 'SC_ORDER_STATUSES_ONE_PER_LINE', 'New\r\nReviewed\r\nReview\r\nCancelled\r\nTreated\r\nArchive\r\nPaid', '0', 'textarea', '', '20', '10', 'SC_ORDER_STATUSES');
INSERT INTO `#__sc_config` VALUES ('13', 'dateformat', 'SC_DATE_FORMAT', 'd-m-y', '0', 'text', '', '0', '0', 'SC_FRONTEND_DISPLAY');
INSERT INTO `#__sc_config` VALUES ('14', 'timeformat', 'SC_TIME_FORMAT', 'h:i:s', '0', 'text', '', '0', '0', 'SC_FRONTEND_DISPLAY');
INSERT INTO `#__sc_config` VALUES ('18', 'usecidasemail', 'SC_USE_CONTENT_AS_EMAIL', '0', '0', 'yesno', '', '0', '0', 'SC_COMMUNICATION');
INSERT INTO `#__sc_config` VALUES ('19', 'usestdproduct', 'SC_USE_THE_SYSTEMATIC_PRODUCT', '0', '0', 'yesno', '', '10', '0', 'SC_FINANCE');
INSERT INTO `#__sc_config` VALUES ('20', 'emailcid', 'SC_CONFIRMATION_CONTENT_ID', '0', '0', 'text', '', '3', '0', 'SC_COMMUNICATION');
INSERT INTO `#__sc_config` VALUES ('21', 'cart_fee_product', 'SC_SYSTEMATIC_PRODUCT_CODE_TO_ADD_TO_CART', 'extracost', '0', 'text', '', '10', '0', 'SC_FINANCE');
INSERT INTO `#__sc_config` VALUES ('22', 'prodcats', 'SC_PRODUCT_CATEGORIES_ONE_PER_LINE', 'Food\r\nDrinks\r\nPlants\r\nChemistry\r\nNon-Food', '0', 'textarea', '', '40', '5', 'SC_PRODUCT_CATEGORIES');
INSERT INTO `#__sc_config` VALUES ('26', 'mailengine', 'SC_MAIL_ENGINE', 'joomla', '0', 'list', 'Joomla:joomla\r\nAlternative:alternative', '0', '0', 'SC_COMMUNICATION');
INSERT INTO `#__sc_config` VALUES ('28', 'checkminqty', 'SC_CHECK_MINIMUM_QUANTITY_IN_DB', '1', '0', 'yesno', '', '0', '0', 'SC_CHECKING_OUT');
INSERT INTO `#__sc_config` VALUES ('29', 'mincheckout', 'SC_MINIMUM_AMOUNT_FOR_CHECKOUT', '0', '0', 'text', '', '0', '0', 'SC_CHECKING_OUT');
INSERT INTO `#__sc_config` VALUES ('36', 'email_fromname', 'SC_SEND_EMAIL_FROM_NAME', 'Demo name', '0', 'text', '', '40', '10', 'SC_COMMUNICATION');
INSERT INTO `#__sc_config` VALUES ('37', 'email_from', 'SC_SEND_EMAIL_FROM', 'demo@me.com', '0', 'text', '', '40', '10', 'SC_COMMUNICATION');
INSERT INTO `#__sc_config` VALUES ('39', 'autodecfromstore', 'SC_AUTODEC_FROM_STORE', '0', '0', 'yesno', '', '0', '0', 'SC_CHECKING_OUT');
INSERT INTO `#__sc_config` VALUES ('40', 'use_downloadables', 'SC_USE_DOWNLOADABLES', '1', '0', 'yesno', '', '0', '0', 'SC_MODULES');
INSERT INTO `#__sc_config` VALUES ('41', 'downloadcid', 'SC_DOWNLOADCID', '78', '0', 'text', '', '1', '0', 'SC_DOWNLOADABLES');
INSERT INTO `#__sc_config` VALUES ('44', 'email_subject', 'SC_EMAIL_SUBJECT', 'Order from #name# for #ordertotal#', '0', 'text', '', '80', '0', 'SC_COMMUNICATION');
INSERT INTO `#__sc_config` VALUES ('45', 'downloadpath', 'SC_DOWNLOADPATH', '/', '0', 'text', '', '120', '0', 'SC_DOWNLOADABLES');
INSERT INTO `#__sc_config` VALUES ('47', 'show_tax_fields', 'SC_SHOW_TAX_FIELDS', '1', '0', 'yesno', '', '0', '0', 'SC_MODULES');
INSERT INTO `#__sc_config` VALUES ('48', 'show_shipping_fields', 'SC_SHOW_SHIPPING_FIELDS', '1', '0', 'yesno', '', '0', '0', 'SC_MODULES');
INSERT INTO `#__sc_config` VALUES ('49', 'itemseparator', 'SC_ITEM_SEPARATOR', '/', '0', 'text', '', '80', '0', 'SC_FRONTEND_DISPLAY');
INSERT INTO `#__sc_config` VALUES ('50', 'taxonshipping', 'SC_PAY_TAX_ON_SHIPPING', '1', '0', 'yesno', '', '0', '0', 'SC_FINANCE');

INSERT IGNORE INTO `#__sc_fields` VALUES (1, 'name', 'Your Name', 'text', '50', 'inputbox', 1, 10, 1, 'checkfilled', '');
INSERT IGNORE INTO `#__sc_fields` VALUES (2, 'address1', 'Address', 'textarea', '0', 'inputbox', 1, 40, 1, 'checkfilled', '');
INSERT IGNORE INTO `#__sc_fields` VALUES (3, 'postal_code', 'Postal code / Zipcode', 'text', '10', 'inputbox', 0, 90, 1, 'checkfilled', '');
INSERT IGNORE INTO `#__sc_fields` VALUES (10, 'deliverybefore', 'Deliver before', 'date', '0', 'inputbox', 1, 120, 0, 'checkfilled', '');
INSERT IGNORE INTO `#__sc_fields` VALUES (5, 'city', 'City', 'text', '40', 'inputbox', 0, 60, 1, 'checkfilled', '');
INSERT IGNORE INTO `#__sc_fields` VALUES (6, 'phone', 'Phone', 'text', '40', 'inputbox', 0, 110, 1, 'checkfilled', '');
INSERT IGNORE INTO `#__sc_fields` VALUES (7, 'email', 'Email', 'text', '40', 'inputbox', 0, 30, 1, 'checkfilled', '');
INSERT IGNORE INTO `#__sc_fields` VALUES (16, 'region', 'Region', 'dropdown', '0', 'inputbox', 0, 70, 1, 'checkfilled', 'Alberta;British Columbia;Manitoba;New Brunswick;Newfoundland and Labrador;Northwest Territories;Nova Scotia;Nunavut;Ontario;Prince Edward Island;Quebec;Saskatchewan;Yukon');
INSERT IGNORE INTO `#__sc_fields` VALUES (17, 'country', 'Country', 'text', '0', 'inputbox', 0, 80, 1, 'checkfilled', '');
INSERT IGNORE INTO `#__sc_fields` VALUES (11, 'div1', 'Extra info', 'divider', '0', 'divider', 0, 100, 1, 'checkfilled', '');
INSERT IGNORE INTO `#__sc_fields` VALUES (15, 'address2', 'Address 2', 'text', '0', 'inputbox', 0, 50, 1, 'checkfilled', '');
INSERT IGNORE INTO `#__sc_fields` VALUES (14, 'username', 'Username', 'text', '0', 'inputbox', 1, 20, 1, 'checkfilled', '');

INSERT IGNORE INTO `#__sc_modules` VALUES ('1', 'products', '0');
INSERT IGNORE INTO `#__sc_modules` VALUES ('2', 'scplugins', '8');
INSERT IGNORE INTO `#__sc_modules` VALUES ('3', 'orders', '1');
INSERT IGNORE INTO `#__sc_modules` VALUES ('4', 'sccontents', '2');
INSERT IGNORE INTO `#__sc_modules` VALUES ('5', 'fields', '3');
INSERT IGNORE INTO `#__sc_modules` VALUES ('6', 'sc_configuration', '4');
INSERT IGNORE INTO `#__sc_modules` VALUES ('7', 'scphocagallery', '5');
