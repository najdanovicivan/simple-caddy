<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Henk von Pickartz
 * @license		GNU General Public License version 2 or later
 * This plugin is created based on the SKELETON plugin on http://atlanticintelligence.net
 * SimpleCaddy coupon plugin v1.0 for SimpleCaddy 2.x and Joomla 2.5
 */

// No direct access.
defined('_JEXEC') or die;
if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
	require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
	return;
}

class sccouponsconfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $couponcode;
	var $timelimited;
	var $numberlimited;
	var $expiry;
	var $num_avail;
	var $product_specific;
	var $prod_codes;
	var $value_type;
	var $value;
	var $published;

	function __construct() { // initialise class and check if data table exists. If not, create it
		// Load plugin language, since we use it in the backend, standard language loading is NOT performed
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_sccoupons', JPATH_ADMINISTRATOR);
		$db	=JFactory::getDBO();
		try {
			parent::__construct( '#__sc_coupons', 'id', $db );
		}
		catch (Exception $e) {
			$query= "CREATE TABLE `#__sc_coupons` (
					`id`  int NOT NULL AUTO_INCREMENT ,
					`couponcode`  varchar(32) NOT NULL ,
					`timelimited`  int NOT NULL ,
					`numberlimited`  int NOT NULL ,
					`expiry`  int NULL ,
					`num_avail`  int NULL ,
					`product_specific`  int NOT NULL ,
					`prod_codes`  text NULL ,
					`value_type`  int NULL ,
					`value`  varchar(64) NOT NULL ,
					`published`  int NOT NULL ,
					PRIMARY KEY (`id`),
					UNIQUE INDEX `couponcode` (`couponcode`)
					)
					;";
			$this->_db->setQuery($query);
			$this->_db->execute();
			parent::__construct( '#__sc_coupons', 'id', $db );
		};
		// try to update orders table with extra discount column
		$query="SELECT * FROM #__sc_orders limit 1;";
		$db->setQuery($query);
		$result=$db->loadObject();
		if (@!$result->discount)
		{
			try
			{
				$query = "ALTER TABLE `#__sc_orders` ADD COLUMN `discount`  float NULL AFTER `paymentcode`;";
				$db->setQuery($query);
				$db->execute();
				$query = "ALTER TABLE `#__sc_orders` ADD COLUMN `discountcode`  varchar(255) NULL AFTER `paymentcode`;";
				$db->setQuery($query);
				$db->execute();
			} catch (Exception $e) {}
		}
	}

	function addnew() {
		$this->editcoupon();
	}

	function edit() {
		$mainframe = JFactory::getApplication();
		$input     = $mainframe->input;

		$toedit=$input->get("cid");
		$this->editcoupon($toedit[0]);
	}

	function remove() {
		$mainframe = JFactory::getApplication();
		$input     = $mainframe->input;
		$todelete=$input->get("cid");
		foreach($todelete as $key=>$id) {
			$this->delete($id);
		}
		$mainframe=JFactory::getApplication();
		$url="index.php?option=com_simplecaddy&action=pluginconfig&task=showpluginconfig&pluginname=sccoupons";
		$mainframe->redirect($url);
	}

	function publishcoupon() {
		$mainframe = JFactory::getApplication();
		$input     = $mainframe->input;
		$cid=$input->get("cid");
		foreach ($cid as $key=>$id) {
			$this->load($id);
			$this->published=1;
			$this->store();
		}
		$url="index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=sccoupons";
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage("Counpon published");
		$mainframe->redirect($url);
	}

	function unpublishcoupon() {
		$mainframe = JFactory::getApplication();
		$input     = $mainframe->input;
		$cid=$input->get("cid");
		foreach ($cid as $key=>$id) {
			$this->load($id);
			$this->published=0;
			$this->store();
		}
		$url="index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=sccoupons";
		$mainframe=JFactory::getApplication();
		$mainframe->redirect($url);
	}

	function save($src=null, $orderingFilter = '', $ignore = '') { // mandatory function to save any variables
		$this->bind($_REQUEST); // get all the fields from the admin form
		$this->expiry=strtotime($this->expiry);
		$b=$this->store(); // store the relevant fields only
		if ($b) {
			$msg=JText::_("SCC_COUPON_SAVED");
		}
		else
		{
			$msg=JText::_("SCC_COUPON_NOT_SAVED");
			$msg .= " " . $this->_db->getErrorMsg();
		}
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=pluginconfig&task=showpluginconfig&pluginname=sccoupons", $msg);
	}

	function cancel() {
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
	}

	function pluginlist() {
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=plugins");
	}

	function get($key, $default=null) { // retrieves voucher key from storage
		$query="select `id` from `$this->_tbl` where `couponcode` = '$key'";
		$this->_db->setQuery($query);
		$id=$this->_db->loadResult();
		$this->load($id);
	}

	function getlist($sort=null, $sorttype="ASC") {
		$query=" select * from `$this->_tbl` ";
		if ($sort) $query .= " ORDER BY `$sort` $sorttype ";
		$this->_db->setQuery($query);
		$lst=$this->_db->loadObjectList();
		return $lst;
	}

	function editcoupon($id=null) {
		$this->load($id);
		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
//		JHTML::script('components/com_simplecaddy/js/datetimepicker.js' );
//		JHtml::_('bootstrap.framework');
		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCC_SIMPLECADDY_EDIT_COUPON" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SCC_SAVE"), false);
		JToolbarHelper::cancel();
		?>
		<form name="adminForm" method="post" id="adminForm">
			<table class="table">
				<tr><td width="180">id</td><td><?php echo $this->id;?></td></tr>
				<tr><td><?php echo JText::_("SCC_COUPONCODE");?></td><td><input type="text" name="couponcode" value="<?php echo $this->couponcode;?>" /></td></tr>
				<tr><td><?php echo JText::_("SCC_TIME_LIMITED");?></td>
					<td>
						<div class="control-group">
							<div class="controls">
								<fieldset class="radio btn-group">
									<label for="0-timelimited"><input type="radio" id="0-timelimited" value="0" name="timelimited" <?php echo ($this->timelimited==0?' checked="checked"':'');?>><?php echo JText::_('JNO');?></label>
									<label for="1-timelimited"><input type="radio" id="1-timelimited" value="1" name="timelimited" <?php echo ($this->timelimited==1?' checked="checked"':'');?> ><?php echo JText::_('JYES');?></label>
								</fieldset>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td><?php echo JText::_("SCC_EXPIRES");?></td>
					<td>
						<?php echo JHTML::calendar(($this->expiry?date("d-m-Y h:i:s", $this->expiry):""), "expiry", "expiry", '%d-%m-%Y');?>
					</td>
				</tr>
				<tr><td><?php echo JText::_("SCC_NUMBER_LIMITED");?></td>
					<td>
						<div class="control-group">
							<div class="controls">
								<fieldset class="radio btn-group">
									<label for="0-numberlimited"><input type="radio" id="0-numberlimited" value="0" name="numberlimited" <?php echo ($this->numberlimited==0?' checked="checked"':'');?>><?php echo JText::_('JNO');?></label>
									<label for="1-numberlimited"><input type="radio" id="1-numberlimited" value="1" name="numberlimited" <?php echo ($this->numberlimited==1?' checked="checked"':'');?> ><?php echo JText::_('JYES');?></label>
								</fieldset>
							</div>
						</div>

					</td>
				</tr>
				<tr><td><?php echo JText::_("SCC_NUM_AVAIL");?></td><td><input type="text" name="num_avail" value="<?php echo $this->num_avail;?>" size="5" /></td></tr>
				<tr><td><?php echo JText::_("SCC_PRODUCT_SPECIFIC");?></td>
					<td>
						<div class="control-group">
							<div class="controls">
								<fieldset class="radio btn-group">
									<label for="0-product_specific"><input type="radio" id="0-product_specific" value="0" name="product_specific" <?php echo ($this->product_specific==0?' checked="checked"':'');?>><?php echo JText::_('JNO');?></label>
									<label for="1-product_specific"><input type="radio" id="1-product_specific" value="1" name="product_specific" <?php echo ($this->product_specific==1?' checked="checked"':'');?> ><?php echo JText::_('JYES');?></label>
								</fieldset>
							</div>
						</div>
					</td>
				</tr>
				<tr><td><?php echo JText::_("SCC_PRODCODES");?></td>
					<td>
						<textarea name="prod_codes" cols="60" rows="6"><?php echo $this->prod_codes;?></textarea>
					</td>
				</tr>
				<tr><td><?php echo JText::_("SCC_VALUE_TYPE");?></td>
					<td>
						<?php
						$options[0]=JText::_("SCC_PERCENTAGE");
						$options[1]=JText::_("SCC_FIXED");
						$dropdown = JHTML::_('select.genericlist', $options, 'value_type', 'class="inputbox"', 'value', 'text', $this->value_type);
						echo $dropdown;
						?>
					</td></tr>
				<tr><td><?php echo JText::_("SCC_VALUE");?></td><td><input type="text" name="value" value="<?php echo $this->value;?>" /></td></tr>
				<tr><td><?php echo JText::_("SCC_ENABLED");?></td>
					<td>
						<div class="control-group">
							<div class="controls">
								<fieldset class="radio btn-group">
									<label for="0-published"><input type="radio" id="0-published" value="0" name="published" <?php echo ($this->published==0?' checked="checked"':'');?>><?php echo JText::_('JNO');?></label>
									<label for="1-published"><input type="radio" id="1-published" value="1" name="published" <?php echo ($this->published==1?' checked="checked"':'');?> ><?php echo JText::_('JYES');?></label>
								</fieldset>
							</div>
						</div>
					</td>
				</tr>
			</table>
			<input type="hidden" name="option" value="com_simplecaddy" />
			<input type="hidden" name="action" value="pluginconfig"/>
			<input type="hidden" name="pluginname" value="sccoupons"/>
			<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
			<input type="hidden" name="boxchecked" />
			<input type="hidden" name="task" />
		</form>
		<?php
	}

	function _showconfig() { // mandatory function name, identical for all SC plugins
		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCC_SIMPLECADDY_COUPON_LIST" ), 'generic.png');
		JToolBarHelper::addNew( 'addnew', JText::_("SCC_ADDNEW") );
		JToolBarHelper::custom('edit',"edit", "edit", JText::_("SCC_EDIT"));
		JToolbarHelper::deleteList();
		JToolBarHelper::custom("publishcoupon", "publish", "", JText::_("SCC_PUBLISH"), true);
		JToolBarHelper::custom("unpublishcoupon", "unpublish", "", JText::_("SCC_UNPUBLISH"), true);
		JToolBarHelper::custom('pluginlist', 'back', '', 'Main', false);

		$rows=$this->getlist('couponcode');
		?>
		<form name="adminForm" id="adminForm" method="post">
			<table class="table table-bordered table-hover">
				<tr>
					<th style="width:20px;"><?php echo JHTML::_('grid.checkall'); ?></th>
					<th><?php echo JText::_("SCC_COUPONCODE");?></th>
					<th><?php echo JText::_("SCC_TIME_LIMITED");?></th>
					<th><?php echo JText::_("SCC_EXPIRES");?></th>
					<th><?php echo JText::_("SCC_NUMBER_LIMITED");?></th>
					<th><?php echo JText::_("SCC_NUM_AVAIL");?></th>
					<th><?php echo JText::_("SCC_VALUE_TYPE");?></th>
					<th><?php echo JText::_("SCC_ENABLED");?></th>
				</tr>
				<?php
				$k = 0;
				for ($i=0, $n=count( $rows ); $i < $n; $i++) {
					$row = &$rows[$i];
					?>
					<tr class="<?php echo "row$k"; ?>">
						<td>
							<?php echo JHtml::_('grid.id', $i, $row->id); ?>
						</td>
						<td>
							<a href="#edit" onclick="return listItemTask('cb<?php echo $i;?>','edit')">
								<?php echo $row->couponcode;?></a>
						</td>
						<td><?php echo ($row->timelimited==1? JText::_("SCC_YES"): JText::_("SCC_NO") );?></td>
						<td><?php echo ($row->timelimited==1? date("d-m-Y", $row->expiry):"");?></td>
						<td><?php echo ($row->numberlimited==1? JText::_("SCC_YES"): JText::_("SCC_NO") ) ;?></td>
						<td><?php echo ($row->numberlimited==1? $row->num_avail:"");?></td>
						<td><?php echo ($row->value_type==1? JText::_("SCC_FIXED"): JText::_("SCC_PERCENTAGE") );?></td>
						<td>
							<?php
							$published 	= JHTML::_('grid.published', $row, $i );
							echo $published;
							?>
						</td>
					</tr>
					<?php
					$k= 1-$k;
				}
				?>
			</table>

			<input type="hidden" name="option" value="com_simplecaddy" />
			<input type="hidden" name="action" value="pluginconfig"/>
			<input type="hidden" name="pluginname" value="sccoupons"/>
			<input type="hidden" name="boxchecked" />
			<input type="hidden" name="task" />
		</form>
		<?php
	}

	function isincart() {
		$cart=new cart2();
		$validprodcodes=explode("\r\n", $this->prod_codes);
		foreach ($validprodcodes as $key=>$prodcode) {
			$valid=$cart->isInCart($prodcode);
			if ($valid) return true;
		}
		return false;
	}

	function check() {
		$mainframe = JFactory::getApplication();
		$input     = $mainframe->input;
		$couponcode=$input->get("coupon");
		$ordercode=$input->get("data");

		$orders=new orders();
		$orderid=$orders->getOrderIdFromCart($ordercode); // now we have an order ID
		$order= new order();
		$order->load($orderid);

		$thiscid=$input->get("thiscid"); // article content id
		$this->get($couponcode);
		$valid=true;
		$now=time();
		$value=0;
		if ( (@ $this->id) and ($this->published==1) ) {
			if ( ($this->numberlimited==1) and ($this->num_avail <=0)) $valid=false;
			if ( ($this->product_specific==1) and (!$this->isincart()) and ($valid)) $valid=false;
			if ( ($this->timelimited==1) and ($this->expiry <=$now) and ($valid) ) $valid=false;
			if ($valid) { // coupon code is valid, now determine the value
				if ($this->value_type==1) { // fixed
					$value=$this->value;
				}
				if ($this->value_type==0) { // percentage
					$ctotal=$order->total;
					$this->value=str_replace("%", "", $this->value);
					$this->value=str_replace(".", "", $this->value);
					$value= -1 * abs($ctotal * $this->value / 100);
				}
				$order->discount=-1* abs($value); // make sure it's always a negative value as it's a discount...
				$order->discountcode=$couponcode;
				$order->store();
				if ( ($this->numberlimited==1) ) {
					$this->num_avail=$this->num_avail -1;
					$this->store();
				}
				$msg=JText::sprintf("SCC_COUPON_APPLIED", ($value) );
				$mainframe->enqueueMessage($msg);
			}
			else
			{
				$order->discount=0;
				$order->discountcode="";
				$order->store();
				$msg=JText::_("SCC_INVALID_COUPON");
				$mainframe->enqueueMessage($msg, "error");
			}
		}
		else
		{
			$order->discount=0;
			$order->discountcode="";
			$order->store();
			$msg=JText::_("SCC_INVALID_COUPON");
			$mainframe->enqueueMessage($msg, "error");
		}

		// go back to where we came from
		$url =htmlspecialchars_decode(JRoute::_( ContentHelperRoute::getArticleRoute($thiscid) ));
		if (strpos($url, "?")>0) {
			$url .= "&data=".$ordercode;
		}
		else
		{
			$url .= "?data=".$ordercode;
		}
		$mainframe->redirect($url);
	}

}

/**
 * Sccoupons plugin joomla standard part for frontend
 */
class plgContentSccoupons extends JPlugin {
	var $_plugin_number	= 0; // plugin counter -- see below
	var $thiscid;

	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	public function _setPluginNumber() {
		$this->_plugin_number = (int)$this->_plugin_number + 1;
	}

	public function onContentPrepare($context, &$article, &$params, $page = 0)
	{
		// check if we are displaying standard content. If not, get outta here
		$canProceed = $context == 'com_content.article';
		if (!$canProceed) {
			return; // obviously you can imagine better ways of dealing with this, depending on the situation
		}
		$this->thiscid=$article->id;
		// get parameters of the plugin set in admin
		$baserate= $this->params->get('baserate');

		// find the plugin code in the article text
		$regex = '/{(sccoupons)\s*(.*?)}/i'; // modify to the real name of your plugin
		$matches = array();
		preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );

		// parse the command line into usable pieces
		$parms=array();
		foreach ($matches as $match) {
			$this->_setPluginNumber();
			if ($this->_plugin_number==1) { // Example: get the stylesheet only ONCE per page, independently of the number of plugins on the page
				JHTML::stylesheet('plugins/content/sccoupons/css/sccoupons.css');
				// on windows servers this may need to be changed to
				//JHTML::AddStylesheet('sccoupons.css','plugins\content\sccoupons\css\' );
			}

			// the following will try to clean up any plugin command line
			$line=str_replace("&nbsp;", " ", $match[2]); // replace any html spaces with normal ones
			$line=str_replace(" ", "&", $line); // now change all the spaces to &
			$line=strtolower($line); // transform everything to lowercase so that won't bother anyone
			// we could even make sure there is no html like <span> tags left in the line due to TinyMCE or other editors inserting html
			// try that for yourself...

			parse_str( $line, $parms ); // use an integrated php function to transform the url string into an array of parameters

			if (!isset($parms['type'])) { // no type provided, probably just forgot...
				$parms["type"]="unused"; // set it to the default type to make sure
			}

			// get or define all different types of display we support
			switch (strtolower($parms['type']) ) {
				case "unused": // the default, if nothing has been provided
					$html=$this->_showcouponentry($parms);
					break;
				default: // anything else provides an error message but should not crash the plugin or joomla!
					$html=JText::_("SK_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). " ({$parms['type']})";
			}
			$article->text = preg_replace($regex, $html, $article->text, 1);
		}
		return false;
	}

	protected function _showcouponentry($commandline=array())	{
		$mainframe = JFactory::getApplication();
		$input     = $mainframe->input;
		$data=$input->get("data");
		if (@!$data) {
			return "";
		}
		$formname=uniqid("V");
		$html="<div class='sccoupondiv'>";
		$html .= "<form name='$formname' method='post'>";
		$html .= "<input class='sccouponinput' type='text' name='coupon'>";
		$html .= "<input class='sccouponsubmit' type='submit' name='submit' value='".JText::_("SCC_SUBMIT")."'>";
		$html .= "<input type='hidden' name='option' value='com_simplecaddy'>";
		$html .= "<input type='hidden' name='action' value='scplugins'>";
		$html .= "<input type='hidden' name='task' value='check'>";
		$html .= "<input type='hidden' name='pluginname' value='sccoupons'>";
		$html .= "<input type='hidden' name='data' value='$data'>";
		$html .= "<input type='hidden' name='thiscid' value='$this->thiscid'>";
		$html .= "</form></div>";

		return $html;
	}


}

