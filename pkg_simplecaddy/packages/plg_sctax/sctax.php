<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Henk von Pickartz
 * @license		GNU General Public License version 2 or later
 * SimpleCaddy Tax plugin for SimpleCaddy 2.0 for Joomla 3.x
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
	require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
	return;
}

/**
 * configuration class
 */

class sctaxconfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $baserate;
	var $controlfield;
	var $shipcontrolfield;
	var $rates;

	function __construct() { // initialise class and check if data table exists. If not, create it
		// Load plugin language, since we use it in the backend, standard language loading is NOT performed
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_sctax', JPATH_ADMINISTRATOR);
		$db	= JFactory::getDBO();
		try {
	   		parent::__construct( '#__sc_tax', 'id', $db );
		}
		catch (Exception $e) {
			$query= "CREATE TABLE `#__sc_tax` (
			`id`  int NULL AUTO_INCREMENT ,
			`baserate`  float NULL ,
			`controlfield`  varchar(32) NULL ,
			`shipcontrolfield`  varchar(32) NULL ,
			`rates`  text NULL ,
			PRIMARY KEY (`id`)
			);";
			$this->_db->setQuery($query);
			$this->_db->execute();
			parent::__construct( '#__sc_tax', 'id', $db );
		}
		// get the first and only useful record in the db here
		$query=$this->_db->getQuery(true);
		$query->select("`id`");
		$query->from("`$this->_tbl`");
		$this->_db->setQuery($query);
		$id=$this->_db->loadResult(); // this sets the first ID as the one to use.
		$this->load($id); // load the first id
	}

	function _showconfig() { // mandatory function name
		$lang = JFactory::getLanguage(); // joomla's backend does not load frontend languages by itself
		$extension = 'plg_content_sctax1'; // so we need to do this manually here
		$lang->load($extension);

		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
		$fields=new fields();
		$fieldlist=$fields->getFieldNames(); // get all the fields, standard and custom
		$this->load();
		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCTAX_RATES" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SCTAX_SAVE"), false);
		JToolbarHelper::cancel();
		JToolBarHelper::custom('control', 'back', '', 'Main', false);
		?>
		<form name="adminForm" id="adminForm" method="post">
			<table class="adminform">
				<tr>
					<td style="width: 150px;"><?php echo JText::_("SCTAX_BASERATE");?>
					</td>
					<td>
						<input type="text" name="baserate" value="<?php echo $this->baserate; ?>" size="5" /> %
					</td>
				</tr>
				<tr>
					<td style="width: 150px;">
						<?php echo JText::_("SCTAX_CONTROLFIELD");?>
					</td>
					<td>
				<select name="controlfield">
				<?php
					foreach ($fieldlist as $key=>$f) {
						echo "<option value='$f'".($f==$this->controlfield?" selected":"").">$f</option>";
					}
				?>
				</select>
				</td>
				</tr>
				<tr>
					<td style="width: 150px;">
						<?php echo JText::_("SCTAX_SHIPCONTROLFIELD");?>
					</td>
					<td>
				<select name="shipcontrolfield">
				<?php
					foreach ($fieldlist as $key=>$f) {
						echo "<option value='$f'".($f==$this->shipcontrolfield?" selected":"").">$f</option>";
					}
				?>
				</select>
				</td></tr>

				<tr><td style="width: 150px;"><?php echo JText::_("SCTAX_TAXRATES");?></td><td>
				<textarea name="rates" cols="50" rows="10"><?php echo trim($this->rates); ?></textarea>
				</td></tr>
			</table>
			<input type="hidden" name="option" value="com_simplecaddy" />
			<input type="hidden" name="action" value="pluginconfig"/>
			<input type="hidden" name="pluginname" value="sctax"/>
			<input type="hidden" name="task" />
			<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
		</form>
		<?php
	}

	function save($src="", $orderingFilter = '', $ignore = '') { // mandatory function to save any variables
		$this->bind($_REQUEST); // get all the fields from the admin form
		$this->rates=trim($this->rates);
		$this->store(); // store the relevant fields only
		$msg=JText::_("SCTAX_CONFIG_SAVED");
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage($msg);
		$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=sctax");
	}

	function get($property="", $default = null) { // retrieves config key from storage

	}

	function cancel() {
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
	}

}

/**
 * Sctax plugin joomla standard part
 */
class plgContentSctax extends JPlugin { // the Sctax part needs to be changed to the name of your plugin
	var $_plugin_number	= 0; // plugin counter -- see below

	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	public function _setPluginNumber() {
		$this->_plugin_number = (int)$this->_plugin_number + 1;
	}

	public function onContentSimpleCaddyTax($context, &$article, &$params, $page = 0)
	{
		$input=JFactory::getApplication()->input;
		$view=$input->get("view");
		// check if we are displaying standard content. If not, get outta here
		$canProceed = ( ($context == 'com_content.article') and ($view=="article") );
		if (!$canProceed) {
			return "";
		}

		$regex = '/{(sctax)\s*(.*?)}/i';
        $matches = array();
        preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );

        $parms=array();
        foreach ($matches as $match) {
	        $this->_setPluginNumber();
			if ($this->_plugin_number==1) {
				// Example: get the stylesheet only ONCE per page, independently of the number of plugins on the page
 			}

 			// the following will try to clean up any plugin command line
			$line=str_replace("&nbsp;", " ", $match[2]); // replace any html spaces with normal ones
            $line=str_replace(" ", "&", $line); // now change all the spaces to &
            $line=strtolower($line); // transform everything to lowercase so that won't bother anyone
			$line=strip_tags($line); // make sure there is no html like <span> tags left in the line due to TinyMCE or other editors inserting html

            parse_str( $line, $parms ); // use an integrated php function to transform the url string into an array of parameters

            if (!isset($parms['type'])) { // no type provided, probably just forgot...
            	$parms["type"]="notincluded"; // set it to the default type to make sure
            }

        	switch (strtolower($parms['type']) ) {
       			case "notincluded": // the default, if nothing has been provided
                	$html=$this->_shownotincluded($parms);
                	break;
        		default: // anything else provides an error message but should not crash the plugin or joomla!
                	$html=JText::_("SK_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). " ({$parms['type']})";
        	}
	        if (isset($article->text)) {
		        $article->text = preg_replace($regex, $html, $article->text, 1);
	        }
	        else
	        {
		        $article->introtext = preg_replace($regex, $html, $article->introtext, 1);
	        }
        }
        return false;
	}

	protected function _shownotincluded($commandline=array())	{
		$input=JFactory::getApplication()->input;
		$cfg=new sc_configuration();
		$taxonshipping=$cfg->get("taxonshipping");
		$html="";
		$ordercode=$input->get("data"); // the data contains the ordercode when you finish the details page
		$orders=new orders();
		$orderid=$orders->getOrderIdFromCart($ordercode); // now we have an order ID
		$order= new order();
		$order->load($orderid);
		$cfields=unserialize($order->customfields); // returns an array with all fields
		$taxcfg=new sctaxconfiguration();
		if ( (strtolower(@$cfields['useshipping'])=="no") or (!@isset($cfields['useshipping'])) ) {
			$controlfield=$taxcfg->controlfield;
		}
		else
		{
			$controlfield=$taxcfg->shipcontrolfield;
		}
		$destfound=false;
		$staxdef=$taxcfg->rates;
		if (trim($staxdef)!="") { // there ARE tax definitions with specific destinations
			$ataxdef=explode("\r\n", $staxdef);
			$orderdestination=$cfields[$controlfield]; // get the destination from the fields by $controlfield
			foreach ($ataxdef as $key=>$def) {
				list ($destination, $taxrate) = explode(":", $def);
				if (strtolower($destination) == strtolower($orderdestination) ) { // does this destination pay tax ?
					if ($taxonshipping==1)
					{
						$taxamount = ($order->total + $order->shipCost + @$order->discount) * ($taxrate / 100);
					}
					else
					{
						$taxamount = ($order->total + @$order->discount) * ($taxrate / 100);
					}
					$order->tax=$taxamount; // update the tax
					$order->store(); // store the order with new total
					$destfound=true;
				}
			}
		}
		if ($taxcfg->baserate!="" and !$destfound) { // if there is a baserate and nothing found in the destinations list
			$taxrate=$taxcfg->baserate;
			if ($taxonshipping==1)
			{
				$taxamount = ($order->total + $order->shipCost + @$order->discount) * ($taxrate / 100);
			}
			else
			{
				$taxamount = ($order->total + @$order->discount) * ($taxrate / 100);
			}
			$order->tax=$taxamount; // update the tax
			if (!$destfound) $order->store(); // store the order with new total
		}
		return $html;
	}
}