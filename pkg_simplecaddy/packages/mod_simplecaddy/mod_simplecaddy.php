<?php
/**
* Package SimpleCaddy 2.0 for Joomla 2.5
* @Copyright Henk von Pickartz 2006-2013
*/
/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );
jimport('joomla.application.component.router');
require_once (JPATH_SITE.'/components/com_content/helpers/route.php');
//$mainframe=JFactory::getApplication();

if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
    require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
    return;
}

$showtotals=$params->get("productshow");

$a=getCaddy($showtotals);
$itemid	= $params->get('DisplayID');
$nextcid=$params->get("nextcid");

$url = JRoute::_(ContentHelperRoute::getArticleRoute($nextcid)) ;


echo $a;
echo "<div class='row'>
<div class='col-xs-12'>
        <form name='frmcheckout' method ='post' action='$url'>
        	<input  type='submit' class='btnshowcart' name='submit' value='".JText::_('SC_SHOW_CART')."' />
        </form>
    </div>
    </div>
";

function getCaddy($showtotals) {
	$cfg=new sc_configuration();
	$tsep=$cfg->get('thousand_sep');
	$dsep=$cfg->get('decimal_sep');
	$decs=$cfg->get('decimals');
	$currency=$cfg->get('currency');
	$curralign=$cfg->get('curralign');


	$cart2=new cart2();
	$cart=$cart2->readCart();
	if (!$cart) {
		echo JText::_('SC_CART_EMPTY');
		return;
	}
	$gtotal=0;
	$nombre=0;
	$emptycart=true;
	$html = "<div class='row'>";
    if ($showtotals==0) {
    	$nombre=$cart2->getCartNumbers();
    }
    else
    {
        $nombre=$cart2->getCartQuantities();
    }
	$carttotal=$cart2->getCartTotal();
	if ($nombre==0) {
		$html=JText::_('SC_CART_EMPTY');
	}
	else
	{
		$total=number_format($carttotal, $decs, $dsep, $tsep);
		$stotal=($curralign==1?JText::_($currency)."&nbsp;$total":"$total&nbsp;".JText::_($currency));

		$html .="<div class='col-xs-12'>".JText::_('SC_YOUR_CART_CONTAINS')." <strong>$nombre</strong> ".(($nombre==1)?JText::_('SC_PRODUCT'):JText::_('SC_PRODUCTS'))."</div></div>";
// the following line provides for Polish grammar
//		$html .="<tr><td colspan=2>".JText::_('SC_YOUR_CART_CONTAINS')." <strong>$nombre</strong> ".(($nombre==1)?JText::_('SC_PRODUCT'):( (($nombre>1) and ($nombre<=4))?JText::_('SC_PRODUCTS'):JText::_('SC_PRODUCTS_2')) )."</td></tr>";

			$html .="<div class='row' style='font-weight:600;'><div class='col-xs-6'>".JText::_('SC_TOTAL')."</div><div class='col-xs-6' style='text-align:right;'>".$stotal."</div>";

		$html .="</div>";
	}
	return $html;
}
?>
