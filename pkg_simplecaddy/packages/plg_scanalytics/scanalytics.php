<?php
/**
 * @copyright Ivan Najdanović 2013
 * SimpleCaddy CCNow processor 
 * @version 1.0.0 for Joomla 2.5
 */
  
// No direct access allowed to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
    require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
    return;
}

/**
 * This plugin has three parts:
 * top part is getting the SimpleCaddy functions library
 * in the function onContentPrepare are the Joomla functions to manage the plugin in the content
 * the function showanalyticsScreen displays and manages Analytscs specifics
 * 
 */
 
class scAnalyticsConfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $analyticscurrency;
	var $analyticsaffiliation; 
	var $trackshipping;
	var $tracktax;
	
	function __construct(){
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_scanalytics', JPATH_ADMINISTRATOR);
		$db	= JFactory::getDBO();
        try {
            parent::__construct( '#__sc_analytics', 'id', $db );
        }
        catch (Exception $e) {
            $query= "CREATE TABLE `#__sc_analytics` (
			`id`  int NULL AUTO_INCREMENT ,
			`analyticscurrency`  text NULL ,
			`analyticsaffiliation`  text NULL ,
			`trackshipping`  text NULL ,
			`tracktax`  text NULL ,
			PRIMARY KEY (`id`)
			);";
            $this->_db->setQuery($query);
            $this->_db->execute();
            // now reconstruct, since it failed above...
            parent::__construct( '#__sc_analytics', 'id', $db );
		};
        // get the first and only useful record in the db here
        $query=$this->_db->getQuery(true);
        $query->select("id");
        $query->from("`$this->_tbl`");
        $this->_db->setQuery($query);
        $id=$this->_db->loadResult(); // this sets the first ID as the one to use.
        $this->load($id); // load the first id
	}

	function _showconfig() { // mandatory function name
		$lang = JFactory::getLanguage(); // joomla's backend does not load frontend languages by itself
		$extension = 'plg_content_scanalytics'; // so we need to do this manually here
		$lang->load($extension);
		
		//Analytics Supported Currencies and Codes
		$analyticscurrency=array();
		$analyticscurrency["DISABLED"]="Disabled";
		$analyticscurrency["AED"]="United Arab Emirates Dirham (AED)";
		$analyticscurrency["ARS"]="Argentine Pesos (ARS)";
		$analyticscurrency["AUD"]="Australian Dollars (AUD)";
		$analyticscurrency["BGN"]="Bulgarian Lev (BGN)";
		$analyticscurrency["BOB"]="Bolivian Boliviano (BOB)";
		$analyticscurrency["BRL"]="Brazilian Real (BRL)";
		$analyticscurrency["CAD"]="Canadian Dollars (CAD)";
		$analyticscurrency["CHF"]="Swiss Francs (CHF)";
		$analyticscurrency["CLP"]="Chilean Peso (CLP)";
		$analyticscurrency["CNY"]="Yuan Renminbi (CNY)";
		$analyticscurrency["COP"]="Colombian Peso (COP)";
		$analyticscurrency["CZK"]="Czech Koruna (CZK)";
		$analyticscurrency["DKK"]="Denmark Kroner (DKK)";
		$analyticscurrency["EGP"]="Egyptian Pound (EGP)";
		$analyticscurrency["EUR"]="Euros (EUR)";
		$analyticscurrency["FRF"]="French Francs (FRF)";
		$analyticscurrency["GBP"]="British Pounds (GBP)";
		$analyticscurrency["HKD"]="Hong Kong Dollar (HKD)";
		$analyticscurrency["HRK"]="Croatian Kuna (HRK)";
		$analyticscurrency["HUF"]="Hungarian Forint (HUF)";
		$analyticscurrency["IDR"]="Indonesian Rupiah (IDR)";
		$analyticscurrency["ILS"]="Israeli Shekel (ILS)";
		$analyticscurrency["INR"]="Indian Rupee (INR)";
		$analyticscurrency["JPY"]="Japanese Yen (YEN)";
		$analyticscurrency["KRW"]="South Korean Won (KRW)";
		$analyticscurrency["LTL"]="Lithuanian Litas (LTL)";
		$analyticscurrency["MAD"]="Moroccan Dirham (MAD)";
		$analyticscurrency["MXN"]="Mexican Peso (MXN)";
		$analyticscurrency["MYR"]="Malaysian Ringgit (MYR)";
		$analyticscurrency["NOK"]="Norway Kroner (NOK)";
		$analyticscurrency["NZD"]="New Zealand Dollars (NZD)";
		$analyticscurrency["PEN"]="Peruvian Nuevo Sol (PEN)";
		$analyticscurrency["PHP"]="Philippine Peso (PHP)";
		$analyticscurrency["PKR"]="Pakistan Rupee (PKR)";
		$analyticscurrency["PLN"]="Polish New Zloty (PLN)";
		$analyticscurrency["RON"]="New Romanian Leu (RON)";
		$analyticscurrency["RSD"]="Serbian Dinar (RSD)";
		$analyticscurrency["RUB"]="Russian Ruble (RUB)";
		$analyticscurrency["SEK"]="Sweden Kronor (SEK)";
		$analyticscurrency["SAR"]="Saudi Riyal (SAR)";
		$analyticscurrency["SGD"]="Singapore Dollars (SGD)";
		$analyticscurrency["THB"]="Thai Baht (THB)";
		$analyticscurrency["TRL"]="Turkish Lira (TRL)";
		$analyticscurrency["TWD"]="New Taiwan Dollar (TWD)";
		$analyticscurrency["UAH"]="Ukrainian Hryvnia (UAH)";
		$analyticscurrency["USD"]="US Dollars (USD)";
		$analyticscurrency["VEF"]="Venezuela Bolivar Fuerte (VEF)";
		$analyticscurrency["VND"]="Vietnamese Dong (VND)";
		$analyticscurrency["ZAR"]="South African Rand (ZAR)";

        require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
		$this->load();

		$db = JFactory::getDbo();
		$query="SELECT sef, title_native, lang_code FROM #__languages ORDER BY sef ASC";
		$db->setQuery($query);
		$languages = $db->loadObjectList();

		$this->analyticscurrency = unserialize($this->analyticscurrency);
		$this->analyticsaffiliation = unserialize($this->analyticsaffiliation);
		$this->trackshipping = unserialize($this->trackshipping);
		$this->tracktax = unserialize($this->tracktax);

		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCANALYTICS_CONFIGURATION" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SCANALYTICS_SAVE"), false); 
		JToolbarHelper::cancel();
        JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false,  false );
        ?>
		<form name="adminForm" id="adminForm">
			<table class="adminform">
				<?php 
					foreach ($languages as $l) : ?>

                        <tr style="display:block; margin-top: 20px; width:160px">
                            <td>
                                <b><?= $l->title_native ." ".JText::_("SCANALYTICS_SETTINGS")?></b>
                            </td>
                        <tr>

						<tr>
                            <td><?= JText::_("SCANALYTICS_CURRENCY") ?></td>
                            <td>
                                <select name="analyticscurrency[<?= $l->lang_code ?>]">
						        <?php foreach ($analyticscurrency as $key=>$f) : ?>
                                    <option value='<?= $key ?>' <?= ($key==$this->analyticscurrency["$l->lang_code"] ? "selected":"")?>><?= $f ?></option>";
						        <?php endforeach; ?>
						        </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= JText::_("SCANALYTICS_AFFILIATION") ?></td>
                            <td>
				   		        <input type="text" name="analyticsaffiliation[<?= $l->lang_code ?>]" value="<?= $this->analyticsaffiliation["$l->lang_code"]?>" size="20" />
                            </td>
                        </tr>
                        <tr>
                            <td><?= JText::_("SCANALYTICS_TRACKSHIPPING") ?></td>
                            <td>
                                <div class='control-group'>
                                    <div class="controls">
                                        <fieldset class="radio btn-group">';
                                            <label for='0trackshipping<?= $l->lang_code ?>'><input type='radio' id='0trackshipping<?= $l->lang_code ?>' value='0' name='trackshipping[<?= $l->lang_code ?>]' <?= ($this->trackshipping[$l->lang_code]==0? 'checked="checked"':'') ?>>No</label>
                                            <label for='1trackshipping<?= $l->lang_code ?>'><input type='radio' id='1trackshipping<?= $l->lang_code ?>' value='1' name='trackshipping[<?= $l->lang_code ?>]' <?= ($this->trackshipping[$l->lang_code]==1? 'checked="checked"':'') ?>>Yes</label>
                                        </fieldset>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><?= JText::_("SCANALYTICS_TRACKTAX")?></td>
                            <td>
                                <div class='control-group'>
                                    <div class="controls">
                                        <fieldset class="radio btn-group">';
                                            <label for='0tracktax<?= $l->lang_code ?>'><input type='radio' id='0tracktax<?= $l->lang_code ?>' value='0' name='tracktax[<?= $l->lang_code ?>]' <?= ($this->tracktax[$l->lang_code]==0? 'checked="checked"':'') ?>>No</label>
                                            <label for='1tracktax<?= $l->lang_code ?>'><input type='radio' id='1tracktax<?= $l->lang_code ?>' value='1' name='tracktax[<?= $l->lang_code ?>]' <?= ($this->tracktax[$l->lang_code]==1? 'checked="checked"':'') ?>>Yes</label>
                                        </fieldset>
                                    </div>
                                </div>
						    </td>
                        </tr>
						
					<?php endforeach; ?>
				
				<input type="hidden" name="option" value="com_simplecaddy" />
				<input type="hidden" name="action" value="pluginconfig"/>
				<input type="hidden" name="pluginname" value="scanalytics"/>
				<input type="hidden" name="task" />
				<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
			</table>
		</form>
		<?php
	}
	
	function save($stay=false, $orderingFilter = '', $ignore = '') { // mandatory function to save any variables
		$this->bind($_REQUEST); // get all the fields from the admin form
		$this->analyticsaffiliation=serialize($this->analyticsaffiliation);
		$this->analyticscurrency=serialize($this->analyticscurrency);
		$this->trackshipping=serialize($this->trackshipping);
		$this->tracktax=serialize($this->tracktax);
		$b=$this->store(); // store the relevant fields only
		if ($b) {
			$msg=JText::_("SCANALYTICS_CONFIG_SAVED");
		}
		else
		{
			$dbmsg=$this->_db->getErrorMsg();
			$msg=JText::_("SCANALYTICS_CONFIG_NOT_SAVED") . $dbmsg;
		}
		$mainframe=JFactory::getApplication();
        $mainframe->enqueueMessage($msg);
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=scanalytics");
	}
	
	function cancel() {
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=plugins&task=show");
	}
    function main() {
        $mainframe=JFactory::getApplication();
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
    }
}

//The Content plugin Loadmodule
class plgContentScAnalytics extends JPlugin {
    var $debugshow="hidden"; // just for testing purposes
    var $_plugin_number	= 0;
    var $currency;
    var $affiliation;
    var $trackshipping;
    var $tracktax;


    function __construct( &$subject, $config ) {
        parent::__construct( $subject, $config );
        $this->loadLanguage(); // necessary or not, let's make sure we get the language file

        $lang = JFactory::getLanguage();
        $cfg=new scAnalyticsConfiguration();


        $currency = unserialize($cfg->analyticscurrency);
        $affiliation = unserialize($cfg->analyticsaffiliation);
        $trackshipping = unserialize($cfg->trackshipping);
        $tracktax = unserialize($cfg->tracktax);

        $this->currency = $currency[$lang->getTag()];
        $this->affiliation = $affiliation[$lang->getTag()];
        $this->trackshipping = $trackshipping[$lang->getTag()];
        $this->tracktax = $tracktax[$lang->getTag()];
    }

    public function _setPluginNumber() {
        $this->_plugin_number = (int)$this->_plugin_number + 1; // only the first occurrence of the plugin should load css
    }

    function onContentPrepare($context, &$article, &$params, $page = 0 ) {
        if (!JComponentHelper::isEnabled('com_simplecaddy', true)) { // check for the component install
            echo "<div style='color:red;'>The SimpleCaddy component is not installed or is not enabled</div>";
            return "";
        }

        $regex = '/{(scanalytics)\s*(.*?)}/i'; // the plugin code to get from content
	
        $parms=array();
        $matches = array();
        preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
        foreach ($matches as $elm) {
            $this->_setPluginNumber();

            $line=strip_tags($elm[2]); // get rid of most of the embedded html tags, if any. Only tags directly after the { will not be cleaned as regex will not pick those up
            $line=str_replace("&nbsp;", " ", $elm[2]);
            $line=str_replace(" ", "&", $line);
            $line=strtolower($line);
            parse_str( $line, $parms );

            if (!isset($parms['type'])) { // no type provided, or just forgot...
                $parms["type"]="purchase";
            }

            // get all different types of display here, define manually to avoid hacking of the code
            switch (strtolower($parms['type']) ) {
                case "detailsview":
                    $html=$this->analyticsDetailsView($parms);
                    break;
                case "addtocart":
                    $html=$this->analyticsAddToCart($parms);
                    break;
                case "begincheckout":
                    $html=$this->analyticsBeginCheckout($parms);
                    break;
                case "checkoutprogress":
                    $html=$this->analyticsCheckoutProgress($parms);
                    break;
                case "purchase": // the default if nothing has been provided
                    $html=$this->analyticsPurchase($parms);
                    break;
                default: // anything else provides an error message
                    $html=JText::_("SC_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). "({$parms['type']})";
            }

            if ($this->currency == "DISABLED")
                $html = "Analytics Tracking is disabled for this site language";


        	$article->text = preg_replace($regex, $html, $article->text, 1);
        }
        return true;
	}

	function analyticsDetailsView($params){
        $prodcode=@$params['code'];

        $db	= JFactory::getDBO();
        $query=$db->getQuery(true);
        $query->select("*");
        $query->from("#__sc_products");
        $query->where("prodcode='$prodcode'");
        $db->setQuery( $query );
        $product = $db->loadObject();

        if (@!$product->id) {
            $html = "<div>Product Not Fount</div>";
            return $html;
        }

        if ($product->published=='0') {
            $html = "<div>Product Not Published</div>";
            return $html;
        }

        $item = (object) [
            'id' => $product->prodcode,
            'name' => $product->shorttext,
            'brand' => $this->affiliation,
            'category' => $product->category,
            'price' => $product->unitprice,
        ];

        $contents[] = $item;

        $parameters = (object)[
            'items' => $contents,
        ];

        $script = "gtag('event', 'view_item', ".json_encode($parameters).");";

        $document = JFactory::getDocument();
        $document->addScriptDeclaration($script);

        return "";
    }

    function analyticsAddToCart($params){
        $prodcode=@$params['code'];

        $db	= JFactory::getDBO();
        $query=$db->getQuery(true);
        $query->select("*");
        $query->from("#__sc_products");
        $query->where("prodcode='$prodcode'");
        $db->setQuery( $query );
        $product = $db->loadObject();

        if (@!$product->id) {
            $html = "<div>Product Not Fount</div>";
            return $html;
        }

        if ($product->published=='0') {
            $html = "<div>Product Not Published</div>";
            return $html;
        }

        $item = (object) [
            'id' => $product->prodcode,
            'name' => $product->shorttext,
            'brand' => $this->affiliation,
            'category' => $product->category,
            'price' => $product->unitprice,
            'quantity' => "productQty"

        ];

        $contents[] = $item;

        $parameters = (object)[
            'items' => $contents,
        ];

        $script = "gtag('event', 'add_to_cart', ".str_replace( '"productQty"', 'productQty', json_encode($parameters)).");";

        $jquerry = "
        jQuery(document).ready(function($) {
            jQuery('form[name^=addtocart]').submit(function(e){
                var form = $(this);
                var formData = form.serializeArray();
                var fieldName = 'edtqty';
                var fieldFilter = function (field) {
                    return field.name == fieldName;
                }
                var productQty = formData.filter(fieldFilter)[0].value;
                $script
            });
        });";

        $document = JFactory::getDocument();
        $document->addScriptDeclaration($jquerry);

        return "";
    }

    function analyticsBeginCheckout($params){
        $cart2=new cart2();
        $cart=$cart2->readCart();

        $num_items = 0;
        $contents = array();

        foreach ($cart as $product) {
            switch ($product->prodcode) {
                case "shipping":
                case "tax":
                    break;
                default:
                    $num_items ++;

                    $item = (object) [
                        'id' => $product->prodcode,
                        'name'=> $product->prodname,
                        'brand'=> $this->affiliation,
                        'quantity' => $product->quantity,
                        'price' => $product->finalprice
                    ];
                    $contents[]=$item;
            }
        }

        $parameters = (object)[
            'items' => $contents,
        ];

        $script = "gtag('event', 'begin_checkout', ".json_encode($parameters).");";

        //Add pixel Code
        $document = JFactory::getDocument();
        $document->addScriptDeclaration($script);

        return "";
    }

    function analyticsCheckoutProgress($params){

        $jinput = JFactory::getApplication()->input;
        $ordercode=$jinput->getVar("data"); // the data co

        $contents = array();

        if (isset($ordercode)){
            $orders=new orders();
            $orderid=$orders->getOrderIdFromCart($ordercode);
            $odetails=new orderdetail();
            $lst=$odetails->getDetailsByOrderId($orderid);

            foreach ($lst as $product) {
                // create a post field and field value for CCNow

                switch ($product->prodcode) {case "shipping":
                    $shipping = number_format($product->unitprice, 2,".", "");
                    break;
                    case "tax":
                        $tax=number_format($product->unitprice, 2,".", "");
                        break;
                    default:
                        $tmprod = new products();
                        $tmprod->getproductByProdCode($product->prodcode);

                        $item = (object) [
                            'id' => $product->prodcode,
                            'name' => $product->shorttext,
                            'brand' => $this->affiliation,
                            'category' => $tmprod->category,
                            'price' => $product->unitprice,
                            'quantity' => $product->qty
                        ];

                        $contents[]=$item;

                        unset($tmprod);
                }
            }

        }else{
            $cart2=new cart2();
            $cart=$cart2->readCart();

            foreach ($cart as $product) {
                switch ($product->prodcode) {
                    case "shipping":
                    case "tax":
                        break;
                    default:


                        $item = (object) [
                            'id' => $product->prodcode,
                            'name'=> $product->prodname,
                            'brand'=> $this->affiliation,
                            'quantity' => $product->quantity,
                            'price' => $product->finalprice
                        ];
                        $contents[]=$item;
                }
            }
        }

        $parameters = (object)[
            'items' => $contents,
        ];

        $script = "gtag('event', 'checkout_progress', ".json_encode($parameters).");";

        //Add pixel Code
        $document = JFactory::getDocument();
        $document->addScriptDeclaration($script);

        return "";
    }

	function analyticsPurchase($parms) { // display of Analytics script goes here
        $jinput = JFactory::getApplication()->input;
        $ordercode=$jinput->getVar("data"); // the data contains the ordercode when you finish the details page

        $orders=new orders();
		$orderid=$orders->getOrderIdFromCart($ordercode);
		$odetails=new orderdetail();
        $lst=$odetails->getDetailsByOrderId($orderid);

        $gtotal=0; //define the grand total
        $shipping=0; // define the tax for CCNow
        $tax=0;
        $contents = array();

		foreach ($lst as $product) {
            // create a post field and field value for CCNow

            switch ($product->prodcode) {case "shipping":
            		$shipping = number_format($product->unitprice, 2,".", "");
					break;
    			case "tax":
					$tax=number_format($product->unitprice, 2,".", "");
    				break;
    			default:
    				$tmprod = new products();
    				$tmprod->getproductByProdCode($product->prodcode);

                    $item = (object) [
                        'id' => $product->prodcode,
                        'name' => $product->shorttext,
                        'brand' => $this->affiliation,
                        'category' => $tmprod->category,
                        'price' => $product->unitprice,
                        'quantity' => $product->qty
                    ];

                    $contents[]=$item;

					unset($tmprod);
            }
            $gtotal += $product->total;
		}

        $parameters = (object)[
            'transaction_id' => $orderid,
            'affiliation' => $this->affiliation,
            'value' => $gtotal,
            'currency' => $this->currency,
            'items' => $contents,
        ];

        if($this->tracktax == 1)
            $parameters->tax=$tax;
        if($this->trackshipping == 1)
            $parameters->shipping=$shipping;

        $script = "gtag('event', 'purchase', ".json_encode($parameters).");";

        //Add pixel Code
        $document = JFactory::getDocument();
        $document->addScriptDeclaration($script);

        return "";
	}
	
	
} 

