<?php
/**
 * @copyright Ivan Najdanović 2013
 * SimpleCaddy fbpixel processor 
 * @version 1.0.0 for Joomla 2.5
 */
  
// No direct access allowed to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
    require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
    return;
}

jimport( 'joomla.application.input' );
jimport( 'joomla.plugin.helper' );

/**
 * This plugin has three parts:
 * top part is getting the SimpleCaddy functions library
 * in the function onContentPrepare are the Joomla functions to manage the plugin in the content
 * the function showfbpixelScreen displays and manages tracking script
 * 
 */
 
class scFbPixelConfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $fbpixeladdcode;
	var $fbpixelid;
	var $fbpixelcurrency;
	
	function __construct(){
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_scfbpixel', JPATH_ADMINISTRATOR);
		$db	= JFactory::getDBO();
        try {
            parent::__construct( '#__sc_fbpixel', 'id', $db );
        }
        catch (Exception $e) {
            $query= "CREATE TABLE `#__sc_fbpixel` (
			`id`  int NULL AUTO_INCREMENT ,
			`$fbpixeladdcode`  INT(1) NULL ,
			`fbpixelid`  text NULL ,
			`fbpixeladdcode`  text NULL ,
			PRIMARY KEY (`id`)
			);";
            $this->_db->setQuery($query);
            $this->_db->execute();
            // now reconstruct, since it failed above...
            parent::__construct( '#__sc_fbpixel', 'id', $db );
		};
        // get the first and only useful record in the db here
        $query=$this->_db->getQuery(true);
        $query->select("id");
        $query->from("`$this->_tbl`");
        $this->_db->setQuery($query);
        $id=$this->_db->loadResult(); // this sets the first ID as the one to use.
        $this->load($id); // load the first id
	}

	function _showconfig() { // mandatory function name
		$lang = JFactory::getLanguage(); // joomla's backend does not load frontend languages by itself
		$extension = 'plg_content_scfbpixel'; // so we need to do this manually here
		$lang->load($extension);
		$fbpixelcurrency=array();
		$fbpixelcurrency["DISABLED"]="Disable Tacking For This Language";
        $fbpixelcurrency["BGN"]="Bulgarian Lev (BGN)";
		$fbpixelcurrency["EUR"]="Euros (EUR)";
		$fbpixelcurrency["RSD"]="Serbian Dinar (RSD)";
		$fbpixelcurrency["USD"]="US Dollar (USD)";


        require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
		$this->load();

		$db = JFactory::getDbo();
		$query="SELECT sef, title_native, lang_code FROM #__languages ORDER BY sef ASC";
		$db->setQuery($query);
		$languages = $db->loadObjectList();

		$this->fbpixelcurrency = unserialize($this->fbpixelcurrency);
		$this->fbpixelid = unserialize($this->fbpixelid);

		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCFBPIXEL_CONFIGURATION" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SCFBPIXEL_SAVE"), false);
		JToolbarHelper::cancel();
        JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false,  false );
        ?>
		<form name="adminForm" id="adminForm">
			<table class="adminform">
				<tr><td style="width: 450px;"><?php echo JText::_("SCFBPIXEL_ADD_CODE");?></td>
				<td>
                    <div class='control-group'>
                        <div class="controls">
                            <fieldset class="radio btn-group">';
                                <label for='0fbpixeladdcode'><input type='radio' id='0fbpixeladdcode' value='0' name='fbpixeladdcode' <?= ($this->fbpixeladdcode==0? 'checked="checked"':'') ?>>No</label>
                                <label for='1fbpixeladdcode'><input type='radio' id='1fbpixeladdcode' value='1' name='fbpixeladdcode' <?= ($this->fbpixeladdcode==1? 'checked="checked"':'') ?>>Yes</label>
                            </fieldset>
                        </div>
                    </div>

				</td></tr>

                <?php  if ($this->fbpixeladdcode==1) {
                    foreach ($languages as $l) {
                        echo "<tr><td>$l->title_native " . JText::_("SCFBPIXEL_ID") . "</td><td>";
                        echo "<input type=\"text\" name=\"fbpixelid[$l->lang_code]\" value=\"" . $this->fbpixelid[$l->lang_code] . "\" size=\"100\" /></td>";
                    }
                }
                ?>

				<?php 
					foreach ($languages as $l){
						echo "<tr><td>$l->title_native ".JText::_("SCFBPIXEL_CURRENCY")."</td><td>";
						echo "<select name=\"fbpixelcurrency[$l->lang_code]\">";
						foreach ($fbpixelcurrency as $key=>$f) {
							echo "<option value='$key'".($key==$this->fbpixelcurrency["$l->lang_code"]?" selected":"").">$f</option>";
						}
						echo "</select></td></tr>" ;
					}
				?>
				
				<input type="hidden" name="option" value="com_simplecaddy" />
				<input type="hidden" name="action" value="pluginconfig"/>
				<input type="hidden" name="pluginname" value="scfbpixel"/>
				<input type="hidden" name="task" />
				<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
			</table>
		</form>
		<?php
	}
	
	function save($stay=false, $orderingFilter = '', $ignore = '') { // mandatory function to save any variables;


		$this->bind($_REQUEST); // get all the fields from the admin form

        if ($this->fbpixeladdcode==1 && isset($_REQUEST["fbpixelid"]))
            $this->fbpixelid=serialize($this->fbpixelid);


		$this->fbpixelcurrency=serialize($this->fbpixelcurrency);

		$b=$this->store(); // store the relevant fields only
		if ($b) {
			$msg=JText::_("SCFBPIXEL_CONFIG_SAVED");
		}
		else
		{
			$dbmsg=$this->_db->getErrorMsg();
			$msg=JText::_("SCFBPIXEL_CONFIG_NOT_SAVED") . $dbmsg;
		}
		$mainframe=JFactory::getApplication();
        $mainframe->enqueueMessage($msg);
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=scfbpixel");
	}
	
	function cancel() {
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=plugins&task=show");
	}
    function main() {
        $mainframe=JFactory::getApplication();
        $mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
    }
}

//The Content plugin Loadmodule
class plgContentScFbPixel extends JPlugin {
    var $debugshow="hidden"; // just for testing purposes
    var $_plugin_number	= 0;
    var $fbpixeladdcode;
    var $fbpixelid;
    var $currency;


    function __construct( &$subject, $config ) {
        parent::__construct( $subject, $config );
        $this->loadLanguage(); // necessary or not, let's make sure we get the language file

        $lang = JFactory::getLanguage();
        $cfg=new scfbpixelconfiguration();

        $this->fbpixeladdcode = $cfg->fbpixeladdcode;

        if ($this->fbpixeladdcode==1){
            $fbpixelid = unserialize($cfg->fbpixelid);
            $this->fbpixelid = $fbpixelid[$lang->getTag()];
        }

        $currency = unserialize($cfg->fbpixelcurrency);
        $this->currency = $currency[$lang->getTag()];


    }

    public function _setPluginNumber() {
        $this->_plugin_number = (int)$this->_plugin_number + 1; // only the first occurrence of the plugin should load css
    }

    function onContentPrepare($context, &$article, &$params, $page = 0 ) {
        if (!JComponentHelper::isEnabled('com_simplecaddy', true)) { // check for the component install
            echo "<div style='color:red;'>The SimpleCaddy component is not installed or is not enabled</div>";
            return "";
        }

        $regex = '/{(scfbpixel)\s*(.*?)}/i'; // the plugin code to get from content
	
        $parms=array();
        $matches = array();
        preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
        foreach ($matches as $elm) {
 			$this->_setPluginNumber();
 			if ($this->_plugin_number==1 &&  $this->fbpixeladdcode==1) { // get the stylesheet only ONCE per page
                if (!JPluginHelper::isEnabled("system",'DSYPixel'));
                    $this->fbPixelInit();
 			}

            $line=strip_tags($elm[2]); // get rid of most of the embedded html tags, if any. Only tags directly after the { will not be cleaned as regex will not pick those up
            $line=str_replace("&nbsp;", " ", $line);
            $line=str_replace(" ", "&", $line);
            $line=strtolower($line);
            parse_str( $line, $parms );

            if (!isset($parms['type'])) { // no type provided, or just forgot...
                $parms["type"]="purchase";
            }

            // get all different types of display here, define manually to avoid hacking of the code
            switch (strtolower($parms['type']) ) {
                case "viewcontent":
                    $html=$this->fbPixelViewContent($parms);
                    break;
                case "addtocart":
                    $html=$this->fbPixelAddToCart($parms);
                    break;
                case "initiatecheckout":
                    $html=$this->fbPixelInitiateCheckout($parms);
                    break;
                case "addpaymentinfo":
                    $html=$this->fbPixelAddPaymentInto($parms);
                    break;
                case "purchase": // the default if nothing has been provided
                    $html=$this->fbPixelPurchase($parms);
                    break;
                default: // anything else provides an error message
                    $html=JText::_("SC_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). "({$parms['type']})";
                    break;
            }

            if ($this->fbpixeladdcode==1)
                if (!$this->fbpixelid)
                    $html = "Facebook Pixel id is not entered, please add this to the configuration first. Plugin cannot be used!";

            if ($this->currency == "DISABLED")
                $html = "";

            $article->text = preg_replace($regex, $html, $article->text, 1);
        }
        return true;
	}

	function fbPixelInit(){

        $jinput = JFactory::getApplication()->input;

        $ordercode=$jinput->getVar("data");

        $parameters=array();

        if ($ordercode){
            $orders=new orders();
            $orderid=$orders->getOrderIdFromCart($ordercode);
            $order= $orders->getorder($orderid);
            $customFiels = unserialize($order->customfields);

            if (isset($order->email)){
                $email = strtolower(filter_var($order->email, FILTER_SANITIZE_EMAIL));
                if (filter_var($email, FILTER_VALIDATE_EMAIL))
                    $parameters["em"]=strtolower(str_replace(' ', '', $order->email));
            }
            if (isset($customFiels['first_name']))
                $parameters["fn"]=strtolower(str_replace(' ', '', $customFiels['first_name']));
            if (isset($customFiels['last_name']))
                $parameters["ln"]=strtolower(str_replace(' ', '', $customFiels['last_name']));

            if (isset($order->telephone)){
                $phone = preg_replace("/[^0-9]/","", $order->telephone);
                if (strlen($phone) > 0)
                    $parameters["ph"] = $phone;
            }
            if (isset($order->city))
                $parameters["ct"]=strtolower(str_replace(' ', '', $order->city));

            if (isset($order->codepostal))
                $zip = preg_replace("/[^0-9]/","", $order->codepostal);
                if (strlen($zip) > 0)
                    $parameters["zp"] = $zip;

        }

        $script = "fbq('set', 'autoConfig', 'false', '$this->fbpixelid');";


        if (count($parameters)>0){
            $script = "fbq('init', '$this->fbpixelid', {\n";
            foreach ($parameters as $parameter => $value){
                $script.= "'$parameter' : '$value',\n";
            }
            $script .= "});";
        }else{
            $script .= "fbq('init', '$this->fbpixelid');";
        }

        //Add pixel Code
        $document = JFactory::getDocument();
        $document->addScript('/plugins/content/scfbpixel/pixel.js');
        $document->addScriptDeclaration($script);

    }

	function fbPixelViewContent($params){
        $prodcode=@$params['code'];

        $db	= JFactory::getDBO();
        $query=$db->getQuery(true);
        $query->select("*");
        $query->from("#__sc_products");
        $query->where("prodcode='$prodcode'");
        $db->setQuery( $query );
        $product = $db->loadObject();

        if (@!$product->id) {
            $html = "<div>Product Not Fount</div>";
            return $html;
        }

        if ($product->published=='0') {
            $html = "<div>Product Not Published</div>";
            return $html;
        }

        $item = (object) [
            'id' => $product->prodcode,
            'item_price' => $product->unitprice,
            'quantity' => 1
        ];

        $contents[] = $item;

        $parameters = (object)[
            'value' => $product->unitprice,
            'currency' => $this->currency,
            'content_name' => $product->shorttext,
            'content_type' => 'product',
            'contents' => $contents,
        ];


        $script = "fbq('track', 'ViewContent',".json_encode($parameters).");";

        $document = JFactory::getDocument();
        $document->addScriptDeclaration($script);

        //TODO: Implement No Script Tag

        return "";

    }

    function fbPixelAddToCart($params){
        $prodcode=@$params['code'];


        $db	= JFactory::getDBO();
        $query=$db->getQuery(true);
        $query->select("*");
        $query->from("#__sc_products");
        $query->where("prodcode='$prodcode'");
        $db->setQuery( $query );
        $product = $db->loadObject();

        if (@!$product->id) {
            $html = "<div>Product Not Fount</div>";
            return $html;
        }

        if ($product->published=='0') {
            $html = "<div>Product Not Published</div>";
            return $html;
        }

        $item = (object) [
            'id' => $product->prodcode,
            'item_price' => $product->unitprice,
            'quantity' => "productQty"
        ];

        $contents[] = $item;

        $parameters = (object)[
            'value' => $product->unitprice,
            'currency' => $this->currency,
            'content_name' => $product->shorttext,
            'content_type' => 'product',
            'contents' => $contents,
        ];

        $script = "fbq('track', 'AddToCart',".str_replace( '"productQty"', 'productQty', json_encode($parameters)).");";

        $jquerry = "
        jQuery(document).ready(function($) {
            jQuery('form[name^=addtocart]').submit(function(e){
                var form = $(this);
                var formData = form.serializeArray();
                var fieldName = 'edtqty';
                var fieldFilter = function (field) {
                    return field.name == fieldName;
                }
                var productQty = formData.filter(fieldFilter)[0].value;
                $script
            });
        });";

        $document = JFactory::getDocument();
        $document->addScriptDeclaration($jquerry);

        return "";
    }

    function fbPixelInitiateCheckout($params){

        $cart2=new cart2();
        $cart=$cart2->readCart();

        $gtotal=0;
        $num_items = 0;
        $contents = array();

        foreach ($cart as $product) {
            switch ($product->prodcode) {
                case "shipping":
                case "tax":
                    break;
                default:
                    $num_items ++;

                    $item = (object) [
                        'id' => $product->prodcode,
                        'quantity' => $product->quantity,
                        'item_price' => $product->finalprice
                    ];
                    $contents[]=$item;
            }
            $gtotal += $product->quantity *  $product->finalprice;
        }

        $parameters = (object)[
            'value' => $gtotal,
            'currency' => $this->currency,
            'content_type' => 'product',
            'contents' => $contents,
            'num_items' => $num_items,
        ];

        $script = "fbq('track', 'InitiateCheckout',".json_encode($parameters).");";

        //Add pixel Code
        $document = JFactory::getDocument();
        $document->addScriptDeclaration($script);

        return "";

    }

    function fbPixelAddPaymentInto($params){

        $jinput = JFactory::getApplication()->input;
        $ordercode=$jinput->getVar("data"); // the data co

        $gtotal=0;
        $num_items = 0;
        $contents = array();

        if (isset($ordercode)){
            $orders=new orders();
            $orderid=$orders->getOrderIdFromCart($ordercode);
            $odetails=new orderdetail();
            $lst=$odetails->getDetailsByOrderId($orderid);

            foreach ($lst as $product) {
                switch ($product->prodcode) {
                    case "shipping":
                    case "tax":
                        break;
                    default:
                        $num_items ++;
                        $tmprod = new products();
                        $tmprod->getproductByProdCode($product->prodcode);

                        $item = (object) [
                            'id' => $product->prodcode,
                            'quantity' => $product->qty,
                            'item_price' => $product->unitprice,
                        ];

                        $contents[] = $item;
                        unset($tmprod);
                }
                $gtotal += $product->total;
            }

        }else{
            $cart2=new cart2();
            $cart=$cart2->readCart();

            foreach ($cart as $product) {
                switch ($product->prodcode) {
                    case "shipping":
                    case "tax":
                        break;
                    default:
                        $num_items ++;

                        $item = (object) [
                            'id' => $product->prodcode,
                            'quantity' => $product->quantity,
                            'item_price' => $product->finalprice
                        ];
                        $contents[]=$item;
                }
                $gtotal += $product->quantity *  $product->finalprice;
            }
        }

        $parameters = (object)[
            'value' => $gtotal,
            'currency' => $this->currency,
            'content_type' => 'product',
            'contents' => $contents,
            'num_items' => $num_items,
        ];

        $script = "fbq('track', 'AddPaymentInfo',".json_encode($parameters).");";

        //Add pixel Code
        $document = JFactory::getDocument();
        $document->addScriptDeclaration($script);

        return "";

    }

	function fbPixelPurchase($params) { // display of any FbPixel specific stuff goes here
        $jinput = JFactory::getApplication()->input;
		$ordercode=$jinput->getVar("data"); // the data contains the ordercode when you finish the details page
				
		$orders=new orders(); 
		$orderid=$orders->getOrderIdFromCart($ordercode);
	    $odetails=new orderdetail();
        $lst=$odetails->getDetailsByOrderId($orderid);

        $gtotal=0;
        $num_items = 0;
        $contents = array();

		foreach ($lst as $product) {
            switch ($product->prodcode) {
                case "shipping":
                case "tax":
                    break;
                default:
                    $num_items ++;
                    $tmprod = new products();
                    $tmprod->getproductByProdCode($product->prodcode);

                    $item = (object) [
                        'id' => $product->prodcode,
                        'quantity' => $product->qty,
                        'item_price' => $product->unitprice,
                    ];

                    $contents[] = $item;
                    unset($tmprod);
            }
            $gtotal += $product->total;
		}

        $parameters = (object)[
            'value' => $gtotal,
            'currency' => $this->currency,
            'content_type' => 'product',
            'contents' => $contents,
            'num_items' => $num_items
        ];


        $script = "fbq('track', 'Purchase',".json_encode($parameters).");";

        $document = JFactory::getDocument();
        $document->addScriptDeclaration($script);

        //TODO: Implement No Script Tag

        return "";
	}
}

