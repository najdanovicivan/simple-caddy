<?php
/**
 * @copyright Henk von Pickartz 2011-2014
 * SimpleCaddy Paypal processor
 * @version 2.05 for Joomla 3.x
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access allowed to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
if (file_exists(JPATH_ROOT.'/components/com_simplecaddy/simplecaddy.class.php')) {
	require_once(JPATH_ROOT . '/components/com_simplecaddy/simplecaddy.class.php');
}
else {
	return;
}

/**
 * This plugin has three parts:
 * top part is getting the SimpleCaddy functions library
 * in the function onContentPrepare are the Joomla functions to manage the plugin in the content
 * the function showPayPalScreen displays and manages PayPal specifics
 */

class scpaypalconfiguration extends JTable { // this is the standard configuration class for the plugin
// naming is {pluginname}configuration
	var $id;
	var $ppenvironment;
	var $reselleremail;
	var $paypalcurrency;
	var $returnsuccess;
	var $returnfail;
	var $dlpage;

	function __construct(){
		$lang = JFactory::getLanguage();
		$lang->load('plg_content_scpaypal', JPATH_ADMINISTRATOR);
		$db	= JFactory::getDBO();
		try {
		   	parent::__construct( '#__sc_paypal', 'id', $db );
		}
		catch (Exception $e) {
			$query= "CREATE TABLE `#__sc_paypal` (
			`id`  int NULL AUTO_INCREMENT ,
			`ppenvironment`  INT(11) NULL ,
			`reselleremail`  varchar(255) NULL ,
			`paypalcurrency`  varchar(32) NULL ,
			`returnsuccess`  varchar(255) NULL ,
			`returnfail`  varchar(255) NULL ,
			`dlpage`  text NULL ,
			PRIMARY KEY (`id`)
			);";
			$this->_db->setQuery($query);
			$this->_db->execute();
			// now reconstruct, since it failed above...
			parent::__construct( '#__sc_paypal', 'id', $db );
		}
		// get the first and only useful record in the db here
		$query=$this->_db->getQuery(true);
		$query->select("id");
		$query->from("`$this->_tbl`");
		$this->_db->setQuery($query);
		$id=$this->_db->loadResult(); // this sets the first ID as the one to use.
		$this->load($id); // load the first id
	}

	function _showconfig() { // mandatory function name
		$lang = JFactory::getLanguage(); // joomla's backend does not load frontend languages by itself
		$extension = 'plg_content_scpaypal'; // so we need to do this manually here
		$lang->load($extension);
		$ppcurrency=array();
		$ppcurrency["AUD"]="Australian Dollars (AUD)";
		$ppcurrency["BRL"]="Brazilian Real (BRL) (Domestic use only)";
		$ppcurrency["CAD"]="Canadian dollar (CAD)";
		$ppcurrency["CHF"]="Swiss Franc (CHF)";
		$ppcurrency["CZK"]="Czech Koruna (CZK)";
		$ppcurrency["DKK"]="Danish Krone (DKK)";
		$ppcurrency["EUR"]="Euros (EUR)";
		$ppcurrency["GBP"]="Pound Sterling (GBP)";
		$ppcurrency["HKD"]="Hong Kong Dollar (HKD)";
		$ppcurrency["HUF"]="Hungarian Forint (HUF)";
		$ppcurrency["ILS"]="Israeli New Sheqel (ILS)";
		$ppcurrency["JPY"]="Yen (YEN)";
		$ppcurrency["MXN"]="Mexican Pesos (MXN)";
		$ppcurrency["MYR"]="Malaysian Ringgit (MYR) (Domestic use only)";
		$ppcurrency["NOK"]="Norwegian Krone (NOK)";
		$ppcurrency["NZD"]="New Zealand Dollar (NZD)";
		$ppcurrency["PHP"]="Philippine Peso (PHP)";
		$ppcurrency["PLN"]="Polish Zloty (PLN)";
		$ppcurrency["SEK"]="Swedish Krona (SEK)";
		$ppcurrency["SGD"]="Singapore Dollar (SGD)";
		$ppcurrency["THB"]="Thai Baht (THB)";
		$ppcurrency["TRY"]="Turkish Lira (TRY)";
		$ppcurrency["TWD"]="Taiwan New Dollar (TWD)";
		$ppcurrency["USD"]="US Dollar (USD)";

		require_once(JPATH_COMPONENT_SITE."/simplecaddy.class.php");
		$this->load();
		display::header(); // while this would be better inside the display function, a plugin does not have access to this
		JToolBarHelper::title( JText::_( "SCPP_CONFIGURATION" ), 'generic.png');
		JToolbarHelper::custom("save", "save", "", JText::_("SCPP_SAVE"), false);
		JToolbarHelper::cancel();
		JToolBarHelper::custom( 'control', 'back.png', 'back.png', 'Main', false,  false );
		?>
		<form name="adminForm" id="adminForm">
			<table class="adminform">
				<tr>
					<td style="width: 250px;">
						<label for="ppenvironment">
						<?php echo JText::_("SCPP_ENVIRONMENT");?>
						</label>
					</td>
					<td>
						<?php
						$show_hide = array (JHTML::_('select.option', 0, JText::_('SCPP_SANDBOX')), JHTML::_('select.option', 1, JText::_('SCPP_LIVE')),);
						echo JHTML::_('select.genericlist', $show_hide, 'ppenvironment',  'class="inputbox"', 'value', 'text', $this->ppenvironment);
						?>
					</td>
				</tr>
				<tr>
					<td style="width: 250px;">
						<label for="reselleremail">
						<?php echo JText::_("SCPP_RESELLER_EMAIL");?>
						</label>
					</td>
					<td>
						<input type="text" id="reselleremail" name="reselleremail" value="<?php echo $this->reselleremail; ?>" size="100" />
					</td>
				</tr>
				<tr>
					<td style="width: 250px;">
						<label for="paypalcurrency">
						<?php echo JText::_("SCPP_CURRENCY");?>
						</label>
					</td>
					<td>
							<?php
							echo JHTML::_('select.genericlist', $ppcurrency, 'paypalcurrency',  'class="inputbox"', 'value', 'text', $this->paypalcurrency);
							?>
					</td>
				</tr>

				<tr>
					<td style="width: 250px;">
						<label for="returnsuccess">
						<?php echo JText::_("SCPP_RETURNSUCCESS");?>
						</label>
					</td>
					<td>
						<input type="text" id="returnsuccess" name="returnsuccess" value="<?php echo $this->returnsuccess; ?>" style="width:400px;" />
					</td>
				</tr>
				<tr>
					<td style="width: 250px;">
						<label for="returnfail">
						<?php echo JText::_("SCPP_RETURNFAIL");?>
						</label>
					</td>
					<td>
						<input type="text" id="returnfail" name="returnfail" value="<?php echo $this->returnfail; ?>"  style="width:400px;" />
					</td>
				</tr>
			</table>
			<input type="hidden" name="option" value="com_simplecaddy" />
			<input type="hidden" name="action" value="pluginconfig"/>
			<input type="hidden" name="pluginname" value="scpaypal"/>
			<input type="hidden" name="task" />
			<input type="hidden" name="id" value="<?php echo $this->id;?>"/>
		</form>
		<p style="color: red;font-weight: bold;">
			<?php
			echo JText::_("SCPP_CLOAK_WARNING");
			?>
		</p>
	<?php
	}

	function save($src="", $orderingFilter = '', $ignore = '') { // mandatory function to save any variables
		$this->bind($_REQUEST); // get all the fields from the admin form
		$b=$this->store(); // store the relevant fields only
		if ($b) {
			$msg=JText::_("SCPP_CONFIG_SAVED");
		}
		else
		{
			$dbmsg=$this->_db->getErrorMsg();
			$msg=JText::_("SCPP_CONFIG_NOT_SAVED") . $dbmsg;
		}
		$mainframe=JFactory::getApplication();
		$mainframe->enqueueMessage($msg);
		$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=showpluginconfig&pluginname=scpaypal");
	}

	function cancel() {
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
	}
	function main() {
		$mainframe=JFactory::getApplication();
		$mainframe->redirect("index.php?option=com_simplecaddy&action=scplugins&task=show");
	}
}

//The Content plugin Loadmodule
class plgContentScpaypal extends JPlugin {
	var $debugshow="hidden"; // just for testing purposes
	var $_plugin_number	= 0;

	function __construct( &$subject, $config ) {
        parent::__construct( $subject, $config );
        $this->loadLanguage(); // necessary or not, let's make sure we get the language file
	}

	public function _setPluginNumber() {
		$this->_plugin_number = (int)$this->_plugin_number + 1; // only the first occurrence of the plugin should load css
	}

	function onContentPrepare($context, &$article, &$params, $page = 0 ) {
		if (!JComponentHelper::isEnabled('com_simplecaddy', true)) { // check for the component install
			echo "<div style='color:red;'>The SimpleCaddy component is not installed or is not enabled</div>";
			return "";
		}

        $regex = '/{(scpaypal)\s*(.*?)}/i'; // the plugin code to get from content

        $parms=array();
        $matches = array();
        preg_match_all( $regex, $article->text, $matches, PREG_SET_ORDER );
        foreach ($matches as $elm) {
 			$this->_setPluginNumber();
 			if ($this->_plugin_number==1) { // get the stylesheet only ONCE per page
				JHTML::stylesheet('components/com_simplecaddy/css/simplecaddy.css' );
				// on windows servers this may need to be changed to
				// JHTML::stylesheet('components\com_simplecaddy\css\simplecaddy.css' );
 			}
 			if ($this->_plugin_number>1 ) { // there should be only ONE payment plugin per page...
	            $article->text = preg_replace($regex, JText::_("SC_EXTRA_PLUGINS_REMOVED"), $article->text, 1);
		 		return true;
 			}
			$line=str_replace("&nbsp;", " ", $elm[2]);
            $line=str_replace(" ", "&", $line);
            $line=strtolower($line);
            parse_str( $line, $parms );

            if (!isset($parms['type'])) { // no type provided, or just forgot...
            	$parms["type"]="checkout";
            }
        	// get all different types of display here, define manually to avoid hacking of the code
        	switch (strtolower($parms['type']) ) {
        		case "ipn":
        		case "paysuccess":
        			$html=$this->showPayPalSuccess($parms);
        			break;
        		case "payfail":
        			$html=$this->showPayPalFail($parms);
        			break;
       			case "checkout": // the default if nothing has been provided
                	$html=$this->showPayPalscreen($parms);
                	break;
        		default: // anything else provides an error message
                	$html=JText::_("SC_THIS_PLUGIN_TYPE_NOT_SUPPORTED"). "({$parms['type']})";
        	}
            $article->text = preg_replace($regex, $html, $article->text, 1);
        }
        return true;
	}

	function showPayPalScreen($parms) { // display of any PayPal specific stuff goes here
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$cfg=new scpaypalconfiguration();
		if (!$cfg->reselleremail) {
			$html = JText::_("SCPP_ERROR_RESELLER_EMAIL");
			return $html;
		}
		$currency = $cfg->paypalcurrency;
		$ordercode=$input->get("data"); // the data contains the ordercode when you finish the details page

		$orders=new orders();
		$orderid=$orders->getOrderIdFromCart($ordercode);
		$order=new order();
		$order->load($orderid);

		// add the details to the order
		$gtotal=0; //define the grand total

        if ($cfg->ppenvironment==1) { // live
    		$html = "<form action='https://www.paypal.com/cgi-bin/webscr' method='post' name ='ppform' target='paypal'>";
        }
        else
        { // sandbox
            $html = "<form action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post' name ='ppform' target='_blank'>";
        }
   		$html .="<input type=\"hidden\" name=\"cmd\" value=\"_cart\">";
   		$html .="<input type=\"hidden\" name=\"upload\" value=\"1\">";
   		$html .="<input type=\"hidden\" name=\"business\" value=\"$cfg->reselleremail\">";
		$html .="<input type=\"hidden\" name=\"currency_code\" value=\"$currency\">";
		$html .="<input type=\"hidden\" name=\"rm\" value=\"2\">";
		$fieldnumber=0; // PayPal field numbering variable
        $odetails=new orderdetail();
        $lst=$odetails->getDetailsByOrderId($orderid);
		foreach ($lst as $product) {
            // create a post field and field value for PayPal
			if($product->total>0) {
				$fieldnumber = $fieldnumber +1 ; //increment the field number (could also be done with $fieldnumber++)
				$html .= "<input type='hidden' name='item_name_".$fieldnumber. "' value='".$product->shorttext." (".$product->prodcode.") ".$product->option. "'>";
				$html .= "<input type='hidden' name='amount_".$fieldnumber. "' value='".number_format($product->unitprice, 2,".", ""). "'>";
				$html .= "<input type='hidden' name='quantity_".$fieldnumber. "' value='".$product->qty. "'>";
			}
            else // price <0 so transfer it as a discount amount instead of a product
            {
                $html .= "<input type='hidden' name='discount_amount_cart' value='".abs($product->total). "'>";
            }
            $gtotal += $product->total;
		}
		$html .="<input type=\"hidden\" name=\"custom\" value=\"$orderid\">";
		if ($order->shipCost > 0) { // add shipping to paypal
			$html .="<input type=\"hidden\" name=\"shipping_1\" value=\"".number_format($order->shipCost, 2,".", "")."\">";
		}

		if ($order->tax > 0) { // add taxes to paypal
			$html .= "<input type=\"hidden\" name=\"tax_cart\" value=\"".number_format($order->tax, 2,".", "").'">';//
		}

        // these are the return urls to go to when coming back from paypal
        $successurl= $cfg->returnsuccess;
        $successurl .= "&dlkey=$order->ordercode";
        $failurl= $cfg->returnfail;

		$html .="<input type=\"hidden\" name=\"cancel_return\" value=\"$failurl\">";
		$html .="<input type=\"hidden\" name=\"return\" value=\"$successurl\">";
		$html .="<input type=\"hidden\" name=\"cbt\" value=\"Return to business\">";

 		// PayPal requires you use their logo to check out. Check the PayPal site for other button types
 		// look here for more buttons from PayPal https://www.paypal.com/newlogobuttons
        // look here for the rules of usage of the paypal logos and pay buttons:
        //https://www.paypalobjects.com/WEBSCR-640-20110401-1/en_US/pdf/merchant_graphic_guidelines.pdf

		/**
		 * customizers, do your stuff here!
		You may add all kinds of fields now to the paypal "cart" to customize your heading in PayPal and so on.
		None of these novelties have been added here, but if you want to customize the appearance of your presence in Paypal,
		Here is the place.
		*/

		$html .= '<p>
        <input type="image" name="submit" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" align="left" style="margin-right:7px;"> <span style="font-size:11px; font-family: Arial, Verdana;">The safer, easier way to pay.</span>
        <p>';
		$html .= "</form>";
		return $html;
	}

	function showPayPalSuccess($parms) {
		$mainframe=JFactory::getApplication();
		$input=$mainframe->input;
		$pcfg=new sc_configuration();
		$html = "";

        $orderid=$input->get("custom"); // orderid coming back from PP
        $txn_id=$input->get("txn_id"); // pp transaction ID

		$cfg=new sc_configuration();
        $statuses=explode("\r\n", $cfg->get("ostatus"));
        $status=$statuses[count($statuses)-1]; // set the status to the last one in the list

        $scorder=new order();
        $scorder->load($orderid);
        $scorder->paymentcode="PayPal: ".$txn_id;
        $scorder->status=$status;
        // obviously there are other changes you could make to an order upon successful payment...
        $scorder->store();

        $dlpageurl= $pcfg->dlpage;
		if (substr($dlpageurl, -1)=="/")
		{
			$dlpageurl .= "?dlkey=$scorder->ordercode";
		}
		else {
			$dlpageurl .= "&dlkey=$scorder->ordercode";
		}
		$mainframe->redirect($dlpageurl);
		return $html;
	}

	function showPayPalFail() {
		$html = "Payment failed";
//		$html .= sprintf("<pre>%s</pre>", print_r($_POST, 1));
		return $html;
	}
}